-- Changed Coding for ABFY-6659(Added ORDER_CREATION_DATE) by arath3 on 24.05-2017
INSERT OVERWRITE TABLE lpfg_core.OTIF_CALCULATION

SELECT distinct calc.plant,
     itemcode AS itemcode,
	suppliercode,
	suppliername,
	CONCAT (
		trim(suppliercode),
		'-',
		TRIM(suppliername)
		) AS SUPPLIER,
	ordd,
	Year,
	CASE 
		WHEN month = '1'
			THEN CONCAT ('Jan')
		WHEN month = '2'
			THEN CONCAT ('Feb')
		WHEN month = '3'
			THEN CONCAT ('Mar')
		WHEN month = '4'
			THEN CONCAT ('Apr')
		WHEN month = '5'
			THEN CONCAT ('May')
		WHEN month = '6'
			THEN CONCAT ('Jun')
		WHEN month = '7'
			THEN CONCAT ('Jul')
		WHEN month = '8'
			THEN CONCAT ('Aug')
		WHEN month = '9'
			THEN CONCAT ('Sep')
		WHEN month = '10'
			THEN CONCAT ('Oct')
		WHEN month = '11'
			THEN CONCAT ('Nov')
		WHEN month = '12'
			THEN CONCAT ('Dec')
		ELSE ''
		END AS month,
	receipt_quantity,
	CALC.po_no,
	CALC.po_line,
	days_early_tolerance,
	days_end_tolerance,
	calc.num_del AS Number_of_deliveries,
	po_sku,
	po_sku_ordd,
	CONCAT ( po_sku_ordd, '_', cast(CALC.num_del AS string)) AS po_sku_ReqDate_deliverables,
	plantdesc,
	DELAY,
	ot1 AS on_time,
	ReceivedOnTime,
	ordq,
	ORDER_CREATION_DATE,
	CUM_SUM_RECEIPT_QTY_CALC AS Cumulative_QNT,
	(InFull * 100) AS InFull,
	round((ot1 * InFull * 100), 2) AS otif,
	itemdesc AS Material_Description,
	po_uom,
	gr_UOM AS receipt_uom,
	INCOTERMS,
	INCOTERMS_DESC,
	Receipt_Date,
	STANDARDLT,
	MATERIAL_TYPE,
	PO_TYPE,
	PO_CLASSIFICATION,
	TO_BE_DISPLAYED,
	CASE WHEN OT_RC.REASON_CODE IS NULL 
	   then 0
	ELSE OT_RC.REASON_CODE
	END AS REASON_CODE,
	CASE WHEN OT_RC.REASON_CODE IS NULL 
	   then ''
	ELSE OT_RC.ROOT_CAUSE
	END AS ROOT_CAUSE,
	CUM_SUM_RECEIPT_QTY,
	INFULL_ALL,	
	CASE 
	   WHEN ot1 = 1 
	     THEN RECEIVEDONTIME 
	   ELSE 
	     CASE
	       WHEN OT_RC.REASON_CODE = 2 
		        THEN receipt_quantity
	       ELSE 0
	   END
	END AS ReceivedOnTime_RC,
	
	CASE 
	   WHEN  OT_RC.REASON_CODE = 2  
	      THEN CUM_SUM_RECEIPT_QTY 
	   ELSE CUM_SUM_RECEIPT_QTY_CALC
	END AS Cumulative_QNT_RC,
	
	CASE 
	   WHEN ot1 = 1 
	     THEN  (InFull_ALL * 100) 
	ELSE 
	   CASE
	   WHEN OT_RC.REASON_CODE = 2 
		 THEN (InFull_ALL * 100)
	   ELSE 0	
	   END
	END AS InFull_RC,
	
	CASE 
	   WHEN ot1 = 1 
	     THEN round((InFull * 100), 2) 
	ELSE 
	  CASE
	   WHEN OT_RC.REASON_CODE = 2 
        THEN round((InFull_ALL * 100), 2)
	   ELSE 0
	  END
	END AS otif_RC,
    percentage_ordq,
	low_qty_tol,  -- added for slotif calculation
    high_qty_tol, -- added for slotif calculation
	case when 
			(percentage_ordq between (cast(100 as double) - low_qty_tol) and (cast(100 as double) + high_qty_tol)) and ot1=1
			then 100
			else 0 
			end as slotif 
FROM ( 
	SELECT *,
		   CASE 
		   WHEN (ordq <> 0) AND CUM_SUM_RECEIPT_QTY_CALC <= ordq
		   THEN (CUM_SUM_RECEIPT_QTY_CALC / cast(ordq AS DOUBLE))
		   
		   WHEN (ordq <> 0)	AND CUM_SUM_RECEIPT_QTY_CALC > ordq 
		   THEN (ordq - (CUM_SUM_RECEIPT_QTY_CALC - ordq )) / ordq 
		   ELSE 0
		   END AS InFull,
		   
		   CASE 
		   WHEN (ordq <> 0) AND CUM_SUM_RECEIPT_QTY <= ordq
		   THEN (CUM_SUM_RECEIPT_QTY / cast(ordq AS DOUBLE))
		   
		   WHEN (ordq <> 0)	AND CUM_SUM_RECEIPT_QTY > ordq 
		   THEN (ordq - (CUM_SUM_RECEIPT_QTY - ordq )) / ordq 
		   ELSE 0
		   END AS  InFull_ALL
		   
		   
	  FROM lpfg_wrk.OTIF_CALC_BASE 
	  
) CALC 
LEFT OUTER JOIN lpfg_wrk.OTIF_ONTIME_DISPLAY  OT_DISPLAY ON (
	    (regexp_replace(trim(CALC.PO_NO),"^0*",""))=(regexp_replace(trim(OT_DISPLAY.PO_NO),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_LINE),"^0*",""))=(regexp_replace(trim(OT_DISPLAY.PO_LINE),"^0*",""))
	 AND CALC.NUM_DEL=OT_DISPLAY.NUM_DEL
		)
	
LEFT OUTER JOIN lpfg_stg.OTIF_REASONCODE OT_RC ON (
		(regexp_replace(trim(CALC.PLANT),"^0*",""))=(regexp_replace(trim(OT_RC.PLANT),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_NO),"^0*",""))=(regexp_replace(trim(OT_RC.PO_NO),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_LINE),"^0*",""))=(regexp_replace(trim(OT_RC.PO_LINE),"^0*",""))
	);
	
INSERT INTO TABLE lpfg_core.OTIF_CALCULATION
SELECT ITM_Spcl_Hist.plant,
		ITM_Spcl_Hist.MATERIAL_NO AS itemcode,
	ITM_Spcl_Hist.SUPPLIER_CODE AS suppliercode,
	ITM_Spcl_Hist.SUPPLIER_NAME AS suppliername,
	CONCAT (ITM_Spcl_Hist.SUPPLIER_CODE,'-',ITM_Spcl_Hist.SUPPLIER_NAME) AS SUPPLIER,
	'' ordd,
	Year,
	MONTH AS month,
	ITM_Spcl_Hist.receipt_quantity,
	ITM_Spcl_Hist.po_no,
	ITM_Spcl_Hist.po_line,
	'0' AS days_early_tolerance,
	'0' AS days_end_tolerance,
	0 AS Number_of_deliveries,
	CONCAT(ITM_Spcl_Hist.PO_NO,'_',ITM_Spcl_Hist.MATERIAL_NO) AS po_sku,
	'0' AS  po_sku_ordd,
	'0' AS po_sku_ReqDate_deliverables,
	'' plantdesc,
	0 AS DELAY,
	0 AS on_time,
	0 AS ReceivedOnTime,
	ITM_Spcl_Hist.Requested_delivery_QUANTITY AS ordq,
	'' AS ORDER_CREATION_DATE,
	ITM_Spcl_Hist.receipt_quantity AS Cumulative_QNT,
	0 AS InFull,
	ITM_Spcl_Hist.OTIF,
	ITM_Spcl_Hist.MATERIAL_DESC AS Material_Description,
	'' AS po_uom,
	'' AS receipt_uom,
	'' AS INCOTERMS,
	'' AS INCOTERMS_DESC,
	'' AS Receipt_Date,
	0 AS STANDARDLT,
	'' AS MATERIAL_TYPE,
	TYPE as PO_TYPE,
	'Special' AS PO_CLASSIFICATION,
	'Y' AS TO_BE_DISPLAYED,
	0 AS REASON_CODE,
	'' AS ROOT_CAUSE,
	0 AS CUM_SUM_RECEIPT_QTY,
	0 AS INFULL_ALL,	
	0 AS ReceivedOnTime_RC,
	0 AS Cumulative_QNT_RC,
	0 AS InFull_RC,
	0 AS otif_RC,
    0 AS percentage_ordq,
	0 AS low_qty_tol,  -- added for slotif calculation
    0 AS high_qty_tol, -- added for slotif calculation
	0 AS slotif 
FROM (SELECT KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY,
		(SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS Requested_delivery_QUANTITY,
		SUM(RECEIPT_QUANTITY) / (SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS OTIF 
		FROM lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY  
		GROUP BY KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY) ITM_Spcl_Hist where cast(OTIF AS string)<>'nan' ;	
		
INSERT INTO TABLE lpfg_core.OTIF_CALCULATION
SELECT VDR_Spcl_Hist.plant,
	   VDR_Spcl_Hist.MATERIAL_NO AS itemcode,
	VDR_Spcl_Hist.SUPPLIER_CODE AS suppliercode,
	VDR_Spcl_Hist.SUPPLIER_NAME AS suppliername,
	CONCAT (VDR_Spcl_Hist.SUPPLIER_CODE,'-',VDR_Spcl_Hist.SUPPLIER_NAME) AS SUPPLIER,
	'' ordd,
	Year,
	MONTH AS month,
	VDR_Spcl_Hist.receipt_quantity,
	VDR_Spcl_Hist.po_no,
	VDR_Spcl_Hist.po_line,
	'0' AS days_early_tolerance,
	'0' AS days_end_tolerance,
	0 AS Number_of_deliveries,
	CONCAT(VDR_Spcl_Hist.PO_NO,'_',VDR_Spcl_Hist.MATERIAL_NO) AS po_sku,
	'0' AS  po_sku_ordd,
	'0' AS po_sku_ReqDate_deliverables,
	'' plantdesc,
	0 AS DELAY,
	0 AS on_time,
	0 AS ReceivedOnTime,
	VDR_Spcl_Hist.Requested_delivery_QUANTITY AS ordq,
	'' AS ORDER_CREATION_DATE,
	VDR_Spcl_Hist.receipt_quantity AS Cumulative_QNT,
	0 AS InFull,
	VDR_Spcl_Hist.OTIF,
	VDR_Spcl_Hist.MATERIAL_DESC AS Material_Description,
	'' AS po_uom,
	'' AS receipt_uom,
	'' AS INCOTERMS,
	'' AS INCOTERMS_DESC,
	'' AS Receipt_Date,
	0 AS STANDARDLT,
	'' AS MATERIAL_TYPE,
	TYPE as PO_TYPE,
	'Special' AS PO_CLASSIFICATION,
	'Y' AS TO_BE_DISPLAYED,
	0 AS REASON_CODE,
	'' AS ROOT_CAUSE,
	0 AS CUM_SUM_RECEIPT_QTY,
	0 AS INFULL_ALL,	
	0 AS ReceivedOnTime_RC,
	0 AS Cumulative_QNT_RC,
	0 AS InFull_RC,
	0 AS otif_RC,
    0 AS percentage_ordq,
	0 AS low_qty_tol,  -- added for slotif calculation
    0 AS high_qty_tol, -- added for slotif calculation
	0 AS slotif 
FROM (SELECT KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY,
		(SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS Requested_delivery_QUANTITY,
		SUM(RECEIPT_QUANTITY) / (SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS OTIF 
		FROM lpfg_wrk.OTIF_VDR_SPECIAL_PO_HISTORY  
		GROUP BY KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY) VDR_Spcl_Hist where cast(OTIF AS string)<>'nan' ;	