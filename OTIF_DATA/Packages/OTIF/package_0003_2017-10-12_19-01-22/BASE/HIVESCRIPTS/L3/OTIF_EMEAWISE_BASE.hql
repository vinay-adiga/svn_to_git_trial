INSERT OVERWRITE TABLE lpfg_core.OTIF_EMEAWISE_BASE
SELECT   PLANT,
	month,
	year,
	ordq_sum,
	Cumulative_Sum,
	Cumulative_Sum_RC,
	on_time,
	InFull_Month,
	InFull_Month_RC,
	sum(ordq_sum) OVER (PARTITION BY year ORDER BY year) AS ordq_sum_all,-- changed by sudhir for emea calculation
	sum(Cumulative_Sum) OVER (PARTITION BY year ORDER BY year) AS Cumulative_Sum_all,-- changed by sudhir for emea calculation
	sum(Cumulative_Sum_RC) OVER (PARTITION BY year ORDER BY year) AS Cumulative_Sum_all_RC,
	((cast(InFull_Month AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month,
	((cast(InFull_Month_RC AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_RC,
	days_early_tolerance,
	days_end_tolerance,
	SLOTIF ,
	COUNT_SLOTIF ,
	SUM_SLOTIF ,
	(SUM_SLOTIF / COUNT_SLOTIF) * 100 AS PLANT_SLOTIF 
FROM (
	SELECT DISTINCT Otif_calc.PLANT,
		Otif_calc.month,
		Otif_calc.year,
		Otif_calc.ordq_sum,
		Otif_calc.Cumulative_Sum,
		Otif_calc.Cumulative_Sum_RC,
		Otif_calc.on_time,
		CASE 
			WHEN (cast(Otif_calc.Cumulative_Sum AS DOUBLE)) <= (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.Cumulative_Sum AS DOUBLE) / cast(Otif_calc.ordq_sum AS DOUBLE))
			WHEN (cast(Otif_calc.Cumulative_Sum AS DOUBLE)) > (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.ordq_sum AS DOUBLE) - (cast(Otif_calc.Cumulative_Sum AS DOUBLE) - cast(Otif_calc.ordq_sum AS DOUBLE))) / cast(Otif_calc.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month,
		CASE 
			WHEN (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE)) <= (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE) / cast(Otif_calc.ordq_sum AS DOUBLE))
			WHEN (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE)) > (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.ordq_sum AS DOUBLE) - (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE) - cast(Otif_calc.ordq_sum AS DOUBLE))) / cast(Otif_calc.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_RC,	
		Otif_calc.days_early_tolerance,
		Otif_calc.days_end_tolerance,
		Otif_calc.SLOTIF ,
		Otif_calc.COUNT_SLOTIF ,
		Otif_calc.SUM_SLOTIF 

	FROM (
		SELECT month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS month,
			cast(year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) as string) AS year,
			calc.del,
			calc.po_sku_ordd,
			OT_CALC.PLANT,
			OT_CALC.on_time,
			ordq_calc.ordq_sum AS ordq_sum,
			sum(cast(OT_CALC.Cumulative_QNT AS DOUBLE)) OVER (
				PARTITION BY --OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum,
			sum(cast(OT_CALC.Cumulative_QNT_RC AS DOUBLE)) OVER (
				PARTITION BY --OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum_RC,	
			OT_CALC.days_early_tolerance,
			OT_CALC.days_end_tolerance,
			OT_CALC.SLOTIF ,
				count() OVER (
				PARTITION BY --OT_CALC.PLANT, -- changed by sudhir for emea calculation
							month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_SLOTIF, --count
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY --OT_CALC.PLANT, -- changed by sudhir for emea calculation
														month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_SLOTIF --sum
		FROM lpfg_core.OTIF_CALCULATION OT_CALC
		LEFT OUTER JOIN lpfg_core.OTIF_EMEA_ORDQ_CALC ordq_calc ON (
				    OT_CALC.po_sku_ordd = ordq_calc.po_sku_ordd
				AND OT_CALC.PLANT = ordq_calc.PLANT
				),
			(
				SELECT max(number_of_deliveries) AS del,
					po_sku_ordd
				FROM lpfg_core.OTIF_CALCULATION b
				GROUP BY po_sku_ordd
				) calc
		WHERE OT_CALC.po_sku_ordd = calc.po_sku_ordd
			AND OT_CALC.number_of_deliveries = calc.del
		) Otif_calc
	) ot_emea_base;
