INSERT OVERWRITE TABLE lpfg_wrk.OTIF_VENDORS
SELECT TRIM(avm.SOURCE) AS SOURCE, 
	TRIM(avm.REGION) AS REGION, 
	TRIM(avm.REGION) AS ENV, 
	TRIM(avm.VENDOR) AS LIFNR, 
	TRIM(avm.VTYPE) AS KTOKK, 
	TRIM(avm.VNDAD1) AS STRAS,
	TRIM(avm.VNDAD2) AS ORT01,
	TRIM(avm.VSTATE) AS REGIO, 
	TRIM(avm.VCOUN) AS LAND1,
	TRIM(avm.VPOST) AS ZIPC,
	TRIM(avm.VCITY) AS CITY,
	--TRIM(avm.VNDNAM) AS NAME2,
	TRIM(avm.VCON) AS NAME3, 
	TRIM(avm.VPHONE) AS TELF1, 
	TRIM(avm.VNDNAM) AS NAME1,
	trim(avmr.axl1) AS affiliate,
	trim(EAVMBS) AS SAP_VENDORCODE, 
	'' AS VENDOR_PARENT_HQ
FROM lpfg_stg.AVM avm
LEFT OUTER JOIN lpfg_stg.avmr avmr ON (
		trim(avm.vendor) = trim(avmr.axvend)
		AND trim(avm.region) = trim(avmr.region)
		)
LEFT OUTER JOIN lpfg_stg.EAVM eavm ON (
		regexp_replace(TRIM(avm.VENDOR), "-", "") = regexp_replace(TRIM(eavm.EAVMBN), "-", "")
		AND TRIM(avm.region) = TRIM(eavm.eavmd)
		);

INSERT INTO TABLE lpfg_wrk.OTIF_VENDORS
SELECT DISTINCT "MOVEX" AS SOURCE
	,"HBG" AS region
	,'HBG' AS ENV
	,trim(cidmas.IDSUNO) AS LIFNR
	,'' AS KTOKK
	,trim(zipcode.SAADR2) AS STRAS
	,trim(zipcode.SAADR2) AS ORT01
	,'' AS REGIO
	,trim(IDCSCD) AS LAND1
	,trim(zipcode.SAPONO) AS ZIPC
	,trim(zipcode.SAADR2) AS CITY
	--,trim(IDSUNM) AS NAME2
	,'' AS NAME3
	,'' AS TELF1
	,trim(IDSUNM) AS NAME1
	,""  AS affiliate
	,cidven.IICFI1 AS SAP_VENDORCODE
	,'' AS VENDOR_PARENT_HQ
FROM lpfg_stg.HBG_CIDMAS cidmas
LEFT OUTER JOIN lpfg_stg.HBG_ZIPCODE zipcode ON (trim(cidmas.idsuno) = trim(zipcode.sasuno))
LEFT OUTER JOIN lpfg_stg.HBG_CIDVEN cidven ON (regexp_replace(TRIM(cidven.IISUNO), "-", "") = regexp_replace(trim(cidmas.IDSUNO), "-", ""));

INSERT INTO TABLE lpfg_wrk.OTIF_VENDORS
SELECT DISTINCT "SAP" AS SOURCE
	,"VDR" AS region
	,"VDR" AS env
	,trim(VA.LIFNR) AS lifnr
	,trim(KTOKK) AS ktokk
	,trim(STRAS) AS stras
	,trim(ORT01) AS ort01
	,trim(REGIO) AS regio
	,trim(LAND1) AS land1
	,trim(PSTLZ) AS Zipc
	,trim(ORT01) AS city
	--,trim(NAME2) AS name2
	,trim(NAME3) AS name3
	,trim(TELF1) AS telf1
	,trim(VA.NAME1) AS name1
	,CASE 
		WHEN KTOKK LIKE "%JCIN%"
			THEN "Y"
		ELSE ""
		END AS affiliate
	,"" AS SAP_VENDORCODE
	,trim(VB.LNRZE) AS VENDOR_PARENT_HQ
FROM lpfg_stg.VDR_LFA1 VA
LEFT OUTER JOIN  (select lifnr, max(LNRZE) as LNRZE from lpfg_stg.VDR_LFB1 group by lifnr) VB ON (trim(VA.LIFNR) = trim(VB.LIFNR));

INSERT INTO TABLE lpfg_wrk.OTIF_VENDORS
SELECT DISTINCT "SAP" AS SOURCE
	,"MAD" AS region
	,"MAD" AS env
	,trim(LA.LIFNR) AS lifnr
	,trim(KTOKK) AS ktokk
	,trim(STRAS) AS stras
	,trim(ORT01) AS ort01
	,trim(REGIO) AS regio
	,trim(LAND1) AS land1
	,trim(PSTLZ) AS Zipc
	,trim(ORT01) AS city
	--,trim(NAME2) AS name2
	,trim(NAME3) AS name3
	,trim(TELF1) AS telf1
	,trim(LA.NAME1) AS name1
	,"" AS affiliate
	,"" AS SAP_VENDORCODE
	,trim(LB.LNRZE) AS VENDOR_PARENT_HQ
FROM lpfg_stg.sap_eurmdr_LFA1 LA
LEFT OUTER JOIN (select lifnr, max(LNRZE) as LNRZE from lpfg_stg.sap_eurmdr_LFB1 group by lifnr) LB ON (trim(LA.LIFNR) = trim(LB.LIFNR));
