DROP TABLE IF EXISTS lpfg_stg.OTIF_Madrid_PO_Exclusions;

	CREATE EXTERNAL TABLE lpfg_stg.OTIF_Madrid_PO_Exclusions (
		 Material_code string,
		 Material_description string,
		 Purchase_Doc_No string
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_stg/otif/OTIF_Madrid_PO_Exclusions";

		