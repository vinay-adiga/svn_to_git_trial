Invalidate metadata lpfg_core.OTIF_REASONCODE_INPUT;

INSERT INTO lpfg_core.OTIF_REASONCODE_INPUT (KEY_ID,PLANT,MONTH,PO_NO,PO_LINE,ITEMCODE,SUPPLIERCODE,SUPPLIERNAME,SUPPLIER,MATERIAL_TYPE,Material_Description,ORDD,REASON_CODE,ROOT_CAUSE,DelQnt_Divide_ReqQnt,DelQnt_Sub_ReqQnt)
SELECT CONCAT (PLANT,'_',PO_NO,'_',PO_LINE) AS KEY_ID,
	OT_CALC.PLANT,
	OT_CALC.MONTH,
	OT_CALC.PO_NO,
	OT_CALC.PO_LINE,
	OT_CALC.ITEMCODE,
	OT_CALC.SUPPLIERCODE,
	OT_CALC.SUPPLIERNAME,
	CONCAT (trim(OT_CALC.suppliercode),'-',TRIM(OT_CALC.suppliername)) AS SUPPLIER,
	OT_CALC.MATERIAL_TYPE,
	OT_CALC.Material_Description,
	OT_CALC.ORDD,
	cast(OT_CALC.REASON_CODE AS String),
	CASE WHEN OT_CALC.ROOT_CAUSE IS NULL
	THEN '' 
	ELSE OT_CALC.ROOT_CAUSE
	END AS ROOT_CAUSE,
	round(cast(OT_CALC.DelQnt_Divide_ReqQnt as Decimal), 2) AS DelQnt_Divide_ReqQnt,
	round(cast(OT_CALC.DelQnt_Sub_ReqQnt as decimal), 2) AS DelQnt_Sub_ReqQnt
FROM lpfg_core.OTIF_CALCULATION OT_CALC
LEFT OUTER JOIN (
	SELECT CASE 
			 WHEN length(tenpj) = 1 THEN CONCAT ('0',tenpj)
			ELSE tenpj
		  END AS dtec_month,
		tenpj,
		tenj4 AS dtec_year,
		teny8
	FROM lpfg_stg.DTEC
	WHERE trim(dtec.teddt) = 'Y'
	) dtec ON (concat(substr(ORDD,7,4),substr(ORDD,1,2),substr(ORDD,4,2)) = trim(dtec.teny8))
WHERE CONCAT (
		dtec_year,
		'-',
		dtec_month
		) <= substr(cast(now() AS string), 1, 7)
	AND CONCAT (
		dtec_year,
		'-',
		dtec_month
		) >=substr(cast(add_months(now(), -1) AS string), 1, 7) 
	AND TO_BE_DISPLAYED = 'Y' AND DelQnt_Divide_ReqQnt IS NOT NULL;