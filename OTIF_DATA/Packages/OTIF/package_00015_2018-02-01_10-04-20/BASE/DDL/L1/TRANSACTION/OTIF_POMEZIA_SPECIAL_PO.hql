DROP TABLE IF EXISTS lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO;
	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO (
		PLANT String,
		MATERIAL_NO String,
		MATERIAL_DESC String,
		SUPPLIER_CODE String,
		SUPPLIER_NAME String,
		PO_NO String,
		PO_LINE String,
		type String,
		MONTH String,
		JNJ_MONTH String,
		YEAR String,
		MONTH_YEAR String,
		ORDD Bigint,
		RECEIPT_QUANTITY DOUBLE,
		MISSED_QUANTITY DOUBLE
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/otif/OTIF_POMEZIA_SPECIAL_PO";