INSERT OVERWRITE TABLE LPFG_WRK.OTIF_DELAY_CALC
SELECT PLANT,
	   ITEMCODE,
	   PO_NO,
	   PO_LINE,
	   ORDD,
	   RECEIPT_DATE,
	   RECEIPT_QUANTITY,
	CASE 
		WHEN datediff(from_unixtime(unix_timestamp(RECEIPT_DATE, 'MM/dd/yyyy')), from_unixtime(unix_timestamp(ORDD, 'MM/dd/yyyy'))) <= 0
			AND sum(dtec_rd_od.totaldays) IS NOT NULL
			THEN datediff(from_unixtime(unix_timestamp(RECEIPT_DATE, 'MM/dd/yyyy')), from_unixtime(unix_timestamp(ORDD, 'MM/dd/yyyy'))) + sum(dtec_rd_od.totaldays)
		WHEN datediff(from_unixtime(unix_timestamp(RECEIPT_DATE, 'MM/dd/yyyy')), from_unixtime(unix_timestamp(ORDD, 'MM/dd/yyyy'))) > 0
			AND sum(dtec_od_rd.totaldays) IS NOT NULL
			THEN datediff(from_unixtime(unix_timestamp(RECEIPT_DATE, 'MM/dd/yyyy')), from_unixtime(unix_timestamp(ORDD, 'MM/dd/yyyy'))) - sum(dtec_od_rd.totaldays)
		ELSE datediff(from_unixtime(unix_timestamp(RECEIPT_DATE, 'MM/dd/yyyy')), from_unixtime(unix_timestamp(ORDD, 'MM/dd/yyyy')))
		END AS DELAY,
	datediff(from_unixtime(unix_timestamp(RECEIPT_DATE, 'MM/dd/yyyy')), from_unixtime(unix_timestamp(ORDD, 'MM/dd/yyyy'))) AS OLD_DELAY
FROM lpfg_wrk.OTIF_DETAILS details
LEFT OUTER JOIN (
	SELECT count(*) AS totaldays,
		teny8
	FROM lpfg_stg.dtec
	WHERE trim(teddt) = 'Y'
		AND tennw IN (
			'6',
			'7'
			)
	GROUP BY teny8
	) dtec_od_rd 
ON dtec_od_rd.teny8 >= CONCAT (
		substr(ORDD, 7, 4),
		substr(ORDD, 1, 2),
		substr(ORDD, 4, 2)
		)
	AND teny8 <= CONCAT (
		substr(RECEIPT_DATE, 7, 4),
		substr(RECEIPT_DATE, 1, 2),
		substr(RECEIPT_DATE, 4, 2)
		)
LEFT OUTER JOIN (
	SELECT count(*) AS totaldays,
		teny8
	FROM lpfg_stg.dtec
	WHERE trim(teddt) = 'Y'
		AND tennw IN (
			'6',
			'7'
			)
	GROUP BY teny8
	) dtec_rd_od 
ON dtec_rd_od.teny8 >= CONCAT (
		substr(RECEIPT_DATE, 7, 4),
		substr(RECEIPT_DATE, 1, 2),
		substr(RECEIPT_DATE, 4, 2)
		)
	AND dtec_rd_od.teny8 <= CONCAT (
		substr(ORDD, 7, 4),
		substr(ORDD, 1, 2),
		substr(ORDD, 4, 2)
		)
GROUP BY PLANT,
	   ITEMCODE,
	   PO_NO,
	   PO_LINE,
	   ORDD,
	   RECEIPT_DATE,
	   RECEIPT_QUANTITY;