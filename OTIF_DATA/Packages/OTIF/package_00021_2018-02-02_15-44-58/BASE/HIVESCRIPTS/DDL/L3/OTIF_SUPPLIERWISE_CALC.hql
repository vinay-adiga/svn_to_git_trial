DROP TABLE IF EXISTS lpfg_core.OTIF_SUPPLIERWISE_CALC;

CREATE EXTERNAL TABLE lpfg_core.OTIF_SUPPLIERWISE_CALC (
		plant STRING,
		SupplierCode string,
		SupplierName string,
		SUPPLIER string,
		Month string,
		Year string,
		Month_year string,
		ordq_sum DOUBLE,
		Cumulative_Sum DOUBLE,
		Cumulative_Sum_RC DOUBLE,
		InFull_Mon_Supplier DOUBLE,
		InFull_Mon_Supplier_RC DOUBLE,
		OTIF_Month_Supplier DOUBLE,
		OTIF_Month_Supplier_RC DOUBLE,
		ordq_sum_all DOUBLE,
		SUM_ORDQ_SUM_ALL DOUBLE,
		Cumulative_Sum_all DOUBLE,
		Cumulative_Sum_all_RC DOUBLE,
		YTD_InFull_Supplier DOUBLE,
		YTD_InFull_Supplier_RC DOUBLE,
		YTD_OTIF_Supplier DOUBLE,
		YTD_OTIF_Supplier_RC DOUBLE,
		target STRING,
		SUPPLIER_SLOTIF DOUBLE,
		YTD_SUPPLIER_SLOTIF DOUBLE,
		SUPPLIER_SLOTIF_RC DOUBLE,
		YTD_SUPPLIER_SLOTIF_RC DOUBLE
		) STORED AS PARQUET LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_core/OTIF_SUPPLIERWISE_CALC";