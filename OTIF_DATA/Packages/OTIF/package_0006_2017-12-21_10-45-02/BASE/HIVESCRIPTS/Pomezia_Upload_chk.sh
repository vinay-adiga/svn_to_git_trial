#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE_PO=$4
QUERY_FILE_INPUT=$5
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

 echo "Connecting to HBASE Shell and truncating ITM Special PO Input table"
#hbase shell
echo "truncate 'lpfg_wrk:OTIF_POMEZIA_SPECIAL_PO_INPUT'" | hbase shell >>ITM_INPUT.txt 2>&1

hadoop fs -put -f ITM_INPUT.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

#Running and checking count(*) of ITM SPECIAL PO Script
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO">ITM_PO_COUNT_log

ITM_PO_COUNT=$(cat ITM_PO_COUNT_log|grep -Ewo "[0-9]?[0-9]")
echo "$ITM_PO_COUNT"

if [ $ITM_PO_COUNT == 0 ];
then echo "Count of OTIF_POMEZIA_SPECIAL_PO is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_PO
  
 else
    echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO Script"
fi

#Running and checking count(*) of ITM SPECIAL PO Input Script
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">ITM_PO_INPUT_COUNT_log

ITM_PO_INPUT_COUNT=$(cat ITM_PO_INPUT_COUNT_log|grep -Ewo "[0-9]?[0-9]")

echo "$ITM_PO_INPUT_COUNT"

if [ $ITM_PO_INPUT_COUNT == 0 ];
then echo "Count of OTIF_POMEZIA_SPECIAL_PO_INPUT is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT
  
 else
    echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select tenpj from lpfg_stg.DTEC where  teddt='Y' and teny8=regexp_replace(substr(cast(now() AS string), 1, 10),'-','')">log

JNJMONTH=$(cat log|grep -Ewo "[0-9]?[0-9]")
echo "$JNJMONTH"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select distinct JNJ_MONTH from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO">ITM_PO_log

ITM_MONTH=$(cat ITM_PO_log|grep -Ewo "[0-9]?[0-9]")
echo "$ITM_MONTH"

count=`expr $JNJMONTH - 2`
echo "$count"

if [ $count == $ITM_MONTH ];
then echo "JNJ Month is :" $JNJMONTH  and "Current Month is :" $ITM_MONTH "Need to run OTIF_POMEZIA_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_PO
  
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO Script"
fi

# Checking Month of ITM Special PO Input Script and comparing with DTEC table to get JNJ Calendar running month
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select distinct JNJ_MONTH from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">ITM_PO_INPUT_log

ITM_INPUT_MONTH=$(cat ITM_PO_INPUT_log|grep -Ewo "[0-9]?[0-9]")
echo "$ITM_INPUT_MONTH"

count=`expr $JNJMONTH - 2`
echo "$count"

if  [ $count == $ITM_INPUT_MONTH ];

then echo "JNJ Month is :$JNJMONTH and Current Month is :"$ITM_INPUT_MONTH "So,need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi