INSERT OVERWRITE TABLE lpfg_wrk.OTIF_VDR_SPECIAL_PO
SELECT trim(ekpo.REGION) AS PLANT,
	trim(ekpo.matnr) AS MATERIAL_NO,
	CASE 
		WHEN makt_E.matnr IS NOT NULL
			THEN trim(makt_E.MAKTX)
		WHEN makt_F.matnr IS NOT NULL
			THEN trim(makt_F.MAKTX)
	END AS MATERIAL_DESC,
	trim(ekko.lifnr) AS SUPPLIER_CODE,
	trim(lfa1.NAME1) AS SUPPLIER_NAME,
	trim(ekbe.ebeln) AS PO_NO,
	trim(ekbe.ebelp) AS PO_LINE,
	'Special' AS type,
	CASE 
		WHEN dtec.tenpj = '1'
			THEN CONCAT ('Jan')
		WHEN dtec.tenpj = '2'
			THEN CONCAT ('Feb')
		WHEN dtec.tenpj = '3'
			THEN CONCAT ('Mar')
		WHEN dtec.tenpj = '4'
			THEN CONCAT ('Apr')
		WHEN dtec.tenpj = '5'
			THEN CONCAT ('May')
		WHEN dtec.tenpj = '6'
			THEN CONCAT ('Jun')
		WHEN dtec.tenpj = '7'
			THEN CONCAT ('Jul')
		WHEN dtec.tenpj = '8'
			THEN CONCAT ('Aug')
		WHEN dtec.tenpj = '9'
			THEN CONCAT ('Sep')
		WHEN dtec.tenpj = '10'
			THEN CONCAT ('Oct')
		WHEN dtec.tenpj = '11'
			THEN CONCAT ('Nov')
		WHEN dtec.tenpj = '12'
			THEN CONCAT ('Dec')
		ELSE ''
		END AS MONTH,
	cast(dtec.tenpj AS INT) AS JNJ_MONTH,
	dtec.year AS YEAR,
	MAX(cast(trim(regexp_replace(ekbe.BUDAT, "-", ""))as bigint)) AS ORDD,
	SUM(cast(ekbe.menge AS DOUBLE)) AS RECEIPT_QUANTITY,
	0 AS MISSED_QUANTITY
--SUM(cast(ith.TQTY AS DOUBLE)) / (SUM(cast(ith.TQTY AS DOUBLE)) + 0) AS OTIF
FROM (SELECT * FROM lpfg_stg.VDR_EKBE where trim(BEWTP) = 'E') ekbe
JOIN (
	SELECT ebeln,
		ebelp,
		count(*)
	FROM lpfg_stg.VDR_EKET
	GROUP BY ebeln,
		ebelp
	HAVING count(*) > 1
	) EKET_MULTIPLE ON (
		trim(ekbe.EBELN) = trim(EKET_MULTIPLE.EBELN)
		AND trim(ekbe.EBELP) = trim(EKET_MULTIPLE.EBELP)
		)
LEFT OUTER JOIN lpfg_stg.VDR_EKPO ekpo ON (
		trim(ekpo.EBELN) = trim(EKET_MULTIPLE.EBELN)
		AND trim(ekpo.EBELP) = trim(EKET_MULTIPLE.EBELP)
		AND ekpo.werks = '0001'
		AND ekpo.loekz <> 'L'
		)
JOIN (select * from lpfg_stg.VDR_EKKO WHERE BSART in('NB' , 'CO') and loekz <> 'L' and EKORG in('0001' , 'CONS') )ekko ON 
							(trim(ekbe.EBELN) = trim(ekko.EBELN))
LEFT OUTER JOIN lpfg_stg.VDR_MAKT makt_E ON (
		trim(ekpo.matnr) = trim(makt_E.matnr)
		AND trim(makt_E.spras) = 'E'
		)
LEFT OUTER JOIN lpfg_stg.VDR_MAKT makt_F ON (
		trim(ekpo.matnr) = trim(makt_F.matnr)
		AND trim(makt_F.spras) = 'F'
		)
LEFT OUTER JOIN lpfg_stg.VDR_LFA1 lfa1 ON ( trim(ekko.lifnr) = trim(lfa1.lifnr))							
LEFT OUTER JOIN (
	SELECT CASE 
			WHEN length(tenpj) = 1
				THEN CONCAT (
						'0',
						tenpj
						)
			ELSE tenpj
			END AS month,
		tenpj,
		tenj4 AS year,
		teny8
	FROM lpfg_stg.DTEC
	WHERE trim(dtec.teddt) = 'Y'
	) dtec ON (trim(regexp_replace(ekbe.BUDAT, "-", "")) = trim(dtec.teny8))
WHERE CONCAT (
		year,
		'-',
		month
		) < substr(cast(now() AS string), 1, 7)
	AND CONCAT (
		year,
		'-',
		month
		) > substr(cast(add_months(now(), - 2) AS string), 1, 7)
GROUP BY PLANT,
	MATERIAL_NO,
	makt_E.matnr,
	makt_F.matnr,
	SUPPLIER_CODE,
	--SUPPLIER_NAME,
	ekbe.ebeln,
	ekbe.ebelp,
	lfa1.name1,
	makt_E.maktx,
	makt_F.maktx,
	tenpj,
	dtec.year;