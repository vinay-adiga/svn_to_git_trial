#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE_PO=$4
QUERY_FILE_INPUT=$5
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

#Running and checking count(*) of POMEZIA SPECIAL PO Script
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO">POMEZIA_PO_COUNT_log

POMEZIA_PO_COUNT=$(cat POMEZIA_PO_COUNT_log|grep -Ewo "[0-9]*")
echo "$POMEZIA_PO_COUNT"

if [ $POMEZIA_PO_COUNT == 0 ];
then echo "Count of OTIF_POMEZIA_SPECIAL_PO is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_PO
  
 else
    echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO Script"
fi

#Running and checking count(*) of POMEZIA SPECIAL PO Input Script
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">POMEZIA_PO_INPUT_COUNT_log

POMEZIA_PO_INPUT_COUNT=$(cat POMEZIA_PO_INPUT_COUNT_log|grep -Ewo "[0-9]*")

echo "$POMEZIA_PO_INPUT_COUNT"

if [ $POMEZIA_PO_INPUT_COUNT == 0 ];
then echo "Count of OTIF_POMEZIA_SPECIAL_PO_INPUT is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT
  
 else
    echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -V -q "select distinct regexp_replace(substr(cast(add_months(cast (concat(substr(TENY8,1,4),'-',substr(TENY8,5,2),'-','01') as timestamp), - 1) AS string), 1, 7),'-','') AS DTEC_MONTH_YEAR from lpfg_STG.DTEC where  teddt='Y' and teny8=regexp_replace(substr(cast(now() AS string), 1, 10),'-','')">ITM_log

hadoop fs -put -f ITM_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

JNJMONTH=$(cat  ITM_log|grep -Ewo "[0-9]*")
echo "$JNJMONTH"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -V -q "select distinct regexp_replace(substr(cast(add_months(cast (concat(substr(MONTH_YEAR,1,4),'-',substr(month_year,6,2),'-','01') as timestamp), - 0) AS string), 1, 7),'-','') from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO">POMEZIA_PO_log

hadoop fs -put -f POMEZIA_PO_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

POMEZIA_MONTH=$(cat POMEZIA_PO_log|grep -Ewo "[0-9][0-9][0-9][0-9]*")
echo "$POMEZIA_MONTH"

if [ $JNJMONTH != $POMEZIA_MONTH ];

then 
echo "JNJ Month is :" $JNJMONTH  and "Current Month is :" $POMEZIA_MONTH "Need to run OTIF_POMEZIA_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_PO >POMEZIA_PO_Script_log 2>&1

echo "Check"
hadoop fs -put -f POMEZIA_PO_Script_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo "Connecting to HBASE Shell and truncating POMEZIA Special PO Input table"
#hbase shell
echo "truncate 'lpfg_wrk:OTIF_POMEZIA_SPECIAL_PO_INPUT'" | hbase shell >>POMEZIA_HBase_INPUT.txt 2>&1

hadoop fs -put -f POMEZIA_HBase_INPUT.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo "JNJ Month is :"$JNJMONTH "and Current Month is :"$POMEZIA_INPUT_MONTH "So,need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT >POMEZIA_PO_INPUT_Script_log 2>&1

echo "Check Input "
hadoop fs -put -f POMEZIA_PO_INPUT_Script_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs
 
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO Script and OTIF_POMEZIA_SPECIAL_PO_INPUT Script "
fi