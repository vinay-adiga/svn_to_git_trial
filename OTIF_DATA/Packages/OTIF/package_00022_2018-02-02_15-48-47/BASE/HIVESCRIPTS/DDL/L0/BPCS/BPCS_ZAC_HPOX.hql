DROP TABLE IF EXISTS lpfg_stg.BPCS_ZAC_HPOX;

CREATE EXTERNAL TABLE lpfg_stg.BPCS_ZAC_HPOX(
PXORD  string,
PXPLIN string,
PXITEM string,
PXOQTY double,
PXUOM string,
PXEDTE string,
PXRDTE string,
PXREQ  string,
PXECST string,
PXVTXC  string,
PXITXC  string,
PXFOBC  string,
PXFOBP  string,
PXPSRC  string,
PXLIST string)
ROW format delimited fields terminated BY "|" LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/BPCS/TRANSACTION/ZAC/HPOX';