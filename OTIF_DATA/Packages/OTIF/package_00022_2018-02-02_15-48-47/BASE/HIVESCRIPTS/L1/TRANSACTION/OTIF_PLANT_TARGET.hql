INSERT OVERWRITE TABLE lpfg_wrk.OTIF_PLANT_TARGET
SELECT 
CASE 
     when  TRIM(PLANT) LIKE '%Pomezia%' THEN 'ITM'
	 when  TRIM(PLANT) LIKE '%Helsingborg%' THEN 'HBG'
	 when  TRIM(PLANT) LIKE '%Mandra%' THEN "GRC"
	 when  TRIM(PLANT) LIKE '%Val de reuil%' THEN "VDR"
	 when  TRIM(PLANT) LIKE '%Sezanne%' THEN "FRM"
	 when  TRIM(PLANT) LIKE '%Wuppertal%' THEN "DEM"
	 when  TRIM(PLANT) LIKE '%Cairo%' THEN "CAI"
	 when TRIM(PLANT) LIKE '%Madrid%' THEN "MAD"
	 when TRIM(PLANT) LIKE '%East London%' THEN "ZA01"
	 when  TRIM(PLANT) LIKE '%Cape Town%' THEN "ZA02"
END as PLANT,
  TARGET
from lpfg_stg.OTIF_TARGET ;	   