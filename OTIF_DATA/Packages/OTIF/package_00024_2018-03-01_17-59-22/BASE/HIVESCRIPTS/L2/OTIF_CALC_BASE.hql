INSERT  OVERWRITE TABLE lpfg_wrk.OTIF_CALC_BASE

SELECT detls_tol_l2.*,
    ( CUM_SUM_RECEIPT_QTY_CALC/detls_tol_l2.ordq) *100 as percentage_ordq,
    ( CUM_SUM_RECEIPT_QTY/detls_tol_l2.ordq) *100 as Percentage_ORDQ_RC
from (
SELECT po_sku,
       po_sku_ordd,
       CASE
           WHEN receipt_quantity IS NOT NULL THEN receipt_quantity
           ELSE 0
       END AS receipt_quantity,
	   DelQnt_Divide_ReqQnt , -- Delivered Qnt/Requested Qnt 
	   DelQnt_Sub_ReqQnt, -- Delivered Qnt - Requested Qnt
	   po_line,
       itemcode,
       po_no,
       ordd,
       cast(year(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy')))as string) AS YEAR,
       cast(month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy')))as string) AS MONTH,
       suppliercode,
       suppliername,
       PLANT AS plant,
       ordq,
	   ORDER_CREATION_DATE,
       receipt_date,
       itemdesc,
       po_uom,
       incoterms,
	   INCOTERMS_DESC,
       gr_uom,
       plantdesc,
       delay,
       --supplier_code,
       days_early_tolerance,
       days_end_tolerance,
       ot1,
       RECEIPT_QTY_CALC AS ReceivedOnTime, 
	   SUM(RECEIPT_QTY_CALC) OVER (PARTITION BY po_sku_ordd -- changed for ABFY-6050
                                               ORDER BY Receipt_Date
											   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS CUM_SUM_RECEIPT_QTY_CALC,
												
       SUM(receipt_quantity) OVER (PARTITION BY po_sku_ordd
                                                     ORDER BY Receipt_Date  -- changed for ABFY-6050
													  ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
													  AS CUM_SUM_RECEIPT_QTY ,												
       row_number() over (partition BY po_sku_ordd
                                               ORDER BY Receipt_Date -- changed for ABFY-6050
											   ) AS num_del,
	   
       cast(STANDARDLT AS bigint) AS STANDARDLT ,
       trim(MATERIAL_TYPE) AS Material_Type ,
       trim(PO_TYPE)AS PO_TYPE,
	   PO_CLASSIFICATION AS PO_CLASSIFICATION,
	   trim(PROCUREMENT_TYPE) AS PROCUREMENT_TYPE,
	   low_qty_tol,	
       high_qty_tol		

FROM
  (SELECT details_tol.*,
          IF (details_tol.ot1 = 1,
              details_tol.receipt_quantity ,
              0 ) AS RECEIPT_QTY_CALC
			  
   FROM
     ( SELECT details.*,
              days_early_tolerance,
              days_end_tolerance,
              CASE
                  WHEN
                  ( (-1 * cast(days_early_tolerance AS int)) <= cast(details.DELAY AS INT)
                     AND cast(details.DELAY AS INT) <= cast(tol.days_end_tolerance AS INT))THEN 1
                  ELSE 0
              END AS ot1,
			  
	   5 as low_qty_tol,
	   10 as high_qty_tol
			  
       FROM
        (SELECT CONCAT (dtls.po_no,'_',dtls.itemcode) AS po_sku ,
                CONCAT (dtls.po_no,'_',dtls.itemcode,'_' ,CASE
                                                    WHEN dtls.ordd IS NULL THEN '0'
                                                    ELSE cast(regexp_replace(dtls.ordd,'/','') AS string)
                                                END, '_',trim(dtls.po_line)) AS po_sku_ordd,
                dtls.receipt_quantity,
				dtls.DelQnt_Divide_ReqQnt, -- Delivered Qnt/Requested Qnt  
				dtls.DelQnt_Sub_ReqQnt , -- Delivered Qnt - Requested Qnt
				dtls.ITEMCODE,
                dtls.PO_NO,
				dtls.PO_LINE,
                dtls.ordd,
				year,
                suppliercode,
                suppliername,
                dtls.PLANT,
                ordq,
				ORDER_CREATION_DATE,
                dtls.Receipt_Date,
                itemdesc,
                po_uom,
                INCOTERMS,
				INCOTERMS_DESC,
                GR_UOM,
                plantdesc,
                cast(calc.delay as INT) AS DELAY,
                STANDARDLT,
                Material_Type,
                PO_TYPE,
				PO_CLASSIFICATION,
				PROCUREMENT_TYPE
         FROM lpfg_wrk.OTIF_DETAILS dtls
		 LEFT OUTER JOIN lpfg_wrk.OTIF_DELAY_CALC calc ON (
		 dtls.PLANT = calc.PLANT
		 AND dtls.ITEMCODE = calc.ITEMCODE
		 AND dtls.PO_NO = calc.PO_NO
	     AND dtls.PO_LINE = calc.PO_LINE
	     AND dtls.ORDD = calc.ORDD
	     AND dtls.RECEIPT_DATE = calc.RECEIPT_DATE
	     AND dtls.RECEIPT_QUANTITY = calc.RECEIPT_QUANTITY)) details
      LEFT OUTER JOIN
        (SELECT DISTINCT * FROM lpfg_wrk.OTIF_TOLERANCE) tol ON (    
					 details.PLANT=tol.plant
                 AND details.suppliercode=tol.supplier_codes
				 AND details.PO_NO=tol.PO_NO
				 AND details.YEAR=tol.YEAR )
	)details_tol) detls_tol_l1 )detls_tol_l2;