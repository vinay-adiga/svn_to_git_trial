DROP TABLE IF EXISTS lpfg_core.OTIF_REASONCODE_INPUT;
	CREATE EXTERNAL TABLE lpfg_core.OTIF_REASONCODE_INPUT (
		KEY_ID String,
		PLANT String,
		MONTH String,
		PO_NO String,
		PO_LINE String,
		ITEMCODE String,
		SUPPLIERCODE String,
		SUPPLIERNAME String,
		SUPPLIER String,
		MATERIAL_TYPE String,
		Material_Description String,
		ORDD String,
		REASON_CODE String,
		ROOT_CAUSE String,
		DelQnt_Divide_ReqQnt DECIMAL(20,2), -- Delivered Qnt/Requested Q
		DelQnt_Sub_ReqQnt DECIMAL(20,2) -- Delivered Qnt - Requested Qnt
		) STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
		WITH SERDEPROPERTIES('hbase.columns.mapping' = ':key,cf:PLANT,cf:MONTH,cf:PO_NO,cf:PO_LINE,cf:ITEMCODE,cf:SUPPLIERCODE,cf:SUPPLIERNAME,cf:SUPPLIER,cf:MATERIAL_TYPE,cf:Material_Description,cf:ORDD,cf:REASON_CODE,cf:ROOT_CAUSE,cf:DelQnt_Divide_ReqQnt,cf:DelQnt_Sub_ReqQnt') TBLPROPERTIES('hbase.table.name' = 'lpfg_core:OTIF_REASONCODE_INPUT');