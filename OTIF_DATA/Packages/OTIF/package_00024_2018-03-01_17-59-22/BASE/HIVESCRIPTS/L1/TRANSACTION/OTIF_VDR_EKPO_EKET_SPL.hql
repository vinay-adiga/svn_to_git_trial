--Added Deletion Indicator (LOEKZ) of Purchase Order Line Item
-- Changed coding for UOM Conversion and Quantity conversion for VDR by arath3 on 12.05.2017 for ABFY-6346 and ABFY-6423
INSERT OVERWRITE TABLE lpfg_wrk.OTIF_VDR_EKPO_EKET_SPL
  SELECT  "SAP" AS SOURCE,
	"VDR" AS region,
	"VDR" AS env,
	"VDR" AS werks,
	trim(ekpo.MATNR) AS MATNR,
	trim(ekpo.EBELN) AS EBELN,
	trim(ekpo.EBELP) AS EBELP,
	trim(ekpo.BPRME) AS GR_UOM,
	CONCAT (
		trim(ekpo.EBELN),
		trim(ekpo.EBELP)
		) AS EBELC,
	TRIM(eket.EINDT) AS REQUESTED_DATE,
	CASE 
		WHEN ekpo.PEINH = '0'
			THEN trim(ekpo.NETPR)
		ELSE cast(cast(trim(ekpo.NETPR) AS DOUBLE) / (cast(trim(ekpo.PEINH) AS DOUBLE)) AS string)
		END AS NETPR,
	cast(cast(trim(ekpo.BPUMN) AS DOUBLE) / (cast(trim(ekpo.BPUMZ) AS DOUBLE)) AS string) AS PEINH,
	CASE 
	   WHEN trim(ekpo.BPRME) = 'G'
	      Then cast((CAST(trim(ekpo.MENGE) AS DOUBLE) / 1000)as string)
	   WHEN trim(ekpo.BPRME) IN('MI','TO','KM')
	      Then cast((CAST(trim(ekpo.MENGE) AS DOUBLE) * 1000)as string)
	Else trim(ekpo.MENGE)
	END AS PQORD,
	CASE 
		WHEN trim(ekpo.BPRME) = 'G' OR trim(ekpo.BPRME) = 'TO'
			THEN 'KG'
		WHEN trim(ekpo.BPRME) = 'MI'
			THEN 'UN'
		WHEN trim(ekpo.BPRME) = 'KM'
			THEN 'M'
		ELSE trim(ekpo.BPRME)
		END AS PUM,
	CASE 
		WHEN ekpo.PEINH = '0'
			THEN trim(ekpo.NETPR)
		ELSE cast(cast(trim(ekpo.NETPR) AS DOUBLE) / (cast(trim(ekpo.PEINH) AS DOUBLE)) AS string)
		END AS PECST,
	trim(ekpo.BPRME) AS MEINS,
	trim(ekpo.NETWR) AS DMBTR,
	trim(eket.EINDT) AS RDD,
	CASE 
	   WHEN trim(ekpo.BPRME) = 'G'
	      Then eket.SUM_MENGE / 1000
	   WHEN trim(ekpo.BPRME) IN('MI','TO','KM')
	      Then eket.SUM_MENGE * 1000
	Else eket.SUM_MENGE
	END AS RDQ,
	--trim(eket.MENGE) AS RDQ,
	trim(ekpo.MEINS) AS RDQ_UOM,
	trim(ekpo.MEINS) AS RECEIPT_QTY_UOM,
	trim(ekpo.PLIFZ) AS STD_LEAD_TIME
FROM lpfg_stg.VDR_EKPO ekpo  
LEFT OUTER JOIN (select ebeln,ebelp,eindt,sum(cast(trim(menge)as double))AS SUM_MENGE from lpfg_stg.VDR_EKET group by ebeln,ebelp,eindt)eket ON (
								trim(regexp_replace(ekpo.EBELN, "-", "")) = trim(regexp_replace(eket.EBELN, "-", ""))
							AND trim(regexp_replace(ekpo.EBELP, "-", "")) = trim(regexp_replace(eket.EBELP, "-", ""))
										)
JOIN (select ebeln, ebelp, count(*) from lpfg_stg.VDR_EKET group by ebeln, ebelp having count(*) > 1) EKET_MULTIPLE on (
								trim(regexp_replace(ekpo.EBELN, "-", "")) = trim(regexp_replace(EKET_MULTIPLE.EBELN, "-", ""))
							AND trim(regexp_replace(ekpo.EBELP, "-", "")) = trim(regexp_replace(EKET_MULTIPLE.EBELP, "-", ""))
										)
WHERE ekpo.werks = '0001' and SUBSTRING(LTRIM(RTRIM(ekpo.AEDAT)),1,4)>='2013' and ekpo.loekz <> 'L';