#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

#Checking Sum of Missed Quantity of POMEZIA Input script and if the same is > 0 then running POMEZIA Input script else no need to run the script.
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select sum(MISSED_QUANTITY) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">POMEZIA_SUM_MISS_QTY_log

hadoop fs -put -f POMEZIA_SUM_MISS_QTY_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

POMEZIA_SUM_MISSED_QTY=$(cat POMEZIA_SUM_MISS_QTY_log|grep -Ewo "[0-9]*")

echo "$POMEZIA_SUM_MISSED_QTY"

#Cheking Month_year 
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(MONTH_YEAR) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY ">POMEZIA_PO_HIST_Month_Log

hadoop fs -put -f POMEZIA_PO_HIST_Month_Log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

#PO_HIST_Month=$(cat PO_HIST_Month_Log|grep -Ewo "[A-Z][a-z]*?.[0-9]*")
POMEZIA_PO_HIST_Month=$(cat POMEZIA_PO_HIST_Month_Log|grep -Ewo "[0-9]*")
echo "$POMEZIA_PO_HIST_Month"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(MONTH_YEAR) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT ">POMEZIA_PO_INPUT_Month_Log

hadoop fs -put -f POMEZIA_PO_INPUT_Month_Log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

#PO_INPUT_Month=$(cat PO_INPUT_Month_Log|grep -Ewo "[A-Z][a-z]*?.[0-9]*")
POMEZIA_PO_INPUT_Month=$(cat POMEZIA_PO_INPUT_Month_Log|grep -Ewo "[0-9]*")
echo "$POMEZIA_PO_INPUT_Month"

 echo "$POMEZIA_PO_INPUT_Month"
if [ $POMEZIA_SUM_MISSED_QTY -gt 0 ];
then
echo "Client has updated Missed quantity for Current Month..So need to Overwrite current month Missed Quantity Values"
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "INSERT OVERWRITE TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY select * from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY WHERE month_year <>'$POMEZIA_PO_INPUT_Month' UNION select KEY_ID,PLANT,MATERIAL_NO,MATERIAL_DESC,SUPPLIER_CODE,SUPPLIER_NAME,PO_NO,PO_LINE,type,JNJ_MONTH,MONTH,YEAR,MONTH_YEAR,ORDD,RECEIPT_QUANTITY,MISSED_QUANTITY  from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT where month_year = '$POMEZIA_PO_INPUT_Month'">>itm_hist.txt 2>&1 

 echo "***1**"
hadoop fs -put -f itm_hist.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs
 
 echo "Check"
 else
	echo "History table has correct data no need to run"
fi

echo "Taking Backup of Pomezia PO History Table"
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "INSERT OVERWRITE TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY_BACK_UP SELECT * FROM lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY">>itm_hist_bkp_up.txt 2>&1 

 echo "OTIF_POMEZIA_SPECIAL_PO_HISTORY_BACK_UP Script ran successfully"
hadoop fs -put -f itm_hist_bkp_up.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs