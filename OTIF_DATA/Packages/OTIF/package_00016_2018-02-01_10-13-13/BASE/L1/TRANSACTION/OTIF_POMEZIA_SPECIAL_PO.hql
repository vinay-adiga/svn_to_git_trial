INSERT OVERWRITE TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO
SELECT 	trim(ith.REGION) AS PLANT,
	trim(ith.TPROD) AS MATERIAL_NO,
	CONCAT (
		trim(iim.idesc),
		trim(iim.idsce)
		) AS MATERIAL_DESC,
	trim(iim.IVEND) AS SUPPLIER_CODE,
	TRIM(avm.VNDNAM) AS SUPPLIER_NAME,
	CONCAT (
		trim(ith.TPROD),'_',
		trim(iim.IVEND)
		)AS PO_NO,
	CONCAT (
		trim(dtec.tenpj),'_',
		trim(dtec.year)
		)AS PO_LINE,
		'Consignment' AS type,
	CASE 
		WHEN dtec.tenpj = '1'
			THEN CONCAT ('Jan')
		WHEN dtec.tenpj = '2'
			THEN CONCAT ('Feb')
		WHEN dtec.tenpj = '3'
			THEN CONCAT ('Mar')
		WHEN dtec.tenpj = '4'
			THEN CONCAT ('Apr')
		WHEN dtec.tenpj = '5'
			THEN CONCAT ('May')
		WHEN dtec.tenpj = '6'
			THEN CONCAT ('Jun')
		WHEN dtec.tenpj = '7'
			THEN CONCAT ('Jul')
		WHEN dtec.tenpj = '8'
			THEN CONCAT ('Aug')
		WHEN dtec.tenpj = '9'
			THEN CONCAT ('Sep')
		WHEN dtec.tenpj = '10'
			THEN CONCAT ('Oct')
		WHEN dtec.tenpj = '11'
			THEN CONCAT ('Nov')
		WHEN dtec.tenpj = '12'
			THEN CONCAT ('Dec')
		ELSE ''
		END AS MONTH,	
	dtec.month AS JNJ_MONTH,
	dtec.year AS YEAR,
	MAX(CONCAT (year,'-',month)) as MONTH_YEAR,
	MAX(CAST(TTDTE AS BIGINT) + 19280000) AS ORDD,
	SUM(cast(ith.TQTY AS DOUBLE)) AS RECEIPT_QUANTITY,
	0 AS MISSED_QUANTITY
	--SUM(cast(ith.TQTY AS DOUBLE)) / (SUM(cast(ith.TQTY AS DOUBLE)) + 0) AS OTIF
FROM lpfg_stg.ITH ith
JOIN lpfg_stg.IIM iim ON (
		trim(ith.TPROD) = trim(iim.IPROD)
		AND trim(ith.SOURCE) = trim(iim.SOURCE)
		AND trim(ith.REGION) = trim(iim.REGION)
		)
LEFT OUTER JOIN lpfg_stg.AVM avm ON (
		trim(iim.IVEND) = trim(avm.VENDOR)
		AND trim(iim.SOURCE) = trim(avm.SOURCE)
		AND trim(iim.REGION) = trim(avm.REGION)
		)
LEFT OUTER JOIN lpfg_stg.ITE ON (
		ITE.SOURCE = ITH.SOURCE
		AND ITE.REGION = ITH.REGION
		AND ITE.TTYPE = ITH.TTYPE
		AND TRIM(ITE.TAPO) = 'Y'
		)
LEFT OUTER JOIN (
	SELECT CASE 
			 WHEN length(tenpj) = 1 THEN CONCAT ('0',tenpj)
			ELSE tenpj
		  END AS month,
		tenpj,
		tenj4 AS year,
		teny8
	FROM lpfg_stg.DTEC
	WHERE trim(dtec.teddt) = 'Y'
	) dtec ON ((CAST(TTDTE AS BIGINT) + 19280000) = CAST(trim(dtec.teny8) AS BIGINT))
WHERE trim(ith.ttype) IN ('ES')
	AND CONCAT (year,'-',month) < substr(cast(now() AS string), 1, 7)
	AND CONCAT (
		year,
		'-',
		month
		) > substr(cast(add_months(now(), - 2) AS string), 1, 7) 
GROUP BY PLANT,
	MATERIAL_NO,
	SUPPLIER_CODE,
	SUPPLIER_NAME,
	iim.idesc,
	iim.idsce,
	dtec.month,
	tenpj,
	dtec.year;
	
INSERT INTO TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO
SELECT trim(ith.REGION) AS PLANT,
	trim(ith.TPROD) AS MATERIAL_NO,
	CONCAT (
		trim(iim.idesc),
		trim(iim.idsce)
		) AS MATERIAL_DESC,
	trim(iim.IVEND) AS SUPPLIER_CODE,
	TRIM(avm.VNDNAM) AS SUPPLIER_NAME,
	trim(hpo.PORD) AS PO_NO,
	CONCAT (
		trim(dtec.tenpj),'_',
		trim(dtec.year)
		)AS PO_LINE,
		PODEST as type,
	CASE 
		WHEN dtec.tenpj = '1'
			THEN CONCAT ('Jan')
		WHEN dtec.tenpj = '2'
			THEN CONCAT ('Feb')
		WHEN dtec.tenpj = '3'
			THEN CONCAT ('Mar')
		WHEN dtec.tenpj = '4'
			THEN CONCAT ('Apr')
		WHEN dtec.tenpj = '5'
			THEN CONCAT ('May')
		WHEN dtec.tenpj = '6'
			THEN CONCAT ('Jun')
		WHEN dtec.tenpj = '7'
			THEN CONCAT ('Jul')
		WHEN dtec.tenpj = '8'
			THEN CONCAT ('Aug')
		WHEN dtec.tenpj = '9'
			THEN CONCAT ('Sep')
		WHEN dtec.tenpj = '10'
			THEN CONCAT ('Oct')
		WHEN dtec.tenpj = '11'
			THEN CONCAT ('Nov')
		WHEN dtec.tenpj = '12'
			THEN CONCAT ('Dec')
		ELSE ''
		END AS MONTH,	
	dtec.month AS JNJ_MONTH,
	dtec.year AS YEAR,
	MAX(CONCAT (year,'-',month)) as MONTH_YEAR,
	MAX(CAST(TTDTE AS BIGINT) + 19280000) AS ORDD,
	SUM(cast(ith.TQTY AS DOUBLE)) AS RECEIPT_QUANTITY,
	0 AS MISSED_QUANTITY
	--SUM(cast(ith.TQTY AS DOUBLE)) / (SUM(cast(ith.TQTY AS DOUBLE)) + 0) AS OTIF
FROM (SELECT PODEST,PID,PQREC,trim(PORD) AS PORD,trim(pprod) as pprod,PLINE,SOURCE,REGION FROM lpfg_stg.HPO where PODEST like '%BPO%' and (trim(hpo.PID) not in('PZ') or cast(trim(hpo.PQREC)as double) > 0))hpo
	LEFT OUTER JOIN lpfg_stg.ITH ith on (
		trim(hpo.PORD)=trim(ith.TREF)
		AND trim(hpo.PLINE)=trim(ith.THLIN)
		AND trim(ith.TPROD) = trim(hpo.pprod)
		AND trim(hpo.SOURCE)=trim(ith.SOURCE)
		AND trim(hpo.REGION)=trim(ith.REGION)
	)
JOIN (
	SELECT *
	FROM lpfg_stg.IIM
	WHERE trim(iim.iityp) IN ('A' , 'B' ,'V')
	) iim ON (
		trim(ith.TPROD) = trim(iim.IPROD)
		AND trim(ith.SOURCE) = trim(iim.SOURCE)
		AND trim(ith.REGION) = trim(iim.REGION)
		)
LEFT OUTER JOIN lpfg_stg.AVM avm ON (
		trim(iim.IVEND) = trim(avm.VENDOR)
		AND trim(iim.SOURCE) = trim(avm.SOURCE)
		AND trim(iim.REGION) = trim(avm.REGION)
		)
LEFT OUTER JOIN lpfg_stg.ITE ON (
		ITE.SOURCE = ITH.SOURCE
		AND ITE.REGION = ITH.REGION
		AND ITE.TTYPE = ITH.TTYPE
		AND TRIM(ITE.TAPO) = 'Y'
		)
LEFT OUTER JOIN (
	SELECT CASE 
			 WHEN length(tenpj) = 1 THEN CONCAT ('0',tenpj)
			ELSE tenpj
		  END AS month,
		tenpj,
		tenj4 AS year,
		teny8
	FROM lpfg_stg.DTEC
	WHERE trim(dtec.teddt) = 'Y'
	) dtec ON ((CAST(TTDTE AS BIGINT) + 19280000) = CAST(trim(dtec.teny8) AS BIGINT))
WHERE CONCAT (
		year,
		'-',
		month
		) < substr(cast(now() AS string), 1, 7)
	AND CONCAT (
		year,
		'-',
		month
		) > substr(cast(add_months(now(), - 2) AS string), 1, 7) 
GROUP BY PLANT,
	MATERIAL_NO,
	SUPPLIER_CODE,
	SUPPLIER_NAME,
	PODEST,
	hpo.PORD,
	iim.idesc,
	iim.idsce,
	dtec.month,
	tenpj,
	dtec.year;