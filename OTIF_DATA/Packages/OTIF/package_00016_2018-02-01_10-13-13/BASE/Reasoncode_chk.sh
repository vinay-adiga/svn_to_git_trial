#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE_CALC=$4
QUERY_FILE_INPUT=$5
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1


#Running OTIF_CALCULATION table
echo "Running OTIF_CALCULATION table"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_CALC >>otif_calc.txt 2>&1 

echo "OTIF_CALCULATION script ran successfully"

hadoop fs -put -f otif_calc.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo "Connecting to HBASE Shell and truncating Reasoncode Input table"
#hbase shell
echo "truncate 'lpfg_core:OTIF_REASONCODE_INPUT'" | hbase shell >>reasoncode_hbase.txt 2>&1

hadoop fs -put -f reasoncode_hbase.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

#Running OTIF_REASONCODE_INPUT table
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT >>otif_reasoncode_input.txt 2>&1 

hadoop fs -put -f otif_reasoncode_input.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo "Checking Count of Reasoncode Input table"
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_core.OTIF_REASONCODE_INPUT">>reasoncode_COUNT_Log

reasoncode_count=$(cat reasoncode_COUNT_Log| grep -Ewo "[0-9]*")

echo "${reasoncode_count}"

hadoop fs -put -f ${reasoncode_count} /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

if [ $reasoncode_count -gt 0 ];
then echo "Count of INPUT table is not 0 so need to run OTIF_CALCULATION table"
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_CALC >>otif_calc1.txt 2>&1 

hadoop fs -put -f otif_calc1.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs
 echo "***1**"
 
else
 echo "Record count is  0..So No need to Run OTIF_CALCULATION Script"
fi