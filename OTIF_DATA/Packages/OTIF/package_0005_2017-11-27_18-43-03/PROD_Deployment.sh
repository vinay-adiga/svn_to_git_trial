#!/bin/bash

echo '---------- Changing Mode of package 20 -----------'
chmod -R 771 /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03

echo '---------- Changing user of Service Account-----------'
chown -R salpfgep /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03

echo '---------- Getting into the directory of package 20-----------'
cd /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03

echo '---------- Changing mode of deploy.sh-----------'
chmod 755 deploy.sh

echo '---------- Running deploy.sh-----------'
sh deploy.sh

echo '---------- Running the DDL Script of OTIF -----------'

echo '---------- DDL Script of OTIF -----------' > LOG_Package_05

sh FINAL/HIVESCRIPTS/DDL/DDL_OTIF.sh >> LOG_Package_05 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of OTIF Executed Successfully'
else
    echo "DDL Script of OTIF executed with error code=$EXIT_STATUS"
fi

echo '---------- getting into the directory and running the DDL Script of Reasoncode HBase table -----------'

cd /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03/FINAL/HIVESCRIPTS/DDL/L3
beeline -u 'jdbc:hive2://itsusraedlp02.jnj.com:10000/default;principal=hive/itsusraedlp02.jnj.com@EU.JNJ.COM' -f OTIF_REASONCODE_INPUT.hql >>Package_05_Reasoncode_HBase 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of Reasoncode HBase table Executed Successfully'
else
    echo "DDL Script of Reasoncode HBase table executed with error code=$EXIT_STATUS"
fi

echo '---------- getting into the directory and running the DML Script of Reasoncode HBase table ---------'

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedlp05.jnj.com:21000 -f '/data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03/FINAL/HIVESCRIPTS/L3/OTIF_REASONCODE_INPUT.hql' >>Package_05_Reasoncode_HBase_DML 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DML Script of Reasoncode HBase table Executed Successfully'
else
    echo "DML Script of Reasoncode HBase table executed with error code=$EXIT_STATUS"
fi


hadoop fs -put -f /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03/FINAL/HIVESCRIPTS/Reasoncode_chk.sh /prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS

echo '---------- Running OTIF Reasoncode workflow -----------' >> LOG_Package_05

oozie job -oozie https://itsusraedlp02.jnj.com:11443/oozie -config /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03/FINAL/oozie_wf/OTIF_REASONCODE_WF/job.properties -run >> LOG_Package_05 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Reasoncode Workflow Executed Successfully'
else
    echo "OTIF Reasoncode Workflow executed with error code=$EXIT_STATUS"
fi

echo '---------- Running OTIF UMB workflow -----------' >> LOG_Package_05

oozie job -oozie https://itsusraedlp02.jnj.com:11443/oozie -config /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0005_2017-11-27_18-43-03/FINAL/oozie_wf/OTIF_WEEKEND_UMB_WF/job.properties -run >> LOG_Package_05 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Workflow Executed Successfully'
else
    echo "OTIF Workflow executed with error code=$EXIT_STATUS"
fi