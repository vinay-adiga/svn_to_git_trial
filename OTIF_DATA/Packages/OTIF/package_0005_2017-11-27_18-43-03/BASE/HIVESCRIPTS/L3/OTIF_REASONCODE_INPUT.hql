Invalidate metadata lpfg_core.OTIF_REASONCODE_INPUT;

INSERT INTO lpfg_core.OTIF_REASONCODE_INPUT (
	KEY_ID,
	PLANT,
	MONTH,
	PO_NO,
	PO_LINE,
	ITEMCODE,
	SUPPLIERCODE,
	SUPPLIERNAME,
	SUPPLIER,
	MATERIAL_TYPE,
	Material_Description,
	REASON_CODE,
	ROOT_CAUSE,
	DelQnt_Divide_ReqQnt,
	DelQnt_Sub_ReqQnt
	)
SELECT CONCAT (
		PLANT,
		'_',
		PO_NO,
		'_',
		PO_LINE
		) AS KEY_ID,
	PLANT,
	MONTH,
	PO_NO,
	PO_LINE,
	ITEMCODE,
	SUPPLIERCODE,
	SUPPLIERNAME,
	CONCAT (
		trim(suppliercode),
		'-',
		TRIM(suppliername)
		) AS SUPPLIER,
	MATERIAL_TYPE,
	Material_Description,
	cast(REASON_CODE AS String),
	CASE WHEN ROOT_CAUSE IS NULL
	THEN '' 
	ELSE ROOT_CAUSE
	END AS ROOT_CAUSE,
	round(cast(DelQnt_Divide_ReqQnt as Decimal), 2) AS DelQnt_Divide_ReqQnt,
	round(cast(DelQnt_Sub_ReqQnt as decimal), 2) AS DelQnt_Sub_ReqQnt
FROM lpfg_core.OTIF_CALCULATION
WHERE TO_BE_DISPLAYED = 'Y' AND DelQnt_Divide_ReqQnt IS NOT NULL;