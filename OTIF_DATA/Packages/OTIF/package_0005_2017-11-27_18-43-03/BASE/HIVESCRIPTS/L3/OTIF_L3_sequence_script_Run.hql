-- Changed Coding for ABFY-6659(Added ORDER_CREATION_DATE) by arath3 on 24.05-2017
INSERT OVERWRITE TABLE lpfg_core.OTIF_CALCULATION

SELECT distinct calc.plant,
     itemcode AS itemcode,
	suppliercode,
	suppliername,
	CONCAT (
		trim(suppliercode),
		'-',
		TRIM(suppliername)
		) AS SUPPLIER,
	ordd,
	Year,
	CASE 
		WHEN month = '1'
			THEN CONCAT ('Jan')
		WHEN month = '2'
			THEN CONCAT ('Feb')
		WHEN month = '3'
			THEN CONCAT ('Mar')
		WHEN month = '4'
			THEN CONCAT ('Apr')
		WHEN month = '5'
			THEN CONCAT ('May')
		WHEN month = '6'
			THEN CONCAT ('Jun')
		WHEN month = '7'
			THEN CONCAT ('Jul')
		WHEN month = '8'
			THEN CONCAT ('Aug')
		WHEN month = '9'
			THEN CONCAT ('Sep')
		WHEN month = '10'
			THEN CONCAT ('Oct')
		WHEN month = '11'
			THEN CONCAT ('Nov')
		WHEN month = '12'
			THEN CONCAT ('Dec')
		ELSE ''
		END AS month,
	receipt_quantity,
	CALC.po_no,
	CALC.po_line,
	days_early_tolerance,
	days_end_tolerance,
	calc.num_del AS Number_of_deliveries,
	po_sku,
	po_sku_ordd,
	CONCAT ( po_sku_ordd, '_', cast(CALC.num_del AS string)) AS po_sku_ReqDate_deliverables,
	plantdesc,
	DELAY,
	ot1 AS on_time,
	ReceivedOnTime,
	ordq,
	ORDER_CREATION_DATE,
	CUM_SUM_RECEIPT_QTY_CALC AS Cumulative_QNT,
	(InFull * 100) AS InFull,
	round((ot1 * InFull * 100), 2) AS otif,
	itemdesc AS Material_Description,
	po_uom,
	gr_UOM AS receipt_uom,
	INCOTERMS,
	INCOTERMS_DESC,
	Receipt_Date,
	STANDARDLT,
	MATERIAL_TYPE,
	PO_TYPE,
	PO_CLASSIFICATION,
	TO_BE_DISPLAYED,
	CASE WHEN OT_RC.REASON_CODE IS NULL 
	   then 0
	ELSE OT_RC.REASON_CODE
	END AS REASON_CODE,
	CASE WHEN OT_RC.REASON_CODE IS NULL 
	   then ''
	ELSE OT_RC.ROOT_CAUSE
	END AS ROOT_CAUSE,
	CUM_SUM_RECEIPT_QTY,
	INFULL_ALL,	
	CASE 
	   WHEN ot1 = 1 
	     THEN RECEIVEDONTIME 
	   ELSE 
	     CASE
	       WHEN OT_RC.REASON_CODE = 2 
		        THEN receipt_quantity
	       ELSE 0
	   END
	END AS ReceivedOnTime_RC,
	
	CASE 
	   WHEN  OT_RC.REASON_CODE = 2  
	      THEN CUM_SUM_RECEIPT_QTY 
	   ELSE CUM_SUM_RECEIPT_QTY_CALC
	END AS Cumulative_QNT_RC,
	
	CASE 
	   WHEN ot1 = 1 
	     THEN  (InFull_ALL * 100) 
	ELSE 
	   CASE
	   WHEN OT_RC.REASON_CODE = 2 
		 THEN (InFull_ALL * 100)
	   ELSE 0	
	   END
	END AS InFull_RC,
	
	CASE 
	   WHEN ot1 = 1 
	     THEN round((InFull * 100), 2) 
	ELSE 
	  CASE
	   WHEN OT_RC.REASON_CODE = 2 
        THEN round((InFull_ALL * 100), 2)
	   ELSE 0
	  END
	END AS otif_RC,
    percentage_ordq,
	low_qty_tol,  -- added for slotif calculation
    high_qty_tol, -- added for slotif calculation
	case when 
			(percentage_ordq between (cast(100 as double) - low_qty_tol) and (cast(100 as double) + high_qty_tol)) and ot1=1
			then 100
			else 0 
			end as slotif 
FROM ( 
	SELECT *,
		   CASE 
		   WHEN (ordq <> 0) AND CUM_SUM_RECEIPT_QTY_CALC <= ordq
		   THEN (CUM_SUM_RECEIPT_QTY_CALC / cast(ordq AS DOUBLE))
		   
		   WHEN (ordq <> 0)	AND CUM_SUM_RECEIPT_QTY_CALC > ordq 
		   THEN (ordq - (CUM_SUM_RECEIPT_QTY_CALC - ordq )) / ordq 
		   ELSE 0
		   END AS InFull,
		   
		   CASE 
		   WHEN (ordq <> 0) AND CUM_SUM_RECEIPT_QTY <= ordq
		   THEN (CUM_SUM_RECEIPT_QTY / cast(ordq AS DOUBLE))
		   
		   WHEN (ordq <> 0)	AND CUM_SUM_RECEIPT_QTY > ordq 
		   THEN (ordq - (CUM_SUM_RECEIPT_QTY - ordq )) / ordq 
		   ELSE 0
		   END AS  InFull_ALL
		   
		   
	  FROM lpfg_wrk.OTIF_CALC_BASE 
	  
) CALC 
LEFT OUTER JOIN lpfg_wrk.OTIF_ONTIME_DISPLAY  OT_DISPLAY ON (
	    (regexp_replace(trim(CALC.PO_NO),"^0*",""))=(regexp_replace(trim(OT_DISPLAY.PO_NO),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_LINE),"^0*",""))=(regexp_replace(trim(OT_DISPLAY.PO_LINE),"^0*",""))
	 AND CALC.NUM_DEL=OT_DISPLAY.NUM_DEL
		)
	
LEFT OUTER JOIN lpfg_stg.OTIF_REASONCODE OT_RC ON (
		(regexp_replace(trim(CALC.PLANT),"^0*",""))=(regexp_replace(trim(OT_RC.PLANT),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_NO),"^0*",""))=(regexp_replace(trim(OT_RC.PO_NO),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_LINE),"^0*",""))=(regexp_replace(trim(OT_RC.PO_LINE),"^0*",""))
	);
	
INSERT INTO TABLE lpfg_core.OTIF_CALCULATION
SELECT ITM_Spcl_Hist.plant,
		ITM_Spcl_Hist.MATERIAL_NO AS itemcode,
	ITM_Spcl_Hist.SUPPLIER_CODE AS suppliercode,
	ITM_Spcl_Hist.SUPPLIER_NAME AS suppliername,
	CONCAT (ITM_Spcl_Hist.SUPPLIER_CODE,'-',ITM_Spcl_Hist.SUPPLIER_NAME) AS SUPPLIER,
	'' ordd,
	Year,
	MONTH AS month,
	ITM_Spcl_Hist.receipt_quantity,
	ITM_Spcl_Hist.po_no,
	ITM_Spcl_Hist.po_line,
	'0' AS days_early_tolerance,
	'0' AS days_end_tolerance,
	0 AS Number_of_deliveries,
	CONCAT(ITM_Spcl_Hist.PO_NO,'_',ITM_Spcl_Hist.MATERIAL_NO) AS po_sku,
	'0' AS  po_sku_ordd,
	'0' AS po_sku_ReqDate_deliverables,
	'' plantdesc,
	0 AS DELAY,
	0 AS on_time,
	0 AS ReceivedOnTime,
	ITM_Spcl_Hist.Requested_delivery_QUANTITY AS ordq,
	'' AS ORDER_CREATION_DATE,
	ITM_Spcl_Hist.receipt_quantity AS Cumulative_QNT,
	0 AS InFull,
	ITM_Spcl_Hist.OTIF,
	ITM_Spcl_Hist.MATERIAL_DESC AS Material_Description,
	'' AS po_uom,
	'' AS receipt_uom,
	'' AS INCOTERMS,
	'' AS INCOTERMS_DESC,
	'' AS Receipt_Date,
	0 AS STANDARDLT,
	'' AS MATERIAL_TYPE,
	TYPE as PO_TYPE,
	'Special' AS PO_CLASSIFICATION,
	'Y' AS TO_BE_DISPLAYED,
	0 AS REASON_CODE,
	'' AS ROOT_CAUSE,
	0 AS CUM_SUM_RECEIPT_QTY,
	0 AS INFULL_ALL,	
	0 AS ReceivedOnTime_RC,
	0 AS Cumulative_QNT_RC,
	0 AS InFull_RC,
	0 AS otif_RC,
    0 AS percentage_ordq,
	0 AS low_qty_tol,  -- added for slotif calculation
    0 AS high_qty_tol, -- added for slotif calculation
	0 AS slotif 
FROM (SELECT KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY,
		(SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS Requested_delivery_QUANTITY,
		SUM(RECEIPT_QUANTITY) / (SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS OTIF 
		FROM lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY  
		GROUP BY KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY) ITM_Spcl_Hist where cast(OTIF AS string)<>'nan' ;	
		
INSERT INTO TABLE lpfg_core.OTIF_CALCULATION
SELECT VDR_Spcl_Hist.plant,
	   VDR_Spcl_Hist.MATERIAL_NO AS itemcode,
	VDR_Spcl_Hist.SUPPLIER_CODE AS suppliercode,
	VDR_Spcl_Hist.SUPPLIER_NAME AS suppliername,
	CONCAT (VDR_Spcl_Hist.SUPPLIER_CODE,'-',VDR_Spcl_Hist.SUPPLIER_NAME) AS SUPPLIER,
	'' ordd,
	Year,
	MONTH AS month,
	VDR_Spcl_Hist.receipt_quantity,
	VDR_Spcl_Hist.po_no,
	VDR_Spcl_Hist.po_line,
	'0' AS days_early_tolerance,
	'0' AS days_end_tolerance,
	0 AS Number_of_deliveries,
	CONCAT(VDR_Spcl_Hist.PO_NO,'_',VDR_Spcl_Hist.MATERIAL_NO) AS po_sku,
	'0' AS  po_sku_ordd,
	'0' AS po_sku_ReqDate_deliverables,
	'' plantdesc,
	0 AS DELAY,
	0 AS on_time,
	0 AS ReceivedOnTime,
	VDR_Spcl_Hist.Requested_delivery_QUANTITY AS ordq,
	'' AS ORDER_CREATION_DATE,
	VDR_Spcl_Hist.receipt_quantity AS Cumulative_QNT,
	0 AS InFull,
	VDR_Spcl_Hist.OTIF,
	VDR_Spcl_Hist.MATERIAL_DESC AS Material_Description,
	'' AS po_uom,
	'' AS receipt_uom,
	'' AS INCOTERMS,
	'' AS INCOTERMS_DESC,
	'' AS Receipt_Date,
	0 AS STANDARDLT,
	'' AS MATERIAL_TYPE,
	TYPE as PO_TYPE,
	'Special' AS PO_CLASSIFICATION,
	'Y' AS TO_BE_DISPLAYED,
	0 AS REASON_CODE,
	'' AS ROOT_CAUSE,
	0 AS CUM_SUM_RECEIPT_QTY,
	0 AS INFULL_ALL,	
	0 AS ReceivedOnTime_RC,
	0 AS Cumulative_QNT_RC,
	0 AS InFull_RC,
	0 AS otif_RC,
    0 AS percentage_ordq,
	0 AS low_qty_tol,  -- added for slotif calculation
    0 AS high_qty_tol, -- added for slotif calculation
	0 AS slotif 
FROM (SELECT KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY,
		(SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS Requested_delivery_QUANTITY,
		SUM(RECEIPT_QUANTITY) / (SUM(RECEIPT_QUANTITY) + MISSED_QUANTITY) AS OTIF 
		FROM lpfg_wrk.OTIF_VDR_SPECIAL_PO_HISTORY  
		GROUP BY KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		RECEIPT_QUANTITY,
		MISSED_QUANTITY) VDR_Spcl_Hist where cast(OTIF AS string)<>'nan' ;	
		
-- Supplier Sequence		
INSERT OVERWRITE TABLE lpfg_core.OTIF_SUPPLIER_ORDQ_CALC
SELECT itemcode,
	plant,
	suppliercode,
	suppliername,
	ordd,
	month,
	year,
	receipt_quantity,
	po_no,
	po_line,
	days_early_tolerance,
	days_end_tolerance,
	Number_of_deliveries,
	po_sku,
	OTIF_CALC.po_sku_ordd,
	po_sku_ReqDate_deliverables,
	plantdesc,
	DELAY,
	on_time,
	ReceivedOnTime,
	ordq,
	Cumulative_QNT,
	InFull,
	otif,
	Material_Description,
	PO_UOM,
	Receipt_UOM,
	INCOTERMS,
	Receipt_Date,
	STANDARDLT,
	MATERIAL_TYPE,
	PO_TYPE,
	min_no_del,
	max_no_del,
	sum(cast(ordq AS DOUBLE)) OVER (  
		PARTITION BY plant,
		Suppliercode,
		month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))),
		year(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) DESC 
		ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
		) AS ordq_sum,
	ReceivedOnTime_RC,
	Cumulative_QNT_RC,
	InFull_RC,
	Otif_RC 
FROM lpfg_core.OTIF_CALCULATION OTIF_CALC,
	(
		SELECT min(number_of_deliveries) AS min_no_del,
			max(number_of_deliveries) AS max_no_del,
			po_sku_ordd
		FROM lpfg_core.OTIF_CALCULATION ot_calc
		GROUP BY po_sku_ordd
		) calc
WHERE OTIF_CALC.po_sku_ordd = calc.po_sku_ordd
	AND OTIF_CALC.number_of_deliveries = calc.max_no_del;
	
INSERT OVERWRITE TABLE lpfg_core.OTIF_SUPPLIERWISE_BASE
SELECT PLANT,
	suppliercode,
	suppliername,
	month,
	year,
	ordq_sum,
	Cumulative_Sum,
	Cumulative_Sum_RC,
	on_time,
	InFull_Month_Supplier,
	InFull_Month_Supplier_RC,
	sum(ordq_sum) OVER (
		PARTITION BY suppliercode,year ORDER BY year
		) AS ordq_sum_all,
	sum(Cumulative_Sum) OVER (
		PARTITION BY suppliercode,year ORDER BY year
		) AS Cumulative_Sum_all,
	sum(Cumulative_Sum_RC) OVER (
		PARTITION BY suppliercode,year ORDER BY year
		) AS Cumulative_Sum_all_RC,
	((cast(InFull_Month_Supplier AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_Supplier,
	((cast(InFull_Month_Supplier_RC AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_Supplier_RC,
	days_early_tolerance,
	days_end_tolerance,
	RECEIPT_QUANTITY,
	RECEIVEDONTIME,
	REASON_CODE,
	CUM_SUM_RECEIPT_QTY,
	INFULL_ALL,
	ReceivedOnTime_RC,
	Cumulative_QNT_RC,
	InFull_RC,
	Otif_RC,
	SLOTIF,
	COUNT_SLOTIF,
	SUM_SLOTIF,
	(SUM_SLOTIF / COUNT_SLOTIF) * 100 AS SUPPLIER_SLOTIF
FROM (
	SELECT DISTINCT ot_calc_ordq.PLANT,
		ot_calc_ordq.Suppliercode,
		ot_calc_ordq.suppliername,
		-- ot_calc_ordq.supplier,
		ot_calc_ordq.month,
		ot_calc_ordq.year,
		ot_calc_ordq.ordq_sum,
		ot_calc_ordq.Cumulative_Sum,
		ot_calc_ordq.Cumulative_Sum_RC,
		ot_calc_ordq.on_time,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_Supplier,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_Supplier_RC,	
		ot_calc_ordq.days_early_tolerance,
		ot_calc_ordq.days_end_tolerance,
		ot_calc_ordq.RECEIPT_QUANTITY,
		ot_calc_ordq.RECEIVEDONTIME,
		ot_calc_ordq.REASON_CODE,
		ot_calc_ordq.CUM_SUM_RECEIPT_QTY,
		ot_calc_ordq.INFULL_ALL,
		ot_calc_ordq.ReceivedOnTime_RC,
		ot_calc_ordq.Cumulative_QNT_RC,
		ot_calc_ordq.InFull_RC,
		ot_calc_ordq.Otif_RC,
		ot_calc_ordq.SLOTIF,
		ot_calc_ordq.COUNT_SLOTIF,
		ot_calc_ordq.SUM_SLOTIF
		FROM (
		SELECT month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS month,
			cast(year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS string) AS year,
			calc.del,
			calc.po_sku_ordd,
			OT_CALC.PLANT,
			OT_CALC.Suppliercode,
			OT_CALC.suppliername,
			OT_CALC.on_time,
			ordq_calc.ordq_sum AS ordq_sum,
			sum(cast(OT_CALC.Cumulative_QNT AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum,
			sum(cast(OT_CALC.Cumulative_QNT_RC AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum_RC,	
			OT_CALC.days_early_tolerance,
			OT_CALC.days_end_tolerance,
			OT_CALC.RECEIPT_QUANTITY,
			OT_CALC.RECEIVEDONTIME,
			OT_CALC.REASON_CODE,
			OT_CALC.CUM_SUM_RECEIPT_QTY,
			OT_CALC.INFULL_ALL,
			OT_CALC.ReceivedOnTime_RC,
			OT_CALC.Cumulative_QNT_RC,
			OT_CALC.InFull_RC,
			OT_CALC.Otif_RC,
			OT_CALC.slotif,
			count() OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_SLOTIF, --count
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_SLOTIF --sum
			
		FROM lpfg_core.OTIF_CALCULATION OT_CALC
		LEFT OUTER JOIN lpfg_core.OTIF_SUPPLIER_ORDQ_CALC ordq_calc ON (
				OT_CALC.po_sku_ordd = ordq_calc.po_sku_ordd
				AND OT_CALC.PLANT = ordq_calc.PLANT
				),
			(
				SELECT max(number_of_deliveries) AS del,
					po_sku_ordd
				FROM lpfg_core.OTIF_CALCULATION 
				GROUP BY po_sku_ordd
				) calc
		WHERE OT_CALC.po_sku_ordd = calc.po_sku_ordd
			AND OT_CALC.number_of_deliveries = calc.del
		) ot_calc_ordq
	) ot_suplier_base;
	
INSERT overwrite TABLE lpfg_core.OTIF_CALCULATION_FOR_SUPPLIER
SELECT ot_suplier_base.plant,
	ot_suplier_base.SupplierCode,
	ot_suplier_base.SupplierName,
	CASE 
		WHEN ot_suplier_base.month = 1
			THEN CONCAT ('Jan')
		WHEN ot_suplier_base.month = 2
			THEN CONCAT ('Feb')
		WHEN ot_suplier_base.month = 3
			THEN CONCAT ('Mar')
		WHEN ot_suplier_base.month = 4
			THEN CONCAT ('Apr')
		WHEN ot_suplier_base.month = 5
			THEN CONCAT ('May')
		WHEN ot_suplier_base.month = 6
			THEN CONCAT ('Jun')
		WHEN ot_suplier_base.month = 7
			THEN CONCAT ('Jul')
		WHEN ot_suplier_base.month = 8
			THEN CONCAT ('Aug')
		WHEN ot_suplier_base.month = 9
			THEN CONCAT ('Sep')
		WHEN ot_suplier_base.month = 10
			THEN CONCAT ('Oct')
		WHEN ot_suplier_base.month = 11
			THEN CONCAT ('Nov')
		WHEN ot_suplier_base.month = 12
			THEN CONCAT ('Dec')
		ELSE ''
		END AS Month,
	ot_suplier_base.year AS Year,
	CASE 
		WHEN ot_suplier_base.month = 1
			THEN CONCAT ('Jan','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 2
			THEN CONCAT ('Feb','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 3
			THEN CONCAT ('Mar','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 4
			THEN CONCAT ('Apr','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 5
			THEN CONCAT ('May','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 6
			THEN CONCAT ('Jun','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 7
			THEN CONCAT ('Jul','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 8
			THEN CONCAT ('Aug','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 9
			THEN CONCAT ('Sep','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 10
			THEN CONCAT ('Oct','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 11
			THEN CONCAT ('Nov','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 12
			THEN CONCAT ('Dec','-',cast(ot_suplier_base.year AS string))
		ELSE ''
		END AS Month_year,
	ot_suplier_base.ordq_sum,
	ot_suplier_base.Cumulative_Sum,
	ot_suplier_base.Cumulative_Sum_RC,
	ot_suplier_base.On_Time,
	ot_suplier_base.InFull_Month_Supplier AS InFull_Mon_Supplier,
	ot_suplier_base.InFull_Month_Supplier_RC AS InFull_Mon_Supplier_RC,
	round(ot_suplier_base.OTIF_Month_Supplier, 2) AS OTIF_Month_Supplier,
	round(ot_suplier_base.OTIF_Month_Supplier_RC, 2) AS OTIF_Month_Supplier_RC,
	BASE_FINAL.ordq_sum_all AS ordq_sum_all,
	BASE_FINAL.Cumulative_Sum_all,
	BASE_FINAL.Cumulative_Sum_all_RC,
	ot_suplier_base.YTD_InFull_Supplier,
	ot_suplier_base.YTD_InFull_Supplier_RC,
	round((cast(YTD_InFull_Supplier AS DOUBLE) * 100), 2) AS YTD_OTIF_Supplier,
	round((cast(YTD_InFull_Supplier_RC AS DOUBLE) * 100), 2) AS YTD_OTIF_Supplier_RC,
	TRIM(ot_target.TARGET) AS TARGET,
	ot_suplier_base.days_early_tolerance,
	ot_suplier_base.days_end_tolerance,
	ot_suplier_base.RECEIPT_QUANTITY,
	ot_suplier_base.RECEIVEDONTIME,
	ot_suplier_base.REASON_CODE,
	ot_suplier_base.CUM_SUM_RECEIPT_QTY,
	ot_suplier_base.INFULL_ALL,
	ot_suplier_base.ReceivedOnTime_RC,
	ot_suplier_base.Cumulative_QNT_RC,
	ot_suplier_base.InFull_RC,
	ot_suplier_base.Otif_RC,
	ot_suplier_base.COUNT_SLOTIF,
	ot_suplier_base.SUM_SLOTIF,
	ot_suplier_base.SUPPLIER_SLOTIF
FROM (
	SELECT BASE.*,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFull_Supplier,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFull_Supplier_RC
	FROM lpfg_core.OTIF_SUPPLIERWISE_BASE BASE
	) ot_suplier_base
JOIN (
	SELECT plant,
		suppliercode,
		year,
		sum(MAX_ORDQ_SUM) AS ordq_sum_all,
		sum(MAX_CUM_SUM) AS Cumulative_Sum_all,
		sum(MAX_CUM_SUM_RC) AS Cumulative_Sum_all_RC
	FROM (
		SELECT plant,
			suppliercode,
			month,
			year,
			max(ordq_sum) AS MAX_ORDQ_SUM,
			max(Cumulative_Sum) AS MAX_CUM_SUM,
			max(Cumulative_Sum_RC) AS MAX_CUM_SUM_RC
		FROM lpfg_core.OTIF_SUPPLIERWISE_BASE
		GROUP BY plant,
			suppliercode,
			month,
			year
		) suplier_base
	GROUP BY plant,
		suppliercode,
		year
	) BASE_FINAL ON (
		ot_suplier_base.plant = BASE_FINAL.plant
		AND ot_suplier_base.suppliercode = BASE_FINAL.suppliercode
		AND ot_suplier_base.year = BASE_FINAL.year
		)
LEFT OUTER JOIN lpfg_wrk.OTIF_PLANT_TARGET ot_target ON (ot_suplier_base.PLANT = ot_target.PLANT);

INSERT overwrite TABLE lpfg_core.OTIF_MAX_CALC_SUPPLIERWISE
SELECT plant,
	SupplierCode,
	SupplierName,
	CONCAT (
		trim(suppliercode),
		'-',
		TRIM(suppliername)
		) AS SUPPLIER,
	Month,
	Year,
	Month_year,
	MAX(ordq_sum),
	MAX(Cumulative_Sum),
	MAX(Cumulative_Sum_RC),
	MAX(InFull_Mon_Supplier),
	MAX(InFull_Mon_Supplier_RC),
	MAX(OTIF_Month_Supplier),
	MAX(OTIF_Month_Supplier_RC),
	MAX(ordq_sum_all),
	MAX(Cumulative_Sum_all),
	MAX(Cumulative_Sum_all_RC),
	MAX(YTD_InFull_Supplier),
	MAX(YTD_InFull_Supplier_RC),
	MAX(YTD_OTIF_Supplier),
	MAX(YTD_OTIF_Supplier_RC),
	target,
	MAX(SUPPLIER_SLOTIF)
FROM lpfg_core.OTIF_CALCULATION_FOR_SUPPLIER
GROUP BY plant,
	SupplierCode,
	SupplierName,
	supplier,
	Month,
	Year,
	month_year,
	target;
	
INSERT overwrite TABLE lpfg_core.OTIF_SUPPLIERWISE_CALC
SELECT supplier.plant,
	supplier.SupplierCode,
	supplier.SupplierName,
	supplier.SUPPLIER,
	supplier.Month,
	supplier.Year,
	supplier.Month_year,
	supplier.ordq_sum,
	supplier.Cumulative_Sum,
	supplier.Cumulative_Sum_RC,
	supplier.InFull_Mon_Supplier,
	supplier.InFull_Mon_Supplier_RC,
	supplier.OTIF_Month_Supplier,
	supplier.OTIF_Month_Supplier_RC,
	supplier.ORDQ_SUM_ALL,
	ordq_sum_calc.sum_ordq_sum_all AS SUM_ORDQ_SUM_ALL,
	supplier.Cumulative_Sum_all,
	supplier.Cumulative_Sum_all_RC,
	supplier.YTD_InFull_Supplier,
	supplier.YTD_InFull_Supplier_RC,
	supplier.YTD_OTIF_Supplier,
	supplier.YTD_OTIF_Supplier_RC,
	supplier.target,
	supplier.SUPPLIER_SLOTIF
FROM lpfg_core.OTIF_MAX_CALC_SUPPLIERWISE supplier
JOIN (
	SELECT suppliercode,
		--month,
		year,
		sum(distinct ordq_sum_all) AS sum_ordq_sum_all
	FROM lpfg_core.OTIF_MAX_CALC_SUPPLIERWISE
	GROUP BY suppliercode,
		--month,
		year
	) ordq_sum_calc ON (
		supplier.suppliercode = ordq_sum_calc.suppliercode
		--AND supplier.month = ordq_sum_calc.month
		AND supplier.year = ordq_sum_calc.year
		);


--Plantwise Sequence		
INSERT OVERWRITE TABLE lpfg_core.OTIF_PLANT_ORDQ_CALC
SELECT itemcode,
	plant,
	ordd,
	month,
	year,
	receipt_quantity,
	po_no,
	po_line,
	days_early_tolerance,
	days_end_tolerance,
	Number_of_deliveries,
	po_sku,
	OTIF_CALC.po_sku_ordd,
	po_sku_ReqDate_deliverables,
	plantdesc,
	DELAY,
	on_time,
	ReceivedOnTime,
	ordq,
	Cumulative_QNT,
	InFull,
	otif,
	Material_Description,
	PO_UOM,
	Receipt_UOM,
	INCOTERMS,
	Receipt_Date,
	STANDARDLT,
	MATERIAL_TYPE,
	PO_TYPE,
	min_no_del,
	max_no_del,
	sum(cast(ordq AS DOUBLE)) OVER (  
		PARTITION BY plant,
		month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))),
		year(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) DESC 
		ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
		) AS ordq_sum,
	ReceivedOnTime_RC,
	Cumulative_QNT_RC,
	InFull_RC,
	Otif_RC 
FROM lpfg_core.OTIF_CALCULATION OTIF_CALC,
	(
		SELECT min(number_of_deliveries) AS min_no_del,
			max(number_of_deliveries) AS max_no_del,
			po_sku_ordd
		FROM lpfg_core.OTIF_CALCULATION ot_calc
		GROUP BY po_sku_ordd
		) calc
WHERE OTIF_CALC.po_sku_ordd = calc.po_sku_ordd
	AND OTIF_CALC.number_of_deliveries = calc.max_no_del;
	
INSERT OVERWRITE lpfg_core.OTIF_PLANTWISE_BASE 
SELECT   PLANT,
	month,
	year,
	ordq_sum,
	Cumulative_Sum,
	Cumulative_Sum_RC,
	on_time,
	InFull_Month,
	InFull_Month_RC,
	sum(ordq_sum) OVER (PARTITION BY PLANT,year ORDER BY year) AS ordq_sum_all,
	sum(Cumulative_Sum) OVER (PARTITION BY PLANT,year ORDER BY year) AS Cumulative_Sum_all,
	sum(Cumulative_Sum_RC) OVER (PARTITION BY PLANT,year ORDER BY year) AS Cumulative_Sum_all_RC,
	((cast(InFull_Month AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month,
	((cast(InFull_Month_RC AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_RC,
	days_early_tolerance,
	days_end_tolerance,
	SLOTIF ,
		COUNT_SLOTIF ,
		SUM_SLOTIF ,
		(SUM_SLOTIF / COUNT_SLOTIF) * 100 AS PLANT_SLOTIF,
    COUNT_YEAR_SLOTIF ,
		SUM_YEAR_SLOTIF ,
		(SUM_YEAR_SLOTIF / COUNT_YEAR_SLOTIF) * 100 AS YTD_PLANT_SLOTIF		
FROM (
	SELECT DISTINCT ot_calc_ordq.PLANT,
		ot_calc_ordq.month,
		ot_calc_ordq.year,
		ot_calc_ordq.ordq_sum,
		ot_calc_ordq.Cumulative_Sum,
		ot_calc_ordq.Cumulative_Sum_RC,
		ot_calc_ordq.on_time,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_RC,
		ot_calc_ordq.days_early_tolerance,
		ot_calc_ordq.days_end_tolerance,
		ot_calc_ordq.SLOTIF ,
		ot_calc_ordq.COUNT_SLOTIF ,
		ot_calc_ordq.SUM_SLOTIF, 
		ot_calc_ordq.COUNT_YEAR_SLOTIF, 
		ot_calc_ordq.SUM_YEAR_SLOTIF
	FROM (
		SELECT month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS month,
			cast(year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) as string) AS year,
			calc.del,
			calc.po_sku_ordd,
			OT_CALC.PLANT,
			OT_CALC.on_time,
			ordq_calc.ordq_sum AS ordq_sum,
			sum(cast(OT_CALC.Cumulative_QNT AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum,
				
			sum(cast(OT_CALC.Cumulative_QNT_RC AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum_RC,
				
			OT_CALC.days_early_tolerance,
			OT_CALC.days_end_tolerance,
			OT_CALC.SLOTIF ,
				count() OVER (
				PARTITION BY OT_CALC.PLANT,
							month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_SLOTIF, --count
			count() OVER (
				PARTITION BY OT_CALC.PLANT,
							--month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_YEAR_SLOTIF,
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY OT_CALC.PLANT,
														month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_SLOTIF,
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY OT_CALC.PLANT,
														--month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_YEAR_SLOTIF
		FROM lpfg_core.OTIF_CALCULATION OT_CALC
		LEFT OUTER JOIN lpfg_core.OTIF_PLANT_ORDQ_CALC ordq_calc ON (
				    OT_CALC.po_sku_ordd = ordq_calc.po_sku_ordd
				AND OT_CALC.PLANT = ordq_calc.PLANT
				),
			(
				SELECT max(number_of_deliveries) AS del,
					po_sku_ordd
				FROM lpfg_core.OTIF_CALCULATION 
				GROUP BY po_sku_ordd
				) calc
		WHERE OT_CALC.po_sku_ordd = calc.po_sku_ordd
			AND OT_CALC.number_of_deliveries = calc.del
		) ot_calc_ordq
	) ot_plant_base;
	
INSERT overwrite TABLE lpfg_core.OTIF_CALCULATION_FOR_PLANT
SELECT  ot_plant_base.plant,
	CASE 
		WHEN ot_plant_base.month = 1
			THEN CONCAT ('Jan')
		WHEN ot_plant_base.month = 2
			THEN CONCAT ('Feb')
		WHEN ot_plant_base.month = 3
			THEN CONCAT ('Mar')
		WHEN ot_plant_base.month = 4
			THEN CONCAT ('Apr')
		WHEN ot_plant_base.month = 5
			THEN CONCAT ('May')
		WHEN ot_plant_base.month = 6
			THEN CONCAT ('Jun')
		WHEN ot_plant_base.month = 7
			THEN CONCAT ('Jul')
		WHEN ot_plant_base.month = 8
			THEN CONCAT ('Aug')
		WHEN ot_plant_base.month = 9
			THEN CONCAT ('Sep')
		WHEN ot_plant_base.month = 10
			THEN CONCAT ('Oct')
		WHEN ot_plant_base.month = 11
			THEN CONCAT ('Nov')
		WHEN ot_plant_base.month = 12
			THEN CONCAT ('Dec')
		ELSE ''
		END AS Month,
	ot_plant_base.year AS Year,
	CASE 
		WHEN ot_plant_base.month = 1
			THEN CONCAT ('Jan','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 2
			THEN CONCAT ('Feb','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 3
			THEN CONCAT ('Mar','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 4
			THEN CONCAT ('Apr','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 5
			THEN CONCAT ('May','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 6
			THEN CONCAT ('Jun','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 7
			THEN CONCAT ('Jul','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 8
			THEN CONCAT ('Aug','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 9
			THEN CONCAT ('Sep','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 10
			THEN CONCAT ('Oct','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 11
			THEN CONCAT ('Nov','-',cast(ot_plant_base.year AS string))
		WHEN ot_plant_base.month = 12
			THEN CONCAT ('Dec','-',cast(ot_plant_base.year AS string))
		END AS Month_year,
	ot_plant_base.ordq_sum,
	ot_plant_base.Cumulative_Sum,
	ot_plant_base.Cumulative_Sum_RC,
	ot_plant_base.On_Time,
	ot_plant_base.InFull_Month AS InFull_Month,
	ot_plant_base.InFull_Month_RC AS InFull_Month_RC,
	round(ot_plant_base.otif_month, 2) AS otif_month,
	round(ot_plant_base.otif_month_rc, 2) AS otif_month_RC,
	BASE_FINAL.ordq_sum_all AS ordq_sum_all,
	BASE_FINAL.Cumulative_Sum_all,
	BASE_FINAL.Cumulative_Sum_all_RC,
	ot_plant_base.YTD_InFULL,
	ot_plant_base.YTD_InFULL_RC,
	round((cast(YTD_InFULL AS DOUBLE) * 100), 2) AS YTD_OTIF,
	round((cast(YTD_InFULL_RC AS DOUBLE) * 100), 2) AS YTD_OTIF_RC,
	TRIM(ot_target.TARGET) AS TARGET,
	ot_plant_base.days_early_tolerance,
	ot_plant_base.days_end_tolerance,
	ot_plant_base.COUNT_SLOTIF,
	ot_plant_base.SUM_SLOTIF,
	ot_plant_base.PLANT_SLOTIF,
	ot_plant_base.YTD_PLANT_SLOTIF
FROM (
	SELECT BASE.*,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFULL,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFULL_RC
	FROM lpfg_core.OTIF_PLANTWISE_BASE BASE 
    
	) ot_plant_base
	
JOIN (select plant,
             year,
			 sum(MAX_ORDQ_SUM) as ordq_sum_all,
			 sum(MAX_CUM_SUM) as Cumulative_Sum_all,
			sum(MAX_CUM_SUM_RC) as Cumulative_Sum_all_RC 
		
		from (
		       select plant,
			          month,
					  year,
					  max(ordq_sum) as MAX_ORDQ_SUM,
					  max(Cumulative_Sum) as MAX_CUM_SUM,
					  max(Cumulative_Sum_RC) as MAX_CUM_SUM_RC
        
		         from lpfg_core.OTIF_PLANTWISE_BASE 
				group by plant,
				         month,
						 year) plant_BASE 
						 
	   group by plant,
	            year
				
	)BASE_FINAL ON ( ot_plant_base.plant = BASE_FINAL.plant AND 
	                 ot_plant_base.year = BASE_FINAL.year   
					 )
	
LEFT OUTER JOIN lpfg_wrk.OTIF_PLANT_TARGET ot_target ON (ot_plant_base.PLANT = ot_target.PLANT);

INSERT overwrite TABLE lpfg_core.OTIF_PLANTWISE_CALC	
SELECT 	plant,
	Month,
	Year,
	Month_year,
		MAX(ordq_sum), 
		MAX(Cumulative_Sum),
		MAX(Cumulative_Sum_RC),
		MAX(InFull_Month),
		MAX(InFull_Month_RC),		
		MAX(otif_Month ),
		MAX(otif_Month_RC),
		MAX(ordq_sum_all), 
		MAX(Cumulative_Sum_all), 
		MAX(Cumulative_Sum_all_RC),
		MAX(YTD_InFULL ),
		MAX(YTD_InFULL_RC),
		MAX(YTD_OTIF),
		MAX(YTD_OTIF_RC),
		target,
		MAX(PLANT_SLOTIF),
		MAX(YTD_PLANT_SLOTIF)
FROM lpfg_core.OTIF_CALCULATION_FOR_PLANT
group by plant,
	Month,
	Year,
	month_year,
	target; 
	
-- EMEA Sequence
INSERT OVERWRITE TABLE lpfg_core.OTIF_EMEA_ORDQ_CALC
SELECT itemcode,
	plant,
	ordd,
	month,
	year,
	receipt_quantity,
	po_no,
	po_line,
	days_early_tolerance,
	days_end_tolerance,
	Number_of_deliveries,
	po_sku,
	OTIF_CALC.po_sku_ordd,
	po_sku_ReqDate_deliverables,
	plantdesc,
	DELAY,
	on_time,
	ReceivedOnTime,
	ordq,
	Cumulative_QNT,
	InFull,
	otif,
	Material_Description,
	PO_UOM,
	Receipt_UOM,
	INCOTERMS,
	Receipt_Date,
	STANDARDLT,
	MATERIAL_TYPE,
	PO_TYPE,
	min_no_del,
	max_no_del,
	sum(cast(ordq AS DOUBLE)) OVER (  
		PARTITION BY --plant, -- changed by sudhir for emea calculation
		month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))),
		year(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) DESC 
		ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
		) AS ordq_sum,
	ReceivedOnTime_RC,
	Cumulative_QNT_RC,
	InFull_RC,
	Otif_RC 
FROM lpfg_core.OTIF_CALCULATION OTIF_CALC,
	(
		SELECT min(number_of_deliveries) AS min_no_del,
			max(number_of_deliveries) AS max_no_del,
			po_sku_ordd
		FROM lpfg_core.OTIF_CALCULATION ot_calc
		GROUP BY po_sku_ordd
		) calc
WHERE OTIF_CALC.po_sku_ordd = calc.po_sku_ordd
	AND OTIF_CALC.number_of_deliveries = calc.max_no_del;
	
INSERT OVERWRITE TABLE lpfg_core.OTIF_EMEAWISE_BASE
SELECT   PLANT,
	month,
	year,
	ordq_sum,
	Cumulative_Sum,
	Cumulative_Sum_RC,
	on_time,
	InFull_Month,
	InFull_Month_RC,
	sum(ordq_sum) OVER (PARTITION BY year ORDER BY year) AS ordq_sum_all,-- changed by sudhir for emea calculation
	sum(Cumulative_Sum) OVER (PARTITION BY year ORDER BY year) AS Cumulative_Sum_all,-- changed by sudhir for emea calculation
	sum(Cumulative_Sum_RC) OVER (PARTITION BY year ORDER BY year) AS Cumulative_Sum_all_RC,
	((cast(InFull_Month AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month,
	((cast(InFull_Month_RC AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_RC,
	days_early_tolerance,
	days_end_tolerance,
	SLOTIF ,
	COUNT_SLOTIF ,
	SUM_SLOTIF ,
	(SUM_SLOTIF / COUNT_SLOTIF) * 100 AS EMEA_SLOTIF,
	COUNT_YEAR_SLOTIF ,
		SUM_YEAR_SLOTIF ,
		(SUM_YEAR_SLOTIF / COUNT_YEAR_SLOTIF) * 100 AS YTD_EMEA_SLOTIF	
	
FROM (
	SELECT DISTINCT Otif_calc.PLANT,
		Otif_calc.month,
		Otif_calc.year,
		Otif_calc.ordq_sum,
		Otif_calc.Cumulative_Sum,
		Otif_calc.Cumulative_Sum_RC,
		Otif_calc.on_time,
		CASE 
			WHEN (cast(Otif_calc.Cumulative_Sum AS DOUBLE)) <= (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.Cumulative_Sum AS DOUBLE) / cast(Otif_calc.ordq_sum AS DOUBLE))
			WHEN (cast(Otif_calc.Cumulative_Sum AS DOUBLE)) > (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.ordq_sum AS DOUBLE) - (cast(Otif_calc.Cumulative_Sum AS DOUBLE) - cast(Otif_calc.ordq_sum AS DOUBLE))) / cast(Otif_calc.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month,
		CASE 
			WHEN (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE)) <= (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE) / cast(Otif_calc.ordq_sum AS DOUBLE))
			WHEN (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE)) > (cast(Otif_calc.ordq_sum AS DOUBLE))
				THEN (cast(Otif_calc.ordq_sum AS DOUBLE) - (cast(Otif_calc.Cumulative_Sum_RC AS DOUBLE) - cast(Otif_calc.ordq_sum AS DOUBLE))) / cast(Otif_calc.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_RC,	
		Otif_calc.days_early_tolerance,
		Otif_calc.days_end_tolerance,
		Otif_calc.SLOTIF ,
		Otif_calc.COUNT_SLOTIF ,
		Otif_calc.SUM_SLOTIF,
		Otif_calc.COUNT_YEAR_SLOTIF, 
		Otif_calc.SUM_YEAR_SLOTIF

	FROM (
		SELECT month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS month,
			cast(year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) as string) AS year,
			calc.del,
			calc.po_sku_ordd,
			OT_CALC.PLANT,
			OT_CALC.on_time,
			ordq_calc.ordq_sum AS ordq_sum,
			sum(cast(OT_CALC.Cumulative_QNT AS DOUBLE)) OVER (
				PARTITION BY --OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum,
			sum(cast(OT_CALC.Cumulative_QNT_RC AS DOUBLE)) OVER (
				PARTITION BY --OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum_RC,	
			OT_CALC.days_early_tolerance,
			OT_CALC.days_end_tolerance,
			OT_CALC.SLOTIF ,
				count() OVER (
				PARTITION BY --OT_CALC.PLANT, -- changed by sudhir for emea calculation
							month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_SLOTIF, --count
				count() OVER (
				PARTITION BY --OT_CALC.PLANT,
							--month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_YEAR_SLOTIF,
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY --OT_CALC.PLANT, -- changed by sudhir for emea calculation
														month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_SLOTIF, --sum
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY --OT_CALC.PLANT,
														--month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_YEAR_SLOTIF
		FROM lpfg_core.OTIF_CALCULATION OT_CALC
		LEFT OUTER JOIN lpfg_core.OTIF_EMEA_ORDQ_CALC ordq_calc ON (
				    OT_CALC.po_sku_ordd = ordq_calc.po_sku_ordd
				AND OT_CALC.PLANT = ordq_calc.PLANT
				),
			(
				SELECT max(number_of_deliveries) AS del,
					po_sku_ordd
				FROM lpfg_core.OTIF_CALCULATION b
				GROUP BY po_sku_ordd
				) calc
		WHERE OT_CALC.po_sku_ordd = calc.po_sku_ordd
			AND OT_CALC.number_of_deliveries = calc.del
		) Otif_calc
	) ot_emea_base;

INSERT overwrite TABLE lpfg_core.OTIF_CALCULATION_FOR_EMEA
SELECT  ot_emea_base.plant,
	CASE 
		WHEN ot_emea_base.month = 1
			THEN CONCAT ('Jan')
		WHEN ot_emea_base.month = 2
			THEN CONCAT ('Feb')
		WHEN ot_emea_base.month = 3
			THEN CONCAT ('Mar')
		WHEN ot_emea_base.month = 4
			THEN CONCAT ('Apr')
		WHEN ot_emea_base.month = 5
			THEN CONCAT ('May')
		WHEN ot_emea_base.month = 6
			THEN CONCAT ('Jun')
		WHEN ot_emea_base.month = 7
			THEN CONCAT ('Jul')
		WHEN ot_emea_base.month = 8
			THEN CONCAT ('Aug')
		WHEN ot_emea_base.month = 9
			THEN CONCAT ('Sep')
		WHEN ot_emea_base.month = 10
			THEN CONCAT ('Oct')
		WHEN ot_emea_base.month = 11
			THEN CONCAT ('Nov')
		WHEN ot_emea_base.month = 12
			THEN CONCAT ('Dec')
		ELSE ''
		END AS Month,
	ot_emea_base.year AS Year,
	CASE 
		WHEN ot_emea_base.month = 1
			THEN CONCAT ('Jan','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 2
			THEN CONCAT ('Feb','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 3
			THEN CONCAT ('Mar','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 4
			THEN CONCAT ('Apr','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 5
			THEN CONCAT ('May','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 6
			THEN CONCAT ('Jun','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 7
			THEN CONCAT ('Jul','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 8
			THEN CONCAT ('Aug','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 9
			THEN CONCAT ('Sep','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 10
			THEN CONCAT ('Oct','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 11
			THEN CONCAT ('Nov','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 12
			THEN CONCAT ('Dec','-',cast(ot_emea_base.year AS string))
		ELSE ''
		END AS Month_year,
	ot_emea_base.ordq_sum,
	ot_emea_base.Cumulative_Sum,
	ot_emea_base.Cumulative_Sum_RC,
	ot_emea_base.On_Time,
	ot_emea_base.InFull_Month AS InFull_Month,
	ot_emea_base.InFull_Month AS InFull_Month_RC,
	round(ot_emea_base.otif_month, 2) AS otif_month,
	round(ot_emea_base.otif_month_RC, 2) AS otif_month_RC,
	BASE_FINAL.ordq_sum_all AS ordq_sum_all,
	BASE_FINAL.Cumulative_Sum_all,
	BASE_FINAL.Cumulative_Sum_all_RC,
	ot_emea_base.YTD_InFULL,
	ot_emea_base.YTD_InFULL_RC,
	round((cast(YTD_InFULL AS DOUBLE) * 100), 2) AS YTD_OTIF,
	round((cast(YTD_InFULL_RC AS DOUBLE) * 100), 2) AS YTD_OTIF_RC,
	TRIM(ot_tar.TARGET) AS TARGET,
	ot_emea_base.days_early_tolerance,
	ot_emea_base.days_end_tolerance,
	ot_emea_base.COUNT_SLOTIF,
	ot_emea_base.SUM_SLOTIF,
		ot_emea_base.EMEA_SLOTIF,
		ot_emea_base.EMEA_YEAR_SLOTIF
FROM (
	SELECT BASE.*,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFULL,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFULL_RC	
	FROM lpfg_core.OTIF_EMEAWISE_BASE BASE 
    
	) ot_emea_base
	
JOIN (select --plant,
             year,
			 sum(MAX_ORDQ_SUM) as ordq_sum_all,
			 sum(MAX_CUM_SUM) as Cumulative_Sum_all, 
			 sum(MAX_CUM_SUM_RC) as Cumulative_Sum_all_RC
		
		from (
		       select --plant,
			          month,
					  year,
					  max(ordq_sum) as MAX_ORDQ_SUM,
					  max(Cumulative_Sum) as MAX_CUM_SUM,
					  max(Cumulative_Sum_RC) as MAX_CUM_SUM_RC
        
		         from lpfg_core.OTIF_EMEAWISE_BASE 
				group by --plant,
				         month,
						 year) a 
						 
	   group by --plant,
	            year
				
	)BASE_FINAL ON ( --ot_emea_base.plant = BASE_FINAL.plant AND 
	                 ot_emea_base.year = BASE_FINAL.year   
					 )
	
LEFT OUTER JOIN lpfg_wrk.OTIF_PLANT_TARGET ot_tar ON (ot_emea_base.PLANT = ot_tar.PLANT);

INSERT overwrite TABLE lpfg_core.OTIF_EMEAWISE_CALC		
SELECT
	Month,
	Year,
	Month_year,
	MAX(ordq_sum), 
	MAX(Cumulative_Sum),
	MAX(Cumulative_Sum_RC),
	MAX(InFull_Month),
	MAX(InFull_Month_RC),		
	MAX(otif_Month ),
	MAX(otif_Month_RC),
	MAX(ordq_sum_all), 
	MAX(Cumulative_Sum_all), 
	MAX(Cumulative_Sum_all_RC),
	MAX(YTD_InFULL ),
	MAX(YTD_InFULL_RC),
	MAX(YTD_OTIF),
	MAX(YTD_OTIF_RC),
	target,
	MAX(EMEA_SLOTIF),
	MAX(EMEA_YEAR_SLOTIF)
FROM lpfg_core.OTIF_CALCULATION_FOR_EMEA
group by
	Month,
	Year,
	month_year,
	target; 