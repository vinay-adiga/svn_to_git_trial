INSERT OVERWRITE TABLE lpfg_core.OTIF_CALCULATION

SELECT distinct calc.plant,
     calc.itemcode AS itemcode,
	calc.suppliercode,
	calc.suppliername,
	CONCAT (
		trim(calc.suppliercode),
		'-',
		TRIM(calc.suppliername)
		) AS SUPPLIER,
	ordd,
	Year,
	CASE 
		WHEN calc.month = '1'
			THEN CONCAT ('Jan')
		WHEN calc.month = '2'
			THEN CONCAT ('Feb')
		WHEN calc.month = '3'
			THEN CONCAT ('Mar')
		WHEN calc.month = '4'
			THEN CONCAT ('Apr')
		WHEN calc.month = '5'
			THEN CONCAT ('May')
		WHEN calc.month = '6'
			THEN CONCAT ('Jun')
		WHEN calc.month = '7'
			THEN CONCAT ('Jul')
		WHEN calc.month = '8'
			THEN CONCAT ('Aug')
		WHEN calc.month = '9'
			THEN CONCAT ('Sep')
		WHEN calc.month = '10'
			THEN CONCAT ('Oct')
		WHEN calc.month = '11'
			THEN CONCAT ('Nov')
		WHEN calc.month = '12'
			THEN CONCAT ('Dec')
		ELSE ''
		END AS month,
	receipt_quantity,
	CALC.DelQnt_Divide_ReqQnt , -- Delivered Qnt/Requested Qnt 
	CALC.DelQnt_Sub_ReqQnt, -- Delivered Qnt - Requested Qnt
	CALC.po_no,
	CALC.po_line,
	days_early_tolerance,
	days_end_tolerance,
	calc.num_del AS Number_of_deliveries,
	po_sku,
	po_sku_ordd,
	CONCAT ( po_sku_ordd, '_', cast(CALC.num_del AS string)) AS po_sku_ReqDate_deliverables,
	plantdesc,
	DELAY,
	ot1 AS on_time,
	ReceivedOnTime,
	ordq,
	ORDER_CREATION_DATE,
	CUM_SUM_RECEIPT_QTY_CALC AS Cumulative_QNT,
	(InFull * 100) AS InFull,
	round((ot1 * InFull * 100), 2) AS otif,
	itemdesc AS Material_Description,
	po_uom,
	gr_UOM AS receipt_uom,
	INCOTERMS,
	INCOTERMS_DESC,
	Receipt_Date,
	STANDARDLT,
	CALC.MATERIAL_TYPE,
	PO_TYPE,
	PO_CLASSIFICATION,
	TO_BE_DISPLAYED,
	CASE WHEN OT_RC.REASON_CODE IS NULL OR OT_RC.REASON_CODE =''
	   then 0
	ELSE cast(OT_RC.REASON_CODE as int)
	END AS REASON_CODE,
	CASE WHEN OT_RC.REASON_CODE IS NULL 
	   then ''
	ELSE OT_RC.ROOT_CAUSE
	END AS ROOT_CAUSE,
	CUM_SUM_RECEIPT_QTY,
	INFULL_ALL,	
	CASE 
	   WHEN ot1 = 1 
	     THEN RECEIVEDONTIME 
	   ELSE 
	     CASE
	       WHEN cast(OT_RC.REASON_CODE as int) = 2 
		        THEN receipt_quantity
	       ELSE 0
	   END
	END AS ReceivedOnTime_RC,
	
	CASE 
	   WHEN  cast(OT_RC.REASON_CODE as int) = 2  
	      THEN CUM_SUM_RECEIPT_QTY 
	   ELSE CUM_SUM_RECEIPT_QTY_CALC
	END AS Cumulative_QNT_RC,
	
	CASE 
	   WHEN ot1 = 1 
	     THEN  (InFull_ALL * 100) 
	ELSE 
	   CASE
	   WHEN cast(OT_RC.REASON_CODE as int) = 2 
		 THEN (InFull_ALL * 100)
	   ELSE 0	
	   END
	END AS InFull_RC,
	
	CASE 
	   WHEN ot1 = 1 
	     THEN round((InFull * 100), 2) 
	ELSE 
	  CASE
	   WHEN cast(OT_RC.REASON_CODE as int) = 2 
        THEN round((InFull_ALL * 100), 2)
	   ELSE 0
	  END
	END AS otif_RC,
    percentage_ordq,
	Percentage_ORDQ_RC,
	low_qty_tol,  -- added for slotif calculation
    high_qty_tol, -- added for slotif calculation
	case when 
			(percentage_ordq between (cast(100 as double) - low_qty_tol) and (cast(100 as double) + high_qty_tol)) and ot1=1
			then 100
			else 0 
			end as slotif,
	case when 
			(Percentage_ORDQ_RC between (cast(100 as double) - low_qty_tol) and (cast(100 as double) + high_qty_tol)) and ot1=1
			then 100
			else 0 
			end as Slotif_RC		
FROM ( 
	SELECT *,
		   CASE 
		   WHEN (ordq <> 0) AND CUM_SUM_RECEIPT_QTY_CALC <= ordq
		   THEN (CUM_SUM_RECEIPT_QTY_CALC / cast(ordq AS DOUBLE))
		   
		   WHEN (ordq <> 0)	AND CUM_SUM_RECEIPT_QTY_CALC > ordq 
		   THEN (ordq - (CUM_SUM_RECEIPT_QTY_CALC - ordq )) / ordq 
		   ELSE 0
		   END AS InFull,
		   
		   CASE 
		   WHEN (ordq <> 0) AND CUM_SUM_RECEIPT_QTY <= ordq
		   THEN (CUM_SUM_RECEIPT_QTY / cast(ordq AS DOUBLE))
		   
		   WHEN (ordq <> 0)	AND CUM_SUM_RECEIPT_QTY > ordq 
		   THEN (ordq - (CUM_SUM_RECEIPT_QTY - ordq )) / ordq 
		   ELSE 0
		   END AS  InFull_ALL
		   
		   
	  FROM lpfg_wrk.OTIF_CALC_BASE 
	  
) CALC 
LEFT OUTER JOIN lpfg_wrk.OTIF_ONTIME_DISPLAY  OT_DISPLAY ON (
	    (regexp_replace(trim(CALC.PO_NO),"^0*",""))=(regexp_replace(trim(OT_DISPLAY.PO_NO),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_LINE),"^0*",""))=(regexp_replace(trim(OT_DISPLAY.PO_LINE),"^0*",""))
	 AND CALC.NUM_DEL=OT_DISPLAY.NUM_DEL
		)
	
LEFT OUTER JOIN lpfg_core.OTIF_REASONCODE OT_RC ON (
		(regexp_replace(trim(CALC.PLANT),"^0*",""))=(regexp_replace(trim(OT_RC.PLANT),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_NO),"^0*",""))=(regexp_replace(trim(OT_RC.PO_NO),"^0*",""))
	 AND(regexp_replace(trim(CALC.PO_LINE),"^0*",""))=(regexp_replace(trim(OT_RC.PO_LINE),"^0*",""))
	);
	
INSERT INTO TABLE lpfg_core.OTIF_CALCULATION
Select ITM_Special_Calc.*,
    OTIF AS percentage_ORDQ,
    OTIF_RC AS percentage_ORDQ_RC,
	5 AS low_qty_tol,  -- added for slotif calculation
    10 AS high_qty_tol, -- added for slotif calculation
	CASE WHEN OTIF between cast(95 as double) and cast(110 as double)
		THEN 100
	  ELSE 0
	END AS slotif,
	CASE WHEN OTIF_RC between cast(95 as double) and cast(110 as double)
		THEN 100
	  ELSE 0
	END AS Slotif_RC
FROM (	
	SELECT ITM_Hist_InFull_Calc.plant,
		ITM_Hist_InFull_Calc.MATERIAL_NO AS itemcode,
	ITM_Hist_InFull_Calc.SUPPLIER_CODE AS suppliercode,
	ITM_Hist_InFull_Calc.SUPPLIER_NAME AS suppliername,
	CONCAT (ITM_Hist_InFull_Calc.SUPPLIER_CODE,'-',ITM_Hist_InFull_Calc.SUPPLIER_NAME) AS SUPPLIER,
	'' ordd,
	Year,
	MONTH AS month,
	ITM_Hist_InFull_Calc.receipt_quantity,
	(ITM_Hist_InFull_Calc.receipt_quantity / ITM_Hist_InFull_Calc.Requested_delivery_quantity)  AS DelQnt_Divide_ReqQnt , -- Delivered quantity/Requested quantity 
    (ITM_Hist_InFull_Calc.receipt_quantity - ITM_Hist_InFull_Calc.Requested_delivery_quantity)  AS DelQnt_Sub_ReqQnt, -- Delivered quantity - Requested quantity
	ITM_Hist_InFull_Calc.po_no,
	ITM_Hist_InFull_Calc.po_line,
	'0' AS days_early_tolerance,
	'0' AS days_end_tolerance,
	0 AS Number_of_deliveries,
	CONCAT(ITM_Hist_InFull_Calc.PO_NO,'_',ITM_Hist_InFull_Calc.MATERIAL_NO) AS po_sku,
	'0' AS  po_sku_ordd,
	'0' AS po_sku_ReqDate_deliverables,
	'' plantdesc,
	0 AS DELAY,
	1 AS on_time,
	ITM_Hist_InFull_Calc.receipt_quantity AS ReceivedOnTime,
	ITM_Hist_InFull_Calc.Requested_delivery_quantity AS ORDQ,
	'' AS ORDER_CREATION_DATE,
	ITM_Hist_InFull_Calc.receipt_quantity AS Cumulative_quantity,
	InFull AS InFull,
	ITM_Hist_InFull_Calc.OTIF AS OTIF,
	ITM_Hist_InFull_Calc.MATERIAL_DESC AS Material_Description,
	'' AS po_uom,
	'' AS receipt_uom,
	'' AS INCOTERMS,
	'' AS INCOTERMS_DESC,
	'' AS Receipt_Date,
	0 AS STANDARDLT,
	'' AS MATERIAL_TYPE,
	TYPE as PO_TYPE,
	'Special' AS PO_CLASSIFICATION,
	'Y' AS TO_BE_DISPLAYED,
	CASE WHEN ITM_Hist_InFull_Calc.REASON_CODE IS NULL OR ITM_Hist_InFull_Calc.REASON_CODE =''
	   then 0
	ELSE cast(ITM_Hist_InFull_Calc.REASON_CODE as int)
	END AS REASON_CODE,
	CASE WHEN ITM_Hist_InFull_Calc.REASON_CODE IS NULL 
	   then ''
	ELSE ITM_Hist_InFull_Calc.ROOT_CAUSE
	END AS ROOT_CAUSE,
	CUM_SUM_RECEIPT_QTY AS CUM_SUM_RECEIPT_QTY,
	InFull_ALL AS INFULL_ALL,	
	 CASE
	       WHEN cast(ITM_Hist_InFull_Calc.REASON_CODE as int) = 2 
		        THEN receipt_quantity
	       ELSE 0
	END AS ReceivedOnTime_RC,
	CASE 
	   WHEN  cast(ITM_Hist_InFull_Calc.REASON_CODE as int) = 2  
	      THEN CUM_SUM_RECEIPT_QTY 
	   ELSE CUM_SUM_RECEIPT_QTY_CALC
	END AS Cumulative_QNT_RC,
	CASE
	   WHEN cast(ITM_Hist_InFull_Calc.REASON_CODE as int) = 2 
		 THEN (InFull_ALL * 100)
	   ELSE 0	
	END AS InFull_RC,
	CASE
	   WHEN cast(ITM_Hist_InFull_Calc.REASON_CODE as int) = 2 
        THEN round((InFull_ALL * 100), 2)
	   ELSE 0
	END AS otif_RC
	FROM ( 
	SELECT ITM_Spcl_Hist.*,
		   CASE 
		   WHEN (Requested_delivery_quantity <> 0) AND CUM_SUM_RECEIPT_QTY_CALC <= Requested_delivery_quantity
		   THEN (CUM_SUM_RECEIPT_QTY_CALC / Requested_delivery_quantity)
		   
		   WHEN (Requested_delivery_quantity <> 0)	AND CUM_SUM_RECEIPT_QTY_CALC > Requested_delivery_quantity 
		   THEN (Requested_delivery_quantity - (CUM_SUM_RECEIPT_QTY_CALC - Requested_delivery_quantity )) / Requested_delivery_quantity 
		   ELSE 0
		   END AS InFull,
		   
		   CASE 
		   WHEN (Requested_delivery_quantity <> 0) AND CUM_SUM_RECEIPT_QTY <= Requested_delivery_quantity
		   THEN (CUM_SUM_RECEIPT_QTY / cast(Requested_delivery_quantity AS DOUBLE))
		   
		   WHEN (Requested_delivery_quantity <> 0)	AND CUM_SUM_RECEIPT_QTY > Requested_delivery_quantity 
		   THEN (Requested_delivery_quantity - (CUM_SUM_RECEIPT_QTY - Requested_delivery_quantity )) / Requested_delivery_quantity 
		   ELSE 0
		   END AS  InFull_ALL,
		OT_RC.REASON_CODE,
		OT_RC.ROOT_CAUSE
FROM(
SELECT KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		receipt_quantity,
		SUM(receipt_quantity) AS CUM_SUM_RECEIPT_QTY,
		SUM(receipt_quantity) AS CUM_SUM_RECEIPT_QTY_CALC,
		MISSED_quantity,
		(SUM(receipt_quantity) + MISSED_quantity) AS Requested_delivery_quantity,
		SUM(receipt_quantity) / (SUM(receipt_quantity) + MISSED_quantity)* 100 AS OTIF 
		FROM lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY  
		GROUP BY KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		receipt_quantity,
		MISSED_quantity
	  )ITM_Spcl_Hist
		LEFT OUTER JOIN lpfg_core.OTIF_REASONCODE OT_RC ON (
		trim(ITM_Spcl_Hist.PLANT)=trim(OT_RC.PLANT)
	 AND trim(ITM_Spcl_Hist.PO_NO)=trim(OT_RC.PO_NO)
	 AND trim(ITM_Spcl_Hist.PO_LINE)=trim(OT_RC.PO_LINE))
   )ITM_Hist_InFull_Calc where cast(OTIF AS string)<>'nan'
  )ITM_Special_Calc ;	
		
INSERT INTO TABLE lpfg_core.OTIF_CALCULATION
Select VDR_Special_Calc.*,
    OTIF AS percentage_ORDQ,
    OTIF_RC AS percentage_ORDQ_RC,
	5 AS low_qty_tol,  -- added for slotif calculation
    10 AS high_qty_tol, -- added for slotif calculation
	CASE WHEN OTIF between cast(95 as double) and cast(110 as double)
		THEN 100
	  ELSE 0
	END AS slotif,
	CASE WHEN OTIF_RC between cast(95 as double) and cast(110 as double)
		THEN 100
	  ELSE 0
	END AS Slotif_RC
FROM (	
	SELECT VDR_Hist_InFull_Calc.plant,
		VDR_Hist_InFull_Calc.MATERIAL_NO AS itemcode,
	VDR_Hist_InFull_Calc.SUPPLIER_CODE AS suppliercode,
	VDR_Hist_InFull_Calc.SUPPLIER_NAME AS suppliername,
	CONCAT (VDR_Hist_InFull_Calc.SUPPLIER_CODE,'-',VDR_Hist_InFull_Calc.SUPPLIER_NAME) AS SUPPLIER,
	'' ordd,
	Year,
	MONTH AS month,
	VDR_Hist_InFull_Calc.receipt_quantity,
	(VDR_Hist_InFull_Calc.receipt_quantity / VDR_Hist_InFull_Calc.Requested_delivery_quantity)  AS DelQnt_Divide_ReqQnt , -- Delivered quantity/Requested quantity 
    (VDR_Hist_InFull_Calc.receipt_quantity - VDR_Hist_InFull_Calc.Requested_delivery_quantity)  AS DelQnt_Sub_ReqQnt, -- Delivered quantity - Requested quantity
	VDR_Hist_InFull_Calc.po_no,
	VDR_Hist_InFull_Calc.po_line,
	'0' AS days_early_tolerance,
	'0' AS days_end_tolerance,
	0 AS Number_of_deliveries,
	CONCAT(VDR_Hist_InFull_Calc.PO_NO,'_',VDR_Hist_InFull_Calc.MATERIAL_NO) AS po_sku,
	'0' AS  po_sku_ordd,
	'0' AS po_sku_ReqDate_deliverables,
	'' plantdesc,
	0 AS DELAY,
	1 AS on_time,
	VDR_Hist_InFull_Calc.receipt_quantity AS ReceivedOnTime,
	VDR_Hist_InFull_Calc.Requested_delivery_quantity AS ORDQ,
	'' AS ORDER_CREATION_DATE,
	VDR_Hist_InFull_Calc.receipt_quantity AS Cumulative_quantity,
	InFull AS InFull,
	VDR_Hist_InFull_Calc.OTIF AS OTIF,
	VDR_Hist_InFull_Calc.MATERIAL_DESC AS Material_Description,
	'' AS po_uom,
	'' AS receipt_uom,
	'' AS INCOTERMS,
	'' AS INCOTERMS_DESC,
	'' AS Receipt_Date,
	0 AS STANDARDLT,
	'' AS MATERIAL_TYPE,
	TYPE as PO_TYPE,
	'Special' AS PO_CLASSIFICATION,
	'Y' AS TO_BE_DISPLAYED,
	CASE WHEN VDR_Hist_InFull_Calc.REASON_CODE IS NULL OR VDR_Hist_InFull_Calc.REASON_CODE =''
	   then 0
	ELSE cast(VDR_Hist_InFull_Calc.REASON_CODE as int)
	END AS REASON_CODE,
	CASE WHEN VDR_Hist_InFull_Calc.REASON_CODE IS NULL 
	   then ''
	ELSE VDR_Hist_InFull_Calc.ROOT_CAUSE
	END AS ROOT_CAUSE,
	CUM_SUM_RECEIPT_QTY AS CUM_SUM_RECEIPT_QTY,
	InFull_ALL AS INFULL_ALL,	
	 CASE
	       WHEN cast(VDR_Hist_InFull_Calc.REASON_CODE as int) = 2 
		        THEN receipt_quantity
	       ELSE 0
	END AS ReceivedOnTime_RC,
	CASE 
	   WHEN  cast(VDR_Hist_InFull_Calc.REASON_CODE as int) = 2  
	      THEN CUM_SUM_RECEIPT_QTY 
	   ELSE CUM_SUM_RECEIPT_QTY_CALC
	END AS Cumulative_QNT_RC,
	CASE
	   WHEN cast(VDR_Hist_InFull_Calc.REASON_CODE as int) = 2 
		 THEN (InFull_ALL * 100)
	   ELSE 0	
	END AS InFull_RC,
	CASE
	   WHEN cast(VDR_Hist_InFull_Calc.REASON_CODE as int) = 2 
        THEN round((InFull_ALL * 100), 2)
	   ELSE 0
	END AS otif_RC
	FROM ( 
	SELECT VDR_Spcl_Hist.*,
		   CASE 
		   WHEN (Requested_delivery_quantity <> 0) AND CUM_SUM_RECEIPT_QTY_CALC <= Requested_delivery_quantity
		   THEN (CUM_SUM_RECEIPT_QTY_CALC / Requested_delivery_quantity)
		   
		   WHEN (Requested_delivery_quantity <> 0)	AND CUM_SUM_RECEIPT_QTY_CALC > Requested_delivery_quantity 
		   THEN (Requested_delivery_quantity - (CUM_SUM_RECEIPT_QTY_CALC - Requested_delivery_quantity )) / Requested_delivery_quantity 
		   ELSE 0
		   END AS InFull,
		   
		   CASE 
		   WHEN (Requested_delivery_quantity <> 0) AND CUM_SUM_RECEIPT_QTY <= Requested_delivery_quantity
		   THEN (CUM_SUM_RECEIPT_QTY / cast(Requested_delivery_quantity AS DOUBLE))
		   
		   WHEN (Requested_delivery_quantity <> 0)	AND CUM_SUM_RECEIPT_QTY > Requested_delivery_quantity 
		   THEN (Requested_delivery_quantity - (CUM_SUM_RECEIPT_QTY - Requested_delivery_quantity )) / Requested_delivery_quantity 
		   ELSE 0
		   END AS  InFull_ALL,
		OT_RC.REASON_CODE,
		OT_RC.ROOT_CAUSE
FROM(
SELECT KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		receipt_quantity,
		SUM(receipt_quantity) AS CUM_SUM_RECEIPT_QTY,
		SUM(receipt_quantity) AS CUM_SUM_RECEIPT_QTY_CALC,
		MISSED_quantity,
		(SUM(receipt_quantity) + MISSED_quantity) AS Requested_delivery_quantity,
		SUM(receipt_quantity) / (SUM(receipt_quantity) + MISSED_quantity)* 100 AS OTIF 
		FROM lpfg_wrk.OTIF_VDR_SPECIAL_PO_HISTORY  
		GROUP BY KEY_ID,
		PLANT,
		MATERIAL_NO,
		MATERIAL_DESC,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		PO_NO,
		PO_LINE,
		type,
		MONTH,
		JNJ_MONTH,
		YEAR,
		receipt_quantity,
		MISSED_quantity
	  )VDR_Spcl_Hist
		LEFT OUTER JOIN lpfg_core.OTIF_REASONCODE OT_RC ON (
		trim(VDR_Spcl_Hist.PLANT)=trim(OT_RC.PLANT)
	 AND trim(VDR_Spcl_Hist.PO_NO)=trim(OT_RC.PO_NO)
	 AND trim(VDR_Spcl_Hist.PO_LINE)=trim(OT_RC.PO_LINE))
   )VDR_Hist_InFull_Calc where cast(OTIF AS string)<>'nan'
  )VDR_Special_Calc ;