
INSERT OVERWRITE TABLE lpfg_wrk.OTIF_MATERIAL
SELECT DISTINCT TRIM(IM.SOURCE)
	,TRIM(IM.REGION)
	,CASE 
		WHEN (
				TRIM(IM.REGION) = 'ZAC'
				AND (
					lower(trim(iwhs)) LIKE 'ph'
					OR lower(trim(iwhs)) LIKE 'pl'
					)
				)
			THEN 'ZA02'
		WHEN (
				TRIM(IM.REGION) = 'ZAC'
				AND (
					lower(trim(iwhs)) NOT LIKE 'ph'
					AND lower(trim(iwhs)) NOT LIKE 'pl'
					)
				)
			THEN 'ZA01'
		ELSE TRIM(IM.REGION)
		END AS env
	,CASE 
		WHEN (
				TRIM(IM.REGION) = 'ZAC'
				AND (
					lower(trim(iwhs)) LIKE 'ph'
					OR lower(trim(iwhs)) LIKE 'pl'
					)
				)
			THEN 'ZA02'
		WHEN (
				TRIM(IM.REGION) = 'ZAC'
				AND (
					lower(trim(iwhs)) NOT LIKE 'ph'
					AND lower(trim(iwhs)) NOT LIKE 'pl'
					)
				)
			THEN 'ZA01'
		ELSE TRIM(IM.REGION)
		END AS subenv
	,CASE 
		WHEN (
				TRIM(IM.REGION) = 'ZAC'
				AND (
					lower(trim(iwhs)) LIKE 'ph'
					OR lower(trim(iwhs)) LIKE 'pl'
					)
				)
			THEN 'Cape Town, South Africa'
		WHEN (
				TRIM(IM.REGION) = 'ZAC'
				AND (
					lower(trim(iwhs)) NOT LIKE 'ph'
					AND lower(trim(iwhs)) NOT LIKE 'pl'
					)
				)
			THEN 'East London, South Africa'
		ELSE CONCAT (
				CITY
				,', '
				,COUNTRY
				)
		END AS plantdesc
	,TRIM(IPROD)
	,TRIM(IDESC)
	,TRIM(IVEND)
	,TRIM(IVEN2)
	,TRIM(IITYP)
	,TRIM(IM.ICLAS)
	,TRIM(ICDES)
	,CASE 
		WHEN (CAST(IFCST AS DOUBLE) IS NULL)
			THEN '0'
		ELSE TRIM(IFCST)
		END AS STPRS
	,CASE 
		WHEN (
				IM.REGION = 'GRC'
				AND TRIM(IUMS) IN (
					'GR'
					,'EA'
					)
				AND TRIM(IUMP) IN (
					'KG'
					,'TH'
					)
				)
			THEN TRIM(IUMP)
		ELSE TRIM(IUMS)
		END AS IUMS
	,TRIM(IUMCN)
	,TRIM(ILOTS)
	,CASE 
		WHEN (CAST(IIOQ AS DOUBLE) IS NULL)
			THEN '0'
		ELSE TRIM(IIOQ)
		END AS IIOQ
	,TRIM(ILEAD)
	,TRIM(IDRAW)
	,TRIM(IGTEC)
	,TRIM(IFCI)
	,CONCAT (
		CAST(CAST(SUBSTR(TRIM(IMDCRT), 1, 2) AS INT) + 1900 + 28 AS string)
		,substr(TRIM(IMDCRT), 3, 2)
		,substr(TRIM(IMDCRT), 5, 2)
		) AS IMDCRT
	,CASE 
		WHEN (
				CAST(CONCAT (
						CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
						,substr(TRIM(PO.PDDTE), 3, 2)
						,substr(TRIM(PO.PDDTE), 5, 2)
						) AS BIGINT) >= CAST(CONCAT (
						SUBSTRING(cast(months_sub(now(), 12) AS string), 1, 4)
						,SUBSTRING(cast(months_sub(now(), 12) AS string), 6, 2)
						,SUBSTRING(cast(months_sub(now(), 12) AS string), 9, 2)
						) AS BIGINT)
				)
			AND (
				CAST(CONCAT (
						CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
						,substr(TRIM(PO.PDDTE), 3, 2)
						,substr(TRIM(PO.PDDTE), 5, 2)
						) AS BIGINT) <= CAST(CONCAT (
						SUBSTRING(cast(months_sub(now(), 6) AS string), 1, 4)
						,SUBSTRING(cast(months_sub(now(), 6) AS string), 6, 2)
						,SUBSTRING(cast(months_sub(now(), 6) AS string), 9, 2)
						) AS BIGINT)
				)
			THEN '6 - 12 Months'
		WHEN (
				CAST(CONCAT (
						CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
						,substr(TRIM(PO.PDDTE), 3, 2)
						,substr(TRIM(PO.PDDTE), 5, 2)
						) AS BIGINT) >= CAST(CONCAT (
						SUBSTRING(cast(months_sub(now(), 3) AS string), 1, 4)
						,SUBSTRING(cast(months_sub(now(), 3) AS string), 6, 2)
						,SUBSTRING(cast(months_sub(now(), 3) AS string), 9, 2)
						) AS BIGINT)
				)
			THEN '0 - 3 Months'
		WHEN (
				CAST(CONCAT (
						CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
						,substr(TRIM(PO.PDDTE), 3, 2)
						,substr(TRIM(PO.PDDTE), 5, 2)
						) AS BIGINT) >= CAST(CONCAT (
						SUBSTRING(cast(months_sub(now(), 6) AS string), 1, 4)
						,SUBSTRING(cast(months_sub(now(), 6) AS string), 6, 2)
						,SUBSTRING(cast(months_sub(now(), 6) AS string), 9, 2)
						) AS BIGINT)
				)
			AND (
				CAST(CONCAT (
						CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
						,substr(TRIM(PO.PDDTE), 3, 2)
						,substr(TRIM(PO.PDDTE), 5, 2)
						) AS BIGINT) <= CAST(CONCAT (
						SUBSTRING(cast(months_sub(now(), 3) AS string), 1, 4)
						,SUBSTRING(cast(months_sub(now(), 3) AS string), 6, 2)
						,SUBSTRING(cast(months_sub(now(), 3) AS string), 9, 2)
						) AS BIGINT)
				)
			THEN '3 - 6 Months'
		WHEN (
				CAST(CONCAT (
						CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
						,substr(TRIM(PO.PDDTE), 3, 2)
						,substr(TRIM(PO.PDDTE), 5, 2)
						) AS BIGINT) < CAST(CONCAT (
						SUBSTRING(cast(months_sub(now(), 12) AS string), 1, 4)
						,SUBSTRING(cast(months_sub(now(), 12) AS string), 6, 2)
						,SUBSTRING(cast(months_sub(now(), 12) AS string), 9, 2)
						) AS BIGINT)
				)
			THEN 'More than 12 Months'
		ELSE 'None'
		END AS PURCHASEHISTORY
	,TRIM(PO.PDDTE)
	,CONCAT (
		CAST(CAST(SUBSTR(TRIM(PO.PDDTE), 1, 2) AS INT) + 1900 + 28 AS string)
		,substr(TRIM(PO.PDDTE), 3, 2)
		,substr(PO.PDDTE, 5, 2)
		) AS PODATE
	,CAST(TRIM(IFCST) AS DOUBLE) AS TOTAL_STD_COST
	,CASE 
		WHEN (IM.REGION = 'GRC')
			THEN CAST(TRIM(IFCST) AS DOUBLE)
		WHEN (IM.REGION <> 'GRC')
			AND CAST(TRIM(ICFT1) AS DOUBLE) > 0
			THEN CAST(TRIM(ICFT1) AS DOUBLE)
		ELSE CAST(TRIM(ICFT2) AS DOUBLE)
		END AS MATERIAL_COST
	,CAST(TRIM(ICFT8) AS DOUBLE) AS FREIGHT_COST
	,CAST(TRIM(ICFT9) AS DOUBLE) AS DUTY_COST
	,0 AS OTHER_COST
	,CASE 
		WHEN IM.IITYP = 'A'
			OR IM.IITYP = 'R'
			THEN 'Chemicals'
		WHEN IM.IITYP = 'B'
			OR IM.IITYP = 'P'
			THEN 'Packaging'
		ELSE 'Others'
		END AS CATEGORY
	,CASE 
		WHEN (
				IA.MATNR IS NULL
				AND NOT (
					trim(IM.IITYP) = 'V'
					AND trim(IM.REGION) = 'ITM'
					)
				)
			THEN 'Yes'
		ELSE 'No'
		END AS ACTIVE,
	IMM.Material_Number AS EMS_MATERIAL_NUMBER,
	IMM.glb_uom
FROM lpfg_stg.IIM IM
LEFT OUTER JOIN (
	SELECT SOURCE
		,REGION
		,PPROD
		,MAX(PDDTE) AS PDDTE
	FROM lpfg_stg.HPO
	GROUP BY SOURCE
		,REGION
		,PPROD
	) PO ON (
		IM.SOURCE = PO.SOURCE
		AND IM.REGION = PO.REGION
		AND IM.IPROD = PO.PPROD
		)
LEFT OUTER JOIN lpfg_stg.PLANT PL ON (substring(PL.bpcs_plant_code, 1, 2) = substring(IM.REGION, 1, 2))
LEFT OUTER JOIN lpfg_stg.IIC IC ON (
		PL.bpcs_plant_code = IC.REGION
		AND TRIM(IM.ICLAS) = TRIM(IC.ICLAS)
		)
LEFT OUTER JOIN lpfg_stg.PPA_INACTIVE_ITEMS IA ON (
		IA.ENV = IM.REGION
		AND regexp_replace(TRIM(IA.MATNR), "^0*", "") = regexp_replace(TRIM(IPROD), "^0*", "")
		)
LEFT OUTER JOIN lpfg_stg.PPA_INV_SOURCE_ID PI ON (
		TRIM(IM.SOURCE) = TRIM(PI.PPA_SOURCE)
		AND TRIM(IM.REGION) = TRIM(PI.PPA_REGION)
		)
LEFT OUTER JOIN lpfg_wrk.inv_matmaster IMM ON (
		TRIM(IM.IPROD) = TRIM(IMM.Material_Number)
		AND TRIM(PI.INV_SOURCE_ID) = TRIM(IMM.SOURCE_ID)
		)
WHERE TRIM(IM.IID) = 'IM' AND (TRIM(IM.ifci) NOT LIKE 'P%' and TRIM(IM.ifci) NOT LIKE 'CL%')
	AND (
		(
			IM.REGION IN (
				'FRM'
				,'ZAC'
				)
			AND IM.IITYP IN (
				'P'
				,'R'
				)
			)
		OR (
			IM.REGION IN (
				'DEM'
				,'ITM'
				,'GRC'
				)
			AND IM.IITYP IN (
				'A'
				,'B'
				)
			)
		OR (
			IM.REGION IN ('ITM')
			AND IM.IITYP IN ('V')
			)
		OR (
			IM.REGION IN ('GRC')
			AND IM.IITYP IN (
				'1'
				,'2'
				)
			)
		);

INSERT INTO TABLE lpfg_wrk.OTIF_MATERIAL
SELECT DISTINCT "MOVEX" AS SOURCE
	,"HBG" AS region
	,'HBG' AS ENV
	,'HBG' AS subenv
	,'Helsingborg, Sweden' AS PLANTDESC
	,trim(MMITNO) AS MATNR
	,trim(MMITDS) AS TXTLG
	,'' AS IVEND
	,'' AS IVEN2
	,trim(MMITTY) AS IITYP
	,'' AS MATKL
	,'' AS MATKLDESC
	,CASE 
		WHEN mchead6.KOCSU9 IS NULL
			AND mchead3.KOCSU9 IS NULL
			AND mchead9.KOCSU9 IS NULL
			THEN "0"
		WHEN mchead6.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(cast(mchead6.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE) AS string)
		WHEN mchead6.KOCSU9 IS NOT NULL
			THEN mchead6.KOCSU9
		WHEN mchead3.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(cast(mchead3.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE) AS string)
		WHEN mchead3.KOCSU9 IS NOT NULL
			THEN mchead3.KOCSU9
		WHEN mchead9.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(cast(mchead9.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE) AS string)
		WHEN mchead9.KOCSU9 IS NOT NULL
			THEN mchead9.KOCSU9
		END AS STPRS
	,trim(MMUNMS) AS IUMS
	,'1' AS IUMCN
	,'' AS ILOTS
	,'0' AS IIOQ
	,mit.mbleat AS ILEAD
	,trim(LBUS11) AS IDRAW
	,'' AS IGTEC
	,'' AS IFCI
	,trim(MMRGDT) AS IMDCRT
	,'' AS PURCHASEHISTORY
	,'' AS PODATE1
	,'' AS PODATE
	,CASE 
		WHEN mchead6.KOCSU9 IS NULL
			AND mchead3.KOCSU9 IS NULL
			AND mchead9.KOCSU9 IS NULL
			THEN 0
		WHEN mchead6.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(mchead6.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE)
		WHEN mchead6.KOCSU9 IS NOT NULL
			THEN cast(mchead6.KOCSU9 AS DOUBLE)
		WHEN mchead3.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(mchead3.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE)
		WHEN mchead3.KOCSU9 IS NOT NULL
			THEN cast(mchead3.KOCSU9 AS DOUBLE)
		WHEN mchead9.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(mchead9.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE)
		WHEN mchead9.KOCSU9 IS NOT NULL
			THEN cast(mchead9.KOCSU9 AS DOUBLE)
		END AS TOTAL_STD_COST
	,CASE 
		WHEN mchead6.KOCSU9 IS NULL
			AND mchead3.KOCSU9 IS NULL
			AND mchead9.KOCSU9 IS NULL
			THEN 0
		WHEN mchead6.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(mchead6.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE)
		WHEN mchead6.KOCSU9 IS NOT NULL
			THEN cast(mchead6.KOCSU9 AS DOUBLE)
		WHEN mchead3.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(mchead3.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE)
		WHEN mchead3.KOCSU9 IS NOT NULL
			THEN cast(mchead3.KOCSU9 AS DOUBLE)
		WHEN mchead9.KOCSU9 IS NOT NULL
			AND trim(mitmas.mmunms) <> trim(mitmas.mmppun)
			THEN cast(mchead9.KOCSU9 AS DOUBLE) * cast(mitaun.mucofa AS DOUBLE)
		WHEN mchead9.KOCSU9 IS NOT NULL
			THEN cast(mchead9.KOCSU9 AS DOUBLE)
		END AS MATERIAL_COST
	,0 AS FREIGHT_COST
	,0 AS DUTY_COST
	,0 AS OTHER_COST
	,CASE 
		WHEN mitmas.MMITTY = '91'
			OR mitmas.MMITTY = '92'
			OR mitmas.MMITTY = '93'
			OR mitmas.MMITTY = '94'
			THEN 'Raw Materials'
		WHEN mitmas.MMITTY = '61'
			OR mitmas.MMITTY = '62'
			OR mitmas.MMITTY = '71'
			OR mitmas.MMITTY = '72'
			OR mitmas.MMITTY = '73'
			OR mitmas.MMITTY = '79'
			THEN 'Packaging'
		ELSE 'Others'
		END AS CATEGORY
	,CASE 
		WHEN (
				IA.MATNR IS NULL
				AND trim(mitmas.MMSTAT) = '20'
				)
			THEN 'Yes'
		ELSE 'No'
		END AS ACTIVE,
	IMM.Material_Number AS EMS_MATERIAL_NUMBER,
	IMM.glb_uom
FROM lpfg_stg.HBG_MITMAS mitmas
LEFT OUTER JOIN (
	SELECT mchead1.koitno
		,mchead1.kostrt
		,mchead1.kocsu9
	FROM lpfg_stg.HBG_MCHEAD mchead1
	JOIN (
		SELECT koitno
			,max(kopcdt) AS maxdate
		FROM lpfg_stg.HBG_MCHEAD
		WHERE kostrt = 'BUD'
		GROUP BY koitno
		) mchead2 ON (
			mchead1.koitno = mchead2.koitno
			AND mchead1.kopcdt = mchead2.maxdate
			)
	) mchead3 ON (
		mitmas.mmitno = mchead3.koitno
		AND mchead3.kostrt = 'BUD'
		)
LEFT OUTER JOIN (
	SELECT mchead4.koitno
		,mchead4.kostrt
		,mchead4.kocsu9
	FROM lpfg_stg.HBG_MCHEAD mchead4
	JOIN (
		SELECT koitno
			,max(kopcdt) AS maxdate
		FROM lpfg_stg.HBG_MCHEAD
		WHERE kostrt = CONCAT (
				"B"
				,substr(cast(now() AS string), 3, 2)
				)
		GROUP BY koitno
		) mchead5 ON (
			mchead4.koitno = mchead5.koitno
			AND mchead4.kopcdt = mchead5.maxdate
			)
	) mchead6 ON (
		mitmas.mmitno = mchead6.koitno
		AND mchead6.kostrt = CONCAT (
			"B"
			,substr(cast(now() AS string), 3, 2)
			)
		)
LEFT OUTER JOIN (
	SELECT mchead7.koitno
		,mchead7.kostrt
		,mchead7.kocsu9
	FROM lpfg_stg.HBG_MCHEAD mchead7
	JOIN (
		SELECT koitno
			,max(kopcdt) AS maxdate
		FROM lpfg_stg.HBG_MCHEAD
		WHERE trim(kostrt) = ''
		GROUP BY koitno
		) mchead8 ON (
			mchead7.koitno = mchead8.koitno
			AND mchead7.kopcdt = mchead8.maxdate
			)
	) mchead9 ON (
		mitmas.mmitno = mchead9.koitno
		AND trim(mchead9.kostrt) = ''
		)
LEFT OUTER JOIN lpfg_stg.HBG_MITMSD_PPA mitmsd ON (trim(mitmsd.LBITNO) = trim(mitmas.MMITNO))
LEFT OUTER JOIN lpfg_stg.HBG_MITAUN_PPA mitaun ON (
		trim(mitaun.muitno) = trim(mitmas.mmitno)
		AND trim(mitaun.mupcof) = '0.000000000'
		)
LEFT OUTER JOIN lpfg_stg.PPA_INACTIVE_ITEMS IA ON (
		IA.ENV = "HBG"
		AND regexp_replace(TRIM(IA.MATNR), "^0*", "") = regexp_replace(TRIM(MMITNO), "^0*", "")
		)
LEFT OUTER JOIN (
	SELECT mbitno
		,min(mbleat) AS mbleat
	FROM lpfg_stg.HBG_MITBAL
	GROUP BY mbitno
	) mit ON (trim(mitmas.MMITNO) = trim(mit.mbitno))
LEFT OUTER JOIN lpfg_stg.PPA_INV_SOURCE_ID PI ON (
		  TRIM(PI.PPA_SOURCE)='MOVEX'
		AND TRIM(mitmas.REGION) = 'HBG'
		)
LEFT OUTER JOIN lpfg_wrk.inv_matmaster IMM ON (
		TRIM(MMITNO) = TRIM(IMM.Material_Number)
		AND TRIM(PI.INV_SOURCE_ID) = TRIM(IMM.SOURCE_ID)
		)
where MMITTY IN ('61', '62', '71', '72', '73', '79', '91', '92', '93', '94');

INSERT INTO TABLE lpfg_wrk.OTIF_MATERIAL
SELECT "SAP" AS SOURCE
	,"VDR" AS region
	,"VDR" AS env
	,"VDR" AS subenv
	,"Val de Reuil, France" AS plantdesc
	,trim(mara.MATNR) AS matnr
	,CASE 
		WHEN makt2.matnr IS NOT NULL
			THEN trim(makt2.MAKTX)
		WHEN makt4.matnr IS NOT NULL
			THEN trim(makt4.MAKTX)
		END AS txtlg
	,'' AS ivend
	,'' AS iven2
	,'' AS iityp
	,'' AS matkl
	,'' AS matkldesc
	,CASE 
		WHEN (
				mb1.total_std_cost IS NULL
				OR mb2.total_std_cost IS NULL
				)
			THEN '0'
		WHEN (
				mc2.matnr IS NOT NULL
				AND mb1.matnr IS NOT NULL
				AND mb1.total_std_cost IS NOT NULL
				)
			THEN cast(mb1.total_std_cost AS string)
		ELSE cast(mb2.total_std_cost AS string)
		END AS stprs
	,trim(mara.MEINS) AS iums
	,CASE 
		WHEN marm.umrez IS NULL
			THEN '1'
		ELSE cast(cast(marm.umrez AS DOUBLE) / cast(marm.umren AS DOUBLE) AS string)
		END AS iumcn
	,'' AS ilots
	,'' AS iioq
	,'' AS ilead
	,'' AS idraw
	,'' AS igtec
	,'' AS ifci
	,trim(mara.ERSDA) AS imdcrt
	,'' AS purchasehistory
	,'' AS podate1
	,'' AS podate
	,CASE 
		WHEN (
				mc2.matnr IS NOT NULL
				AND mb1.matnr IS NOT NULL
				)
			THEN mb1.total_std_cost
		ELSE mb2.total_std_cost
		END AS total_std_cost
	,CASE 
		WHEN (
				mc2.matnr IS NOT NULL
				AND mb1.matnr IS NOT NULL
				)
			THEN mb1.total_std_cost
		ELSE mb2.total_std_cost
		END AS material_cost
	,0 AS freight_cost
	,0 AS duty_cost
	,0 AS other_cost
	,'' AS category
	,CASE 
		WHEN trim(mc2.lvorm) = 'X'
			THEN 'No'
		WHEN (inatv_itm.MATNR IS NULL)
			THEN 'Yes'
		ELSE 'No'
		END AS ACTIVE,
	IMM.Material_Number AS EMS_MATERIAL_NUMBER,
	IMM.glb_uom
FROM lpfg_stg.VDR_MARA mara
LEFT OUTER JOIN lpfg_stg.VDR_MAKT makt2 ON (
		trim(mara.matnr) = trim(makt2.matnr)
		AND trim(makt2.spras) = 'E'
		)
LEFT OUTER JOIN lpfg_stg.VDR_MAKT makt4 ON (
		trim(mara.matnr) = trim(makt4.matnr)
		AND trim(makt4.spras) = 'F'
		)
LEFT OUTER JOIN lpfg_stg.ppa_inactive_items inatv_itm ON (
		inatv_itm.env = 'VDR'
		AND regexp_replace(TRIM(inatv_itm.MATNR), "^0*", "") = regexp_replace(TRIM(mara.matnr), "^0*", "")
		)
LEFT OUTER JOIN (
	SELECT DISTINCT matnr
		,CASE 
			WHEN (cast(STPRS AS DOUBLE) / cast(PEINH AS DOUBLE)) IS NULL
				THEN 0
			ELSE cast(STPRS AS DOUBLE) / cast(PEINH AS DOUBLE)
			END AS total_std_cost
		,bwkey
	FROM lpfg_stg.VDR_MBEW
	) mb1 ON (
		trim(mara.matnr) = trim(mb1.matnr)
		AND trim(mb1.bwkey) = 'CONS'
		)
LEFT OUTER JOIN (
	SELECT DISTINCT matnr
		,CASE 
			WHEN (cast(STPRS AS DOUBLE) / cast(PEINH AS DOUBLE)) IS NULL
				THEN 0
			ELSE cast(STPRS AS DOUBLE) / cast(PEINH AS DOUBLE)
			END AS total_std_cost
		,bwkey
	FROM lpfg_stg.VDR_MBEW
	) mb2 ON (
		trim(mara.matnr) = trim(mb2.matnr)
		AND trim(mb2.bwkey) = '0001'
		)
LEFT OUTER JOIN lpfg_stg.VDR_MARC mc2 ON (
		trim(mara.matnr) = trim(mc2.matnr)
		AND trim(mc2.werks) = '0001'
		AND trim(mc2.y9grp) IN (
			'C'
			,'R'
			,'O'
			,'N'
			,'S'
			)
		AND trim(mc2.beskz) IN (
			'F'
			,'X'
			)
		)
LEFT OUTER JOIN lpfg_stg.VDR_MARM marm ON (
		trim(mara.matnr) = trim(marm.matnr)
		AND trim(mara.bstme) = trim(marm.meinh)
		)
LEFT OUTER JOIN lpfg_stg.PPA_INV_SOURCE_ID PI ON (
		TRIM(mara.SOURCE) = TRIM(PI.PPA_SOURCE)
		AND TRIM(mara.REGION) = TRIM(PI.PPA_REGION)
		)
LEFT OUTER JOIN lpfg_wrk.inv_matmaster IMM ON (
		TRIM(mara.matnr) = TRIM(IMM.Material_Number)
		AND TRIM(PI.INV_SOURCE_ID) = TRIM(IMM.SOURCE_ID)
		);
		
INSERT INTO TABLE lpfg_wrk.OTIF_MATERIAL
SELECT "SAP" AS SOURCE
	,"MAD" AS region
	,"MAD" AS env
	,"MAD" AS subenv
	,"Madrid, Spain" AS plantdesc
	,trim(mara.MATNR) AS matnr
	,CASE 
		WHEN makt2.matnr IS NOT NULL
			THEN trim(makt2.MAKTX)
		WHEN makt3.matnr IS NOT NULL
			THEN trim(makt2.MAKTX)
		WHEN makt4.matnr IS NOT NULL
			THEN trim(makt4.MAKTX)
		END AS txtlg
	,'' AS ivend
	,'' AS iven2
	,'' AS iityp
	,'' AS matkl
	,'' AS matkldesc
	,cast(ROUND(cast(mbew.STPRS AS DOUBLE) / cast(mbew.PEINH AS DOUBLE), 4) AS string) AS stprs
	,trim(mara.MEINS) AS iums
	,CASE 
		WHEN marm.umrez IS NULL
			THEN '1'
		ELSE cast(cast(marm.umrez AS DOUBLE) / cast(marm.umren AS DOUBLE) AS string)
		END AS iumcn
	,'' AS ilots
	,'0' AS iioq
	,'' AS ilead
	,'' AS idraw
	,'' AS igtec
	,'' AS ifci
	,trim(mara.ERSDA) AS imdcrt
	,'' AS purchasehistory
	,'' AS podate1
	,'' AS podate
	,cast(mbew.STPRS AS DOUBLE) / cast(mbew.PEINH AS DOUBLE) AS total_std_cost
	,cast(mbew.STPRS AS DOUBLE) / cast(mbew.PEINH AS DOUBLE) AS material_cost
	,0 AS freight_cost
	,0 AS duty_cost
	,0 AS other_cost
	,'' AS category
	,CASE 
		WHEN trim(mara.lvorm) = 'X'
			THEN 'No'
		WHEN (inatv_itm.MATNR IS NULL)
			THEN 'Yes'
		ELSE 'No'
		END AS ACTIVE,
IMM.Material_Number AS EMS_MATERIAL_NUMBER,
	IMM.glb_uom
FROM lpfg_stg.SAP_EURMDR_MARA_PPA mara
LEFT OUTER JOIN lpfg_stg.SAP_EURMDR_MAKT_PPA makt2 ON (
		trim(mara.matnr) = trim(makt2.matnr)
		AND trim(makt2.spras) = 'E'
		)
LEFT OUTER JOIN lpfg_stg.SAP_EURMDR_MAKT_PPA makt3 ON (
		trim(mara.matnr) = trim(makt3.matnr)
		AND trim(makt3.spras) = 'S'
		)
LEFT OUTER JOIN lpfg_stg.SAP_EURMDR_MAKT_PPA makt4 ON (
		trim(mara.matnr) = trim(makt4.matnr)
		AND trim(makt4.spras) = 'N'
		)
LEFT OUTER JOIN lpfg_stg.ppa_inactive_items inatv_itm ON (
		inatv_itm.env = 'MAD'
		AND regexp_replace(TRIM(inatv_itm.MATNR), "^0*", "") = regexp_replace(TRIM(mara.matnr), "^0*", "")
		)
LEFT OUTER JOIN lpfg_stg.SAP_EURMDR_MBEW_PPA mbew ON (
		trim(mara.matnr) = trim(mbew.matnr)
		AND trim(mbew.bwkey) = 'ESP1'
		)
LEFT OUTER JOIN lpfg_stg.SAP_EURMDR_MARC_PPA marc ON (trim(mara.matnr) = trim(marc.matnr))
LEFT OUTER JOIN lpfg_stg.SAP_EURMDR_MARM_PPA marm ON (
		trim(mara.matnr) = trim(marm.matnr)
		AND trim(mara.bstme) = trim(marm.meinh)
		)
LEFT OUTER JOIN lpfg_stg.PPA_INV_SOURCE_ID PI ON (
		TRIM(mara.SOURCE) = TRIM(PI.PPA_SOURCE)
		AND TRIM(mara.REGION) = TRIM(PI.PPA_REGION)
		)
LEFT OUTER JOIN lpfg_wrk.inv_matmaster IMM ON (
		TRIM(mara.matnr) = TRIM(IMM.Material_Number)
		AND TRIM(PI.INV_SOURCE_ID) = TRIM(IMM.SOURCE_ID)
		);