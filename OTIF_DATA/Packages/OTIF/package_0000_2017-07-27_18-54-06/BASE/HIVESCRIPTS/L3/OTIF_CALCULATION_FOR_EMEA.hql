INSERT overwrite TABLE lpfg_core.OTIF_CALCULATION_FOR_EMEA
SELECT  ot_emea_base.plant,
	CASE 
		WHEN ot_emea_base.month = 1
			THEN CONCAT ('Jan')
		WHEN ot_emea_base.month = 2
			THEN CONCAT ('Feb')
		WHEN ot_emea_base.month = 3
			THEN CONCAT ('Mar')
		WHEN ot_emea_base.month = 4
			THEN CONCAT ('Apr')
		WHEN ot_emea_base.month = 5
			THEN CONCAT ('May')
		WHEN ot_emea_base.month = 6
			THEN CONCAT ('Jun')
		WHEN ot_emea_base.month = 7
			THEN CONCAT ('Jul')
		WHEN ot_emea_base.month = 8
			THEN CONCAT ('Aug')
		WHEN ot_emea_base.month = 9
			THEN CONCAT ('Sep')
		WHEN ot_emea_base.month = 10
			THEN CONCAT ('Oct')
		WHEN ot_emea_base.month = 11
			THEN CONCAT ('Nov')
		WHEN ot_emea_base.month = 12
			THEN CONCAT ('Dec')
		ELSE ''
		END AS Month,
	ot_emea_base.year AS Year,
	CASE 
		WHEN ot_emea_base.month = 1
			THEN CONCAT ('Jan','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 2
			THEN CONCAT ('Feb','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 3
			THEN CONCAT ('Mar','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 4
			THEN CONCAT ('Apr','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 5
			THEN CONCAT ('May','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 6
			THEN CONCAT ('Jun','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 7
			THEN CONCAT ('Jul','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 8
			THEN CONCAT ('Aug','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 9
			THEN CONCAT ('Sep','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 10
			THEN CONCAT ('Oct','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 11
			THEN CONCAT ('Nov','-',cast(ot_emea_base.year AS string))
		WHEN ot_emea_base.month = 12
			THEN CONCAT ('Dec','-',cast(ot_emea_base.year AS string))
		ELSE ''
		END AS Month_year,
	ot_emea_base.ordq_sum,
	ot_emea_base.Cumulative_Sum,
	ot_emea_base.Cumulative_Sum_RC,
	ot_emea_base.On_Time,
	ot_emea_base.InFull_Month AS InFull_Month,
	ot_emea_base.InFull_Month AS InFull_Month_RC,
	round(ot_emea_base.otif_month, 2) AS otif_month,
	round(ot_emea_base.otif_month_RC, 2) AS otif_month_RC,
	BASE_FINAL.ordq_sum_all AS ordq_sum_all,
	BASE_FINAL.Cumulative_Sum_all,
	BASE_FINAL.Cumulative_Sum_all_RC,
	ot_emea_base.YTD_InFULL,
	ot_emea_base.YTD_InFULL_RC,
	round((cast(YTD_InFULL AS DOUBLE) * 100), 2) AS YTD_OTIF,
	round((cast(YTD_InFULL_RC AS DOUBLE) * 100), 2) AS YTD_OTIF_RC,
	TRIM(ot_tar.TARGET) AS TARGET,
	ot_emea_base.days_early_tolerance,
	ot_emea_base.days_end_tolerance,
	ot_emea_base.COUNT_SLOTIF,
	ot_emea_base.SUM_SLOTIF,
		ot_emea_base.PLANT_SLOTIF
FROM (
	SELECT BASE.*,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFULL,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFULL_RC	
	FROM lpfg_core.OTIF_EMEAWISE_BASE BASE 
    
	) ot_emea_base
	
JOIN (select --plant,
             year,
			 sum(MAX_ORDQ_SUM) as ordq_sum_all,
			 sum(MAX_CUM_SUM) as Cumulative_Sum_all, 
			 sum(MAX_CUM_SUM_RC) as Cumulative_Sum_all_RC
		
		from (
		       select --plant,
			          month,
					  year,
					  max(ordq_sum) as MAX_ORDQ_SUM,
					  max(Cumulative_Sum) as MAX_CUM_SUM,
					  max(Cumulative_Sum_RC) as MAX_CUM_SUM_RC
        
		         from lpfg_core.OTIF_EMEAWISE_BASE 
				group by --plant,
				         month,
						 year) a 
						 
	   group by --plant,
	            year
				
	)BASE_FINAL ON ( --ot_emea_base.plant = BASE_FINAL.plant AND 
	                 ot_emea_base.year = BASE_FINAL.year   
					 )
	
LEFT OUTER JOIN lpfg_wrk.OTIF_PLANT_TARGET ot_tar ON (ot_emea_base.PLANT = ot_tar.PLANT);