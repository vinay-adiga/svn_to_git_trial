INSERT overwrite TABLE lpfg_core.OTIF_SUPPLIERWISE_CALC
SELECT plant,
	SupplierCode,
	SupplierName,
	CONCAT (
		trim(suppliercode),
		'-',
		TRIM(suppliername)
		) AS SUPPLIER,
	Month,
	Year,
	Month_year,
	MAX(ordq_sum),
	MAX(Cumulative_Sum),
	MAX(Cumulative_Sum_RC),
	MAX(InFull_Mon_Supplier),
	MAX(InFull_Mon_Supplier_RC),
	MAX(OTIF_Month_Supplier),
	MAX(OTIF_Month_Supplier_RC),
	MAX(ordq_sum_all),
	MAX(Cumulative_Sum_all),
	MAX(Cumulative_Sum_all_RC),
	MAX(YTD_InFull_Supplier),
	MAX(YTD_InFull_Supplier_RC),
	MAX(YTD_OTIF_Supplier),
	MAX(YTD_OTIF_Supplier_RC),
	target,
	MAX(SUPPLIER_SLOTIF)
FROM lpfg_core.OTIF_CALCULATION_FOR_SUPPLIER
GROUP BY plant,
	SupplierCode,
	SupplierName,
	supplier,
	Month,
	Year,
	month_year,
	target;
	