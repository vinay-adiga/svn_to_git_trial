--Changed coding for JIRA-7057,Reason Coding Report by arath3 on 04-07-2017
DROP TABLE IF EXISTS lpfg_stg.OTIF_REASONCODE;

	CREATE EXTERNAL TABLE lpfg_stg.OTIF_REASONCODE (
	
		SOURCE_ID  string,   --Source ID       	       			
		 PLANT string, 		 -- Plant
		 PO_NO string, 		--Purchase Order number
		 PO_LINE string,    -- Line item
		 REASON_CODE int,   -- Reason code
		 ROOT_CAUSE string  -- Root cause
	) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_stg/otif/OTIF_REASONCODE";