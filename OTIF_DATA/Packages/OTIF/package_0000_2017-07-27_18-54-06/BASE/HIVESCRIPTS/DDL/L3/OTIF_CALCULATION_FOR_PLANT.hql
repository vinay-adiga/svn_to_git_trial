DROP TABLE IF EXISTS lpfg_core.OTIF_CALCULATION_FOR_PLANT;

CREATE EXTERNAL TABLE lpfg_core.OTIF_CALCULATION_FOR_PLANT (
		plant STRING,
		Month string,
		Year string,
		Month_year string,
		ordq_sum DOUBLE,
		Cumulative_Sum DOUBLE,
		Cumulative_Sum_RC DOUBLE,
		On_Time DOUBLE,
		InFull_Month DOUBLE,
		InFull_Month_RC DOUBLE,
		otif_Month DOUBLE,
		otif_Month_RC DOUBLE,
		ordq_sum_all DOUBLE,
		Cumulative_Sum_all DOUBLE,
		Cumulative_Sum_all_RC DOUBLE,
		YTD_InFULL DOUBLE,
		YTD_InFULL_RC DOUBLE,
		YTD_OTIF DOUBLE,
		YTD_OTIF_RC DOUBLE,
		target STRING,
		days_early_tolerance STRING,
		days_end_tolerance STRING,
		COUNT_SLOTIF DOUBLE,
		SUM_SLOTIF DOUBLE,
		PLANT_SLOTIF DOUBLE
		) ROW FORMAT delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_core/OTIF_CALCULATION_FOR_PLANT";