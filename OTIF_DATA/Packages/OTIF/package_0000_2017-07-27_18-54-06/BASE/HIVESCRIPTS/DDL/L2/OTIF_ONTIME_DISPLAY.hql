DROP TABLE IF EXISTS lpfg_wrk.OTIF_ONTIME_DISPLAY;

CREATE EXTERNAL TABLE lpfg_wrk.OTIF_ONTIME_DISPLAY (
		PO_NO string,
		PO_LINE string,
		NUM_DEL bigint,
		TO_BE_DISPLAYED string
		) ROW FORMAT delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_ONTIME_DISPLAY";

