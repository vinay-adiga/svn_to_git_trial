DROP TABLE IF EXISTS lpfg_wrk.OTIF_STD_LEAD_TIME;

 CREATE EXTERNAL TABLE lpfg_wrk.OTIF_STD_LEAD_TIME (
  PLANT string,
  MATERIAL string,
  SUPPLIER_CODE string,
  STD_LEAD_TIME string
  ) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_STD_LEAD_TIME";

