DROP TABLE IF EXISTS lpfg_wrk.OTIF_PLANT_TARGET;

	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_PLANT_TARGET (
		 PLANT string
		 ,TARGET string
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_PLANT_TARGET";

   