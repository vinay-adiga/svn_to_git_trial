-- Created  a new table for exclusion of consignment stock  POs  which are belonging to consignment stock transaction type,JIRA:-6925 by arath3 on 30-06-2017
DROP TABLE IF EXISTS lpfg_wrk.OTIF_BPCS_PO_Exclusions;

	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_BPCS_PO_Exclusions (
		 source string,
		 plant string,
		 GR_TYPE string,
		 PO_NO string,
		 MATNR string,
		 PO_LINE string
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_BPCS_PO_Exclusions";