DROP TABLE IF EXISTS lpfg_wrk.OTIF_MATERIAL;

	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_MATERIAL (
		 SOURCE string
		,REGION string
		,ENV string
		,subenv string		
		,PLANTDESC string
		,MATNR string
		,TXTLG string
		,IVEND string
		,IVEN2 string
		,IITYP string
		,MATKL string
		,MATKLDESC string
		,STPRS string
		,IUMS string
		,IUMCN string
		,ILOTS string
		,IIOQ string
		,ILEAD string
		,IDRAW string
		,IGTEC string
		,IFCI STRING
		,IMDCRT STRING
		,PURCHASEHISTORY string
		,PODATE1 STRING
		,PODATE STRING
		,TOTAL_STD_COST DOUBLE
		,MATERIAL_COST DOUBLE
		,FREIGHT_COST DOUBLE
		,DUTY_COST DOUBLE
		,OTHER_COST DOUBLE
		,CATEGORY STRING
		,ACTIVE STRING
		,EMS_MATERIAL_NUMBER STRING
		,GLOBALUOM STRING
		) ROW format delimited fields terminated BY "|"  LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_MATERIAL";
