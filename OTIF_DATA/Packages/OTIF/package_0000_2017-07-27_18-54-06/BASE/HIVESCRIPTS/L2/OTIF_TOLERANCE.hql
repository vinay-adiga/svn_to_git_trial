INSERT overwrite TABLE lpfg_wrk.OTIF_TOLERANCE

SELECT  trim(Details.PLANT) as PLANT,
		trim(Details.PO_NO) as PO_NO,
			cast(YEAR(from_unixtime(unix_timestamp(Details.ordd, 'MM/dd/yyyy')))as string) AS YEAR,
		trim(Details.SUPPLIERCODE) as SUPPLIER_CODES,
	
		CASE 
		when supplier.LOWER_TOLERANCE is NULL  and tol_plant.plant in('ZA01','ZA02') and Details.PROCUREMENT_TYPE='IMP'
				then '5'
		  WHEN supplier.LOWER_TOLERANCE is NULL
    	       THEN  tol_plant.LOWER_TOLERANCE
		  ELSE supplier.LOWER_TOLERANCE
		END AS DAYS_EARLY_TOLERANCE,
		CASE 
		when supplier.UPPER_TOLERANCE is NULL  and tol_plant.plant in('ZA01','ZA02') and Details.PROCUREMENT_TYPE='IMP'
				then '5'
		  WHEN supplier.UPPER_TOLERANCE is NULL
   	           THEN tol_plant.UPPER_TOLERANCE
          ELSE supplier.UPPER_TOLERANCE
        END AS DAYS_END_TOLERANCE	
FROM lpfg_wrk.OTIF_DETAILS Details

LEFT OUTER JOIN lpfg_stg.OTIF_TOLERANCE_BY_SUPPLIER supplier ON (
		regexp_replace(trim(Details.PLANT), "^0*", "")= regexp_replace(trim(supplier.PLANT), "^0*", "")
	AND	regexp_replace(trim(Details.YEAR), "^0*", "") = regexp_replace(trim(supplier.YEAR), "^0*", "")
	AND regexp_replace(trim(Details.SUPPLIERCODE), "^0*", "") = regexp_replace(trim(supplier.SUPPLIER_NO), "^0*", "")
	)
LEFT OUTER JOIN  lpfg_stg.OTIF_TOLERANCE_BY_PLANT tol_plant ON (
	regexp_replace(trim(Details.PLANT), "^0*", "")= regexp_replace(trim(tol_plant.PLANT), "^0*", "")
	AND	regexp_replace(trim(Details.YEAR), "^0*", "") = regexp_replace(trim(tol_plant.YEAR), "^0*", "")
	);
	
	