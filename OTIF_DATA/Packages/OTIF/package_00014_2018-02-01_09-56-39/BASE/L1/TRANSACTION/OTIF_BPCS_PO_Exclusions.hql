-- Created  a new table for exclusion of consignment stock  POs  which are belonging to consignment stock transaction type,JIRA:-6925 by arath3 on 30-06-2017
INVALIDATE METADATA lpfg_stg.ITH;

INSERT OVERWRITE TABLE lpfg_wrk.OTIF_BPCS_PO_Exclusions
With BPCS_FRM AS
(
SELECT trim(ith.source) as source,
trim(ith.region) as plant,
trim(ith.ttype) as GR_TYPE,
    trim(ith.TREF) as PO_NO,
    trim(ith.tprod) AS MATNR,
    trim(ith.thlin) as PO_LINE
     from lpfg_stg.ITH ith  where(trim(ith.region) ='FRM' and trim(ith.ttype) IN('UZ')) -- Removed GR transaction type for Jira ABfy-8563
),
BPCS_ITM AS
(
SELECT trim(ith.source) as source,
trim(ith.region) as plant,
trim(ith.ttype) as GR_TYPE,
    trim(ith.TREF) as PO_NO,
    trim(ith.tprod) AS MATNR,
    trim(ith.thlin) as PO_LINE
     from lpfg_stg.ITH ith  where(trim(ith.region) ='ITM' and trim(ith.ttype) IN('RR','ES')) 
),
BPCS_DEM AS
(
SELECT trim(ith.source) as source,
trim(ith.region) as plant,
trim(ith.ttype) as GR_TYPE,
    trim(ith.TREF) as PO_NO,
    trim(ith.tprod) AS MATNR,
    trim(ith.thlin) as PO_LINE
     from lpfg_stg.ITH ith  where trim(ith.region) ='DEM' and ith.ttype  IS  NULL
),
BPCS_GRC AS
(
SELECT trim(ith.source) as source,
trim(ith.region) as plant,
trim(ith.ttype) as GR_TYPE,
    trim(ith.TREF) as PO_NO,
    trim(ith.tprod) AS MATNR,
    trim(ith.thlin) as PO_LINE
     from lpfg_stg.ITH ith  where (trim(ith.region) ='GRC' and trim(ith.ttype)  IN('EC','ER'))
),
BPCS_ZAC AS
(
SELECT trim(ith.source) as source,
trim(ith.region) as plant,
trim(ith.ttype) as GR_TYPE,
    trim(ith.TREF) as PO_NO,
    trim(ith.tprod) AS MATNR,
    trim(ith.thlin) as PO_LINE
     from lpfg_stg.ITH ith  where trim(ith.region) ='ZAC' and ith.ttype  IS  NULL
)
Select * from BPCS_FRM
UNION ALL
select * from BPCS_ITM
UNION ALL
Select * from BPCS_DEM
UNION ALL
select * from BPCS_ZAC
UNION ALL
select * from BPCS_GRC
;