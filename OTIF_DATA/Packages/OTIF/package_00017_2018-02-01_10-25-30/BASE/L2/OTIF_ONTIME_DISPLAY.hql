INSERT OVERWRITE TABLE lpfg_wrk.OTIF_ONTIME_DISPLAY
SELECT CALC_PO.PO_NO,
	CALC_PO.PO_LINE,
	CALC_OT.NUM_DEL,
	'Y' as TO_BE_DISPLAYED
FROM (
	SELECT PO_NO,
		PO_LINE,
		CASE 
			WHEN SUM(ot1) = 0
				THEN 0
			ELSE 1
			END AS ot1
	FROM LPFG_WRK.OTIF_CALC_BASE
	GROUP BY PO_NO,
		PO_LINE
	) CALC_PO
LEFT OUTER JOIN (
	SELECT PO_NO,
		PO_LINE,
		ot1,
		MAX(NUM_DEL) AS NUM_DEL
	FROM LPFG_WRK.OTIF_CALC_BASE
	GROUP BY PO_NO,
		PO_LINE,
		ot1
	) CALC_OT 

ON (
		CALC_PO.PO_NO = CALC_OT.PO_NO
		AND CALC_PO.PO_LINE = CALC_OT.PO_LINE
		AND CALC_PO.ot1 = CALC_OT.ot1
	);
