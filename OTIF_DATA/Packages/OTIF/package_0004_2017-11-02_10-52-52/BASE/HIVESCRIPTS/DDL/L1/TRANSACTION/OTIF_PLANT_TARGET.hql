DROP TABLE IF EXISTS lpfg_wrk.OTIF_PLANT_TARGET;

	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_PLANT_TARGET (
		 PLANT string
		 ,TARGET string
		) STORED AS PARQUET LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_PLANT_TARGET";

   