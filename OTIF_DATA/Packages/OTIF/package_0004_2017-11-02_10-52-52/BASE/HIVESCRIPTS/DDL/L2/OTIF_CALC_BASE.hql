DROP TABLE IF EXISTS lpfg_wrk.OTIF_CALC_BASE; 

CREATE EXTERNAL TABLE lpfg_wrk.OTIF_CALC_BASE(
		po_sku STRING,
		po_sku_ordd STRING,
		receipt_quantity DOUBLE,
		po_line STRING,
		itemcode STRING,
		po_no STRING,
		ordd STRING,
		Year string,
		Month string,
		suppliercode STRING,
		suppliername STRING,
		plant STRING,
		ordq double,	
		ORDER_CREATION_DATE string,
		receipt_date STRING,
		itemdesc STRING,
		po_uom STRING,
		incoterms STRING,
		INCOTERMS_DESC String,
		gr_uom STRING,
		plantdesc STRING,
		delay INT,
		--supplier_code STRING,
		days_early_tolerance STRING,
		days_end_tolerance STRING,
		ot1 TINYINT,
		ReceivedOnTime DOUBLE,
		CUM_SUM_RECEIPT_QTY_CALC DOUBLE,
		CUM_SUM_RECEIPT_QTY  double,
		num_del BIGINT,
		STANDARDLT bigint,
		Material_Type string,
		PO_TYPE string,	
		PO_CLASSIFICATION STRING,
		PROCUREMENT_TYPE string,		
		low_qty_tol double,  -- added for slotif calculation
        high_qty_tol double, -- added for slotif calculation
		percentage_ordq double -- added for slotif calculation
		) STORED AS PARQUET LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_CALC_BASE";