INSERT OVERWRITE lpfg_core.OTIF_PLANTWISE_BASE 
SELECT   PLANT,
	month,
	year,
	ordq_sum,
	Cumulative_Sum,
	Cumulative_Sum_RC,
	on_time,
	InFull_Month,
	InFull_Month_RC,
	sum(ordq_sum) OVER (PARTITION BY PLANT,year ORDER BY year) AS ordq_sum_all,
	sum(Cumulative_Sum) OVER (PARTITION BY PLANT,year ORDER BY year) AS Cumulative_Sum_all,
	sum(Cumulative_Sum_RC) OVER (PARTITION BY PLANT,year ORDER BY year) AS Cumulative_Sum_all_RC,
	((cast(InFull_Month AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month,
	((cast(InFull_Month_RC AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_RC,
	days_early_tolerance,
	days_end_tolerance,
	SLOTIF ,
		COUNT_SLOTIF ,
		SUM_SLOTIF ,
		(SUM_SLOTIF / COUNT_SLOTIF) * 100 AS PLANT_SLOTIF,
    COUNT_YEAR_SLOTIF ,
		SUM_YEAR_SLOTIF ,
		(SUM_YEAR_SLOTIF / COUNT_YEAR_SLOTIF) * 100 AS YTD_PLANT_SLOTIF		
FROM (
	SELECT DISTINCT ot_calc_ordq.PLANT,
		ot_calc_ordq.month,
		ot_calc_ordq.year,
		ot_calc_ordq.ordq_sum,
		ot_calc_ordq.Cumulative_Sum,
		ot_calc_ordq.Cumulative_Sum_RC,
		ot_calc_ordq.on_time,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_RC,
		ot_calc_ordq.days_early_tolerance,
		ot_calc_ordq.days_end_tolerance,
		ot_calc_ordq.SLOTIF ,
		ot_calc_ordq.COUNT_SLOTIF ,
		ot_calc_ordq.SUM_SLOTIF, 
		ot_calc_ordq.COUNT_YEAR_SLOTIF, 
		ot_calc_ordq.SUM_YEAR_SLOTIF
	FROM (
		SELECT month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS month,
			cast(year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) as string) AS year,
			calc.del,
			calc.po_sku_ordd,
			OT_CALC.PLANT,
			OT_CALC.on_time,
			ordq_calc.ordq_sum AS ordq_sum,
			sum(cast(OT_CALC.Cumulative_QNT AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum,
				
			sum(cast(OT_CALC.Cumulative_QNT_RC AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum_RC,
				
			OT_CALC.days_early_tolerance,
			OT_CALC.days_end_tolerance,
			OT_CALC.SLOTIF ,
				count() OVER (
				PARTITION BY OT_CALC.PLANT,
							month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_SLOTIF, --count
			count() OVER (
				PARTITION BY OT_CALC.PLANT,
							--month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
							year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_YEAR_SLOTIF,
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY OT_CALC.PLANT,
														month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_SLOTIF,
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
														PARTITION BY OT_CALC.PLANT,
														--month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
														year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_YEAR_SLOTIF
		FROM lpfg_core.OTIF_CALCULATION OT_CALC
		LEFT OUTER JOIN lpfg_core.OTIF_PLANT_ORDQ_CALC ordq_calc ON (
				    OT_CALC.po_sku_ordd = ordq_calc.po_sku_ordd
				AND OT_CALC.PLANT = ordq_calc.PLANT
				),
			(
				SELECT max(number_of_deliveries) AS del,
					po_sku_ordd
				FROM lpfg_core.OTIF_CALCULATION 
				GROUP BY po_sku_ordd
				) calc
		WHERE OT_CALC.po_sku_ordd = calc.po_sku_ordd
			AND OT_CALC.number_of_deliveries = calc.del
		) ot_calc_ordq
	) ot_plant_base;