INSERT OVERWRITE TABLE lpfg_core.OTIF_SUPPLIERWISE_BASE
SELECT PLANT,
	suppliercode,
	suppliername,
	month,
	year,
	ordq_sum,
	Cumulative_Sum,
	Cumulative_Sum_RC,
	on_time,
	InFull_Month_Supplier,
	InFull_Month_Supplier_RC,
	sum(ordq_sum) OVER (
		PARTITION BY suppliercode,year ORDER BY year
		) AS ordq_sum_all,
	sum(Cumulative_Sum) OVER (
		PARTITION BY suppliercode,year ORDER BY year
		) AS Cumulative_Sum_all,
	sum(Cumulative_Sum_RC) OVER (
		PARTITION BY suppliercode,year ORDER BY year
		) AS Cumulative_Sum_all_RC,
	((cast(InFull_Month_Supplier AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_Supplier,
	((cast(InFull_Month_Supplier_RC AS DOUBLE) * cast(on_time AS DOUBLE)) * 100) AS OTIF_Month_Supplier_RC,
	days_early_tolerance,
	days_end_tolerance,
	RECEIPT_QUANTITY,
	RECEIVEDONTIME,
	REASON_CODE,
	CUM_SUM_RECEIPT_QTY,
	INFULL_ALL,
	ReceivedOnTime_RC,
	Cumulative_QNT_RC,
	InFull_RC,
	Otif_RC,
	SLOTIF,
	COUNT_SLOTIF,
	SUM_SLOTIF,
	(SUM_SLOTIF / COUNT_SLOTIF) * 100 AS SUPPLIER_SLOTIF
FROM (
	SELECT DISTINCT ot_calc_ordq.PLANT,
		ot_calc_ordq.Suppliercode,
		ot_calc_ordq.suppliername,
		-- ot_calc_ordq.supplier,
		ot_calc_ordq.month,
		ot_calc_ordq.year,
		ot_calc_ordq.ordq_sum,
		ot_calc_ordq.Cumulative_Sum,
		ot_calc_ordq.Cumulative_Sum_RC,
		ot_calc_ordq.on_time,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_Supplier,
		CASE 
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) <= (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) / cast(ot_calc_ordq.ordq_sum AS DOUBLE))
			WHEN (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE)) > (cast(ot_calc_ordq.ordq_sum AS DOUBLE))
				THEN (cast(ot_calc_ordq.ordq_sum AS DOUBLE) - (cast(ot_calc_ordq.Cumulative_Sum_RC AS DOUBLE) - cast(ot_calc_ordq.ordq_sum AS DOUBLE))) / cast(ot_calc_ordq.ordq_sum AS DOUBLE)
			ELSE 0
			END AS InFull_Month_Supplier_RC,	
		ot_calc_ordq.days_early_tolerance,
		ot_calc_ordq.days_end_tolerance,
		ot_calc_ordq.RECEIPT_QUANTITY,
		ot_calc_ordq.RECEIVEDONTIME,
		ot_calc_ordq.REASON_CODE,
		ot_calc_ordq.CUM_SUM_RECEIPT_QTY,
		ot_calc_ordq.INFULL_ALL,
		ot_calc_ordq.ReceivedOnTime_RC,
		ot_calc_ordq.Cumulative_QNT_RC,
		ot_calc_ordq.InFull_RC,
		ot_calc_ordq.Otif_RC,
		ot_calc_ordq.SLOTIF,
		ot_calc_ordq.COUNT_SLOTIF,
		ot_calc_ordq.SUM_SLOTIF
		FROM (
		SELECT month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS month,
			cast(year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) AS string) AS year,
			calc.del,
			calc.po_sku_ordd,
			OT_CALC.PLANT,
			OT_CALC.Suppliercode,
			OT_CALC.suppliername,
			OT_CALC.on_time,
			ordq_calc.ordq_sum AS ordq_sum,
			sum(cast(OT_CALC.Cumulative_QNT AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum,
			sum(cast(OT_CALC.Cumulative_QNT_RC AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))) DESC ROWS BETWEEN UNBOUNDED PRECEDING
						AND UNBOUNDED FOLLOWING
				) AS Cumulative_Sum_RC,	
			OT_CALC.days_early_tolerance,
			OT_CALC.days_end_tolerance,
			OT_CALC.RECEIPT_QUANTITY,
			OT_CALC.RECEIVEDONTIME,
			OT_CALC.REASON_CODE,
			OT_CALC.CUM_SUM_RECEIPT_QTY,
			OT_CALC.INFULL_ALL,
			OT_CALC.ReceivedOnTime_RC,
			OT_CALC.Cumulative_QNT_RC,
			OT_CALC.InFull_RC,
			OT_CALC.Otif_RC,
			OT_CALC.slotif,
			count() OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) AS COUNT_SLOTIF, --count
			sum(cast(ot_calc.slotif AS DOUBLE)) OVER (
				PARTITION BY OT_CALC.PLANT,
				OT_CALC.Suppliercode,
				month(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy'))),
				year(from_unixtime(unix_timestamp(OT_CALC.ordd, 'MM/dd/yyyy')))
				) / 100 AS SUM_SLOTIF --sum
			
		FROM lpfg_core.OTIF_CALCULATION OT_CALC
		LEFT OUTER JOIN lpfg_core.OTIF_SUPPLIER_ORDQ_CALC ordq_calc ON (
				OT_CALC.po_sku_ordd = ordq_calc.po_sku_ordd
				AND OT_CALC.PLANT = ordq_calc.PLANT
				),
			(
				SELECT max(number_of_deliveries) AS del,
					po_sku_ordd
				FROM lpfg_core.OTIF_CALCULATION 
				GROUP BY po_sku_ordd
				) calc
		WHERE OT_CALC.po_sku_ordd = calc.po_sku_ordd
			AND OT_CALC.number_of_deliveries = calc.del
		) ot_calc_ordq
	) ot_suplier_base;
