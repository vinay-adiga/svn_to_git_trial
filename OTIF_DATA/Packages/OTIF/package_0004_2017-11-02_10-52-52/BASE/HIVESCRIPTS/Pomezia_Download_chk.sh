#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE=$4
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select tenpj from lpfg_stg.DTEC where  teddt='Y' and teny8=regexp_replace(substr(cast(now() AS string), 1, 10),'-','')">log

JNJMONTH=$(cat log|grep -Ewo "[0-9]?[0-9]")
echo "$JNJMONTH"

#Checking month of POMEZIA special Po Input Script and Comparing month of DTEC table month
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(JNJ_MONTH) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">POMEZIA_PO_INPUT_log

POMEZIA_INPUT_MONTH=$(cat POMEZIA_PO_INPUT_log|grep -Ewo "[0-9]?[0-9]")
echo "$POMEZIA_INPUT_MONTH"

count=`expr $JNJMONTH - 2`
echo "$count"

if  [ $count == $POMEZIA_INPUT_MONTH ];

then echo "JNJ Month is :"$JNJMONTH "and Current Month is :"$POMEZIA_INPUT_MONTH "Need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi

#Checking count(*) of POMEZIA Po History Temp table and if count(*) = 0 then run the script else no need to run
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP">POMEZIA_PO_HIST_TMP_COUNT_Log

POMEZIA_PO_HIST_TMP=$(cat POMEZIA_PO_HIST_TMP_COUNT_Log|grep -Ewo "[0-9]?[0-9]")
echo "$POMEZIA_PO_HIST_TMP"

if [ $POMEZIA_PO_HIST_TMP == 0 ];
then echo "Count of OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP Script"
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE>>itm_temp3.txt 2>&1 

 echo "***1**"
hadoop fs -put -f itm_temp3.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs
  
else
 echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP Script"
fi

#Checking Sum of Missed Quantity of POMEZIA Input script and if the same is > 0 then running POMEZIA Input script else no need to run the script.
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select sum(MISSED_QUANTITY) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">POMEZIA_SUM_MISS_QTY_log

POMEZIA_SUM_MISSED_QTY=$(cat POMEZIA_SUM_MISS_QTY_log|grep -Ewo "[0-9]*")

echo "$POMEZIA_SUM_MISSED_QTY"

if [ $POMEZIA_SUM_MISSED_QTY -gt 0 ];
then 
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE >>itm_temp.txt 2>&1 

 echo "***1**"
hadoop fs -put -f itm_temp.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo " Missed Quantity got Updated So,need to run OTIF_POMEZIA_SPECIAL_PO_HISTORY Script to get Updated Quantity so History_Temp Table is running"

#echo "Connecting to HBASE Shell"
#hbase shell
#echo "truncate 'lpfg_wrk:OTIF_POMEZIA_SPECIAL_PO_INPUT'" | hbase shell

else
	echo "Missed Quantity is 0 so no need to move to Next level table"
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
 echo "Running Current month in JNJ Calendar..So No need to Move OTIF_POMEZIA_SPECIAL_PO_INPUT data to OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP Script"
fi

#Cheking Month_year 
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(MONTH_YEAR) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY ">POMEZIA_PO_HIST_Month_Log

#PO_HIST_Month=$(cat PO_HIST_Month_Log|grep -Ewo "[A-Z][a-z]*?.[0-9]*")
POMEZIA_PO_HIST_Month=$(cat POMEZIA_PO_HIST_Month_Log|grep -Ewo "[0-9]*")
echo "$POMEZIA_PO_HIST_Month"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(MONTH_YEAR) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT ">POMEZIA_PO_INPUT_Month_Log
#PO_INPUT_Month=$(cat PO_INPUT_Month_Log|grep -Ewo "[A-Z][a-z]*?.[0-9]*")
POMEZIA_PO_INPUT_Month=$(cat POMEZIA_PO_INPUT_Month_Log|grep -Ewo "[0-9]*")
echo "$POMEZIA_PO_INPUT_Month"

 echo "$POMEZIA_PO_INPUT_Month"
if [ $POMEZIA_SUM_MISSED_QTY -gt 0 ];
then
echo "Client has updated Missed quantity for Current Month..So need to Overwrite current month Missed Quantity Values"
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "INSERT OVERWRITE TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY select * from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY WHERE month_year <>'$POMEZIA_PO_INPUT_Month' UNION select KEY_ID,PLANT,MATERIAL_NO,MATERIAL_DESC,SUPPLIER_CODE,SUPPLIER_NAME,PO_NO,PO_LINE,type,JNJ_MONTH,MONTH,YEAR,MONTH_YEAR,RECEIPT_QUANTITY,MISSED_QUANTITY  from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT where month_year = '$POMEZIA_PO_INPUT_Month'">>itm_temp1.txt 2>&1 

 echo "***1**"
hadoop fs -put -f itm_temp1.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs
 
 echo "Check"
 else
	echo "History table has correct data no need to run"
fi