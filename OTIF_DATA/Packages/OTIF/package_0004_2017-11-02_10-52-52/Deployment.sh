#!/bin/bash

echo '---------- Changing Mode of package 20 -----------'
chmod -R 771 /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0004_2017-11-02_10-52-52

echo '---------- Changing user of Service Account-----------'
chown -R salpfged /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0004_2017-11-02_10-52-52

echo '---------- Getting into the directory of package 20-----------'
cd /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0004_2017-11-02_10-52-52

echo '---------- Changing mode of deploy.sh-----------'
chmod 755 deploy.sh

echo '---------- Running deploy.sh-----------'
sh deploy.sh



echo '---------- Running the DDL Script of OTIF -----------'

echo '---------- DDL Script of OTIF -----------' > LOG_Package_04

sh FINAL/HIVESCRIPTS/DDL/DDL_OTIF.sh >> LOG_Package_04 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of OTIF Executed Successfully'
else
    echo "DDL Script of OTIF executed with error code=$EXIT_STATUS"
fi


echo '---------- getting into the directory and running the DDL Script of ITM and VDR  HBase table -----------'

cd /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0004_2017-11-02_10-52-52/FINAL/HIVESCRIPTS/DDL/L1/TRANSACTION
beeline -u 'jdbc:hive2://itsusraedld01.jnj.com:10000/default;principal=hive/itsusraedld01.jnj.com@EU.JNJ.COM' -f OTIF_POMEZIA_SPECIAL_PO_INPUT.hql >>Package_04_ITM_HBase 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of HBase table Executed Successfully'
else
    echo "DDL Script of HBase table executed with error code=$EXIT_STATUS"
fi

beeline -u 'jdbc:hive2://itsusraedld01.jnj.com:10000/default;principal=hive/itsusraedld01.jnj.com@EU.JNJ.COM' -f OTIF_VDR_SPECIAL_PO_INPUT.hql >>Package_04_VDR_HBase 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of HBase table Executed Successfully'
else
    echo "DDL Script of HBase table executed with error code=$EXIT_STATUS"
fi


#echo '---------- Running OTIF Upload  workflow -----------' >> LOG_Package_04

#oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/FINAL/oozie_wf/OTIF_WEEKEND_UPLOAD_WF/job.properties -run >> LOG_Package_04 2>&1 

#EXIT_STATUS=$?

#if [ "$EXIT_STATUS" -eq "0" ]
#then
  #   echo 'OTIF Upload Workflow Executed Successfully'
#else
 #   echo "OTIF Upload Workflow executed with error code=$EXIT_STATUS"
#fi

echo '---------- Running OTIF UMB  workflow -----------' >> LOG_Package_04

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/FINAL/oozie_wf/OTIF_WEEKEND_UMB_WF/job.properties -run >> LOG_Package_04 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF UMB Workflow Triggered Successfully'
else
    echo "OTIF UMB Workflow Triggered with error code=$EXIT_STATUS"
fi