#!/bin/bash

echo '---------- Changing Mode of package 20 -----------'
chmod -R 771 /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25

echo '---------- Changing user of Service Account-----------'
chown -R salpfgep /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25

echo '---------- Getting into the directory of package 20-----------'
cd /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25

echo '---------- Changing mode of deploy.sh-----------'
chmod 755 deploy.sh

echo '---------- Running deploy.sh-----------'
sh deploy.sh

echo '---------- Running the DDL Script of OTIF -----------'

echo '---------- DDL Script of OTIF -----------' > LOG_Package_07

sh FINAL/HIVESCRIPTS/DDL/DDL_OTIF.sh >> LOG_Package_07 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of OTIF Executed Successfully'
else
    echo "DDL Script of OTIF executed with error code=$EXIT_STATUS"
fi

echo '---------- getting into the directory and running the DDL Script of Reasoncode HBase table -----------'

cd /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/DDL/L3
beeline -u 'jdbc:hive2://itsusraedlp02.jnj.com:10000/default;principal=hive/itsusraedlp02.jnj.com@EU.JNJ.COM' -f OTIF_REASONCODE_INPUT.hql >>Package_05_Reasoncode_HBase 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of Reasoncode HBase table Executed Successfully'
else
    echo "DDL Script of Reasoncode HBase table executed with error code=$EXIT_STATUS"
fi

echo '---------- getting into the directory and running the DML Script of Reasoncode HBase table ---------'

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedlp02.jnj.com:21000 -f '/data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/L3/OTIF_REASONCODE_INPUT.hql' >>Package_05_Reasoncode_HBase_DML 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DML Script of Reasoncode HBase table Executed Successfully'
else
    echo "DML Script of Reasoncode HBase table executed with error code=$EXIT_STATUS"
fi


hadoop fs -put -f /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/Reasoncode_chk.sh /prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS

echo '---------- getting into the directory and running the DDL Script of POMEZIA Input HBase table -----------'

cd /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/DDL/L1/TRANSACTION/
beeline -u 'jdbc:hive2://itsusraedlp02.jnj.com:10000/default;principal=hive/itsusraedlp02.jnj.com@EU.JNJ.COM' -OTIF_POMEZIA_SPECIAL_PO_INPUT.hql >>Package_07_ITM_INPUT_HBase_DDL 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of POMEZIA HBase table Executed Successfully'
else
    echo "DDL Script of POMEZIA HBase table executed with error code=$EXIT_STATUS"
fi

echo '---------- getting into the directory and running the DML Script of Reasoncode HBase table ---------'

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedlp02.jnj.com:21000 -f '/data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/L1/TRANSACTION/OTIF_POMEZIA_SPECIAL_PO_INPUT.hql' >>Package_07_ITM_INPUT_HBase_DML 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DML Script of POMEZIA HBase table Executed Successfully'
else
    echo "DML Script of POMEZIA HBase table executed with error code=$EXIT_STATUS"
fi


hadoop fs -put -f /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/Pomezia_Download_chk.sh /prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS

hadoop fs -put -f /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/Pomezia_Upload_chk.sh /prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS

echo '---------- getting into the directory and running the DDL Script of VDR Input HBase table -----------'

cd /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/DDL/L1/TRANSACTION/
beeline -u 'jdbc:hive2://itsusraedlp02.jnj.com:10000/default;principal=hive/itsusraedlp02.jnj.com@EU.JNJ.COM' -OTIF_VDR_SPECIAL_PO_INPUT.hql >>Package_07_VDR_INPUT_HBase_DDL 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of VDR HBase table Executed Successfully'
else
    echo "DDL Script of VDR HBase table executed with error code=$EXIT_STATUS"
fi

echo '---------- getting into the directory and running the DML Script of Reasoncode HBase table ---------'

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedlp02.jnj.com:21000 -f '/data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/L1/TRANSACTION/OTIF_VDR_SPECIAL_PO_INPUT.hql' >>Package_07_VDR_INPUT_HBase_DML 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DML Script of VDR HBase table Executed Successfully'
else
    echo "DML Script of VDR HBase table executed with error code=$EXIT_STATUS"
fi


hadoop fs -put -f /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/VDR_Download_chk.sh /prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS

hadoop fs -put -f /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/HIVESCRIPTS/VDR_Upload_chk.sh /prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS


echo '---------- Running OTIF UMB workflow -----------' >> LOG_Package_07

oozie job -oozie https://itsusraedlp02.jnj.com:11443/oozie -config /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/oozie_wf/OTIF_WEEKEND_UMB_WF/job.properties -run >> LOG_Package_07 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Workflow Executed Successfully'
else
    echo "OTIF Workflow executed with error code=$EXIT_STATUS"
fi


echo 'Going to schedule OTIF Coordinator'

echo '----------OTIF_L0_TO_L4_COORDINATOR-----------' >> LOG_Package_00


cd  /data/data01/prod/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0007_2018-01-05_13-08-25/FINAL/oozie_coord/OTIF_L0_TO_L4_COORDINATOR >> LOG_Package_07 2>&1 


pwd 2>&1

echo 'OTIF Oozie Coordinator Run'

oozie job -oozie https://itsusraedlp02.jnj.com:11443/oozie -config job.properties -submit -timezone UTC -Dstart_date=`date -u "+%Y-%m-%dT%H:00Z"` -Dotif_frequency="00 10 * * *" 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'Coordinator Scheduled Successfully'
else
    echo "The Coordinator Scheduled interrupted and Executed with error code=$EXIT_STATUS"
fi