INSERT overwrite TABLE lpfg_core.OTIF_SUPPLIERWISE_CALC
SELECT supplier.plant,
	supplier.SupplierCode,
	supplier.SupplierName,
	supplier.SUPPLIER,
	supplier.Month,
	supplier.Year,
	supplier.Month_year,
	supplier.ordq_sum,
	supplier.Cumulative_Sum,
	supplier.Cumulative_Sum_RC,
	supplier.InFull_Mon_Supplier,
	supplier.InFull_Mon_Supplier_RC,
	supplier.OTIF_Month_Supplier,
	supplier.OTIF_Month_Supplier_RC,
	supplier.ORDQ_SUM_ALL,
	ordq_sum_calc.sum_ordq_sum_all AS SUM_ORDQ_SUM_ALL,
	supplier.Cumulative_Sum_all,
	supplier.Cumulative_Sum_all_RC,
	supplier.YTD_InFull_Supplier,
	supplier.YTD_InFull_Supplier_RC,
	supplier.YTD_OTIF_Supplier,
	supplier.YTD_OTIF_Supplier_RC,
	supplier.target,
	supplier.SUPPLIER_SLOTIF,
	supplier.YTD_SUPPLIER_SLOTIF ,
	supplier.SUPPLIER_SLOTIF_RC ,
	supplier.YTD_SUPPLIER_SLOTIF_RC
FROM lpfg_core.OTIF_MAX_CALC_SUPPLIERWISE supplier
JOIN (
	SELECT suppliercode,
		--month,
		year,
		sum(distinct ordq_sum_all) AS sum_ordq_sum_all
	FROM lpfg_core.OTIF_MAX_CALC_SUPPLIERWISE
	GROUP BY suppliercode,
		--month,
		year
	) ordq_sum_calc ON (
		supplier.suppliercode = ordq_sum_calc.suppliercode
		--AND supplier.month = ordq_sum_calc.month
		AND supplier.year = ordq_sum_calc.year
		);		