INSERT overwrite TABLE lpfg_wrk.OTIF_TOLERANCE
SELECT Details.plant,
	Details.PO_NO,
	Details.YEAR,
	Details.SUPPLIERCODE,
	CASE when Details.plant in('ZA01','ZA02') and Details.PROCUREMENT_TYPE='IMP'
				then '5'
		  ELSE '0'
		END AS DAYS_EARLY_TOLERANCE,
	CASE when Details.plant in('ZA01','ZA02') and Details.PROCUREMENT_TYPE='IMP'
				then '5'
		  ELSE '0'
		END AS DAYS_END_TOLERANCE
FROM lpfg_wrk.OTIF_DETAILS Details;
