#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE_PO=$4
QUERY_FILE_INPUT=$5
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

#Running and checking count(*) of VDR SPECIAL PO Script
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_VDR_SPECIAL_PO">VDR_PO_COUNT_log

VDR_PO_COUNT=$(cat VDR_PO_COUNT_log|grep -Ewo "[0-9]*")
echo "$VDR_PO_COUNT"

if [ $VDR_PO_COUNT == 0 ];
then echo "Count of OTIF_VDR_SPECIAL_PO is 0 So,need to run OTIF_VDR_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_PO
  
 else
    echo "Record count is not 0..So No need to Run OTIF_VDR_SPECIAL_PO Script"
fi

#Running and checking count(*) of VDR SPECIAL PO Input Script
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -V -q "select count(*) from lpfg_wrk.OTIF_VDR_SPECIAL_PO_INPUT">VDR_PO_INPUT_COUNT_log

VDR_PO_INPUT_COUNT=$(cat VDR_PO_INPUT_COUNT_log|grep -Ewo "[0-9]*")

echo "$VDR_PO_INPUT_COUNT"

if [ $VDR_PO_INPUT_COUNT == 0 ];
then echo "Count of OTIF_VDR_SPECIAL_PO_INPUT is 0 So,need to run OTIF_VDR_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT
  
 else
    echo "Record count is not 0..So No need to Run OTIF_VDR_SPECIAL_PO_INPUT Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -V -q "select distinct regexp_replace(substr(cast(add_months(cast (concat(substr(TENY8,1,4),'-',substr(TENY8,5,2),'-','01') as timestamp), - 1) AS string), 1, 7),'-','') AS DTEC_MONTH_YEAR from lpfg_STG.DTEC where  teddt='Y' and teny8=regexp_replace(substr(cast(now() AS string), 1, 10),'-','')">log

JNJMONTH=$(cat log|grep -Ewo "[0-9]*")
echo "$JNJMONTH"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -V -q "select distinct regexp_replace(substr(cast(add_months(cast (concat(substr(MONTH_YEAR,1,4),'-',substr(month_year,5,2),'-','01') as timestamp), - 0) AS string), 1, 7),'-','') from lpfg_wrk.OTIF_VDR_SPECIAL_PO">VDR_PO_log

VDR_MONTH=$(cat VDR_PO_log|grep -Ewo "[0-9][0-9][0-9][0-9]*")
echo "$VDR_MONTH"

if [ $JNJMONTH != $VDR_MONTH ];

then 
echo "JNJ Month is :" $JNJMONTH  and "Current Month is :" $VDR_MONTH "Need to run OTIF_VDR_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_PO >VDR_PO_Script_log 2>&1

echo "Check"
hadoop fs -put -f VDR_PO_Script_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo "Connecting to HBASE Shell and truncating VDR Special PO Input table"
#hbase shell
echo "truncate 'lpfg_wrk:OTIF_VDR_SPECIAL_PO_INPUT'" | hbase shell >>VDR_HBase_INPUT.txt 2>&1

hadoop fs -put -f VDR_HBase_INPUT.txt /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs

echo "JNJ Month is :"$JNJMONTH "and Current Month is :"$VDR_INPUT_MONTH "So,need to run OTIF_VDR_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE_INPUT >VDR_PO_INPUT_Script_log 2>&1

echo "Check Input "
hadoop fs -put -f VDR_PO_INPUT_Script_log /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/Logs
 
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_VDR_SPECIAL_PO Script and OTIF_VDR_SPECIAL_PO_INPUT Script "
fi