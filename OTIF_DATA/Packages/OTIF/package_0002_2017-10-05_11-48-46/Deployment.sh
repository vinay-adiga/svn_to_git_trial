#!/bin/bash

echo '---------- Changing Mode of package 20 -----------'
chmod -R 771 /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46

echo '---------- Changing user of Service Account-----------'
chown -R salpfged /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46

echo '---------- Getting into the directory of package 20-----------'
cd /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46

echo '---------- Changing mode of deploy.sh-----------'
chmod 755 deploy.sh

echo '---------- Running deploy.sh-----------'
sh deploy.sh



echo '---------- Running the DDL Script of OTIF -----------'

echo '---------- DDL Script of OTIF -----------' > LOG_Package_02

sh FINAL/HIVESCRIPTS/DDL/DDL_OTIF.sh >> LOG_Package_02 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of OTIF Executed Successfully'
else
    echo "DDL Script of OTIF executed with error code=$EXIT_STATUS"
fi

echo '---------- Uploading files from Edge node to HDFS -----------' >> LOG_Package_01

hadoop fs -put -f /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/sqoop_log_otif.sh /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS >> LOG_Package_02 2>&1

hadoop fs -put -f /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/sqoop_log_driver_otif.sh /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS >> LOG_Package_02 2>&1

hadoop fs -put -f /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/FINAL/HIVESCRIPTS/Download_chk.sh /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS >> LOG_Package_02 2>&1

hadoop fs -put -f /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/FINAL/HIVESCRIPTS/Upload_chk.sh /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS >> LOG_Package_02 2>&1


EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'Files uploaded Successfully'
else
    echo "Files uploaded with error code=$EXIT_STATUS"
fi

echo '---------- Running OTIF workflow -----------' >> LOG_Package_02

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0002_2017-10-05_11-48-46/FINAL/oozie_wf/OTIF_WEEKEND_UMB_WF/job.properties -run >> LOG_Package_02 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Workflow Executed Successfully'
else
    echo "OTIF Workflow executed with error code=$EXIT_STATUS"
fi