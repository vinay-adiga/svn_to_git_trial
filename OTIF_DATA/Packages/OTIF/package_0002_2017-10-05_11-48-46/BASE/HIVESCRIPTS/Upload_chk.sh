#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE=$4
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO">PO_COUNT_log

PO_COUNT=$(cat PO_COUNT_log|grep -Ewo "[0-9]?[0-9]")
echo "$PO_COUNT"

if [ $PO_COUNT == 0 ]
then echo "Count of OTIF_POMEZIA_SPECIAL_PO is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
  
 else
    echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">PO_INPUT_COUNT_log

PO_INPUT_COUNT=$(cat PO_INPUT_COUNT_log|grep -Ewo "[0-9]?[0-9]")

echo "$PO_INPUT_COUNT"

if [ $PO_INPUT_COUNT == 0 ]
then echo "Count of OTIF_POMEZIA_SPECIAL_PO_INPUT is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
  
 else
    echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select tenpj from lpfg_stg.DTEC where  teddt='Y' and teny8=regexp_replace(substr(cast(now() AS string), 1, 10),'-','')">log

JNJMONTH=$(cat log|grep -Ewo "[0-9]?[0-9]")
echo "$JNJMONTH"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select distinct JNJ_MONTH from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO">PO_log

MONTH=$(cat PO_log|grep -Ewo "[0-9]?[0-9]")
echo "$MONTH"

count=`expr $JNJMONTH - 2`
echo "$count"

if [ $count == $MONTH ]
then echo "JNJ Month is :" $JNJMONTH  and "Current Month is :" $MONTH "Need to run OTIF_POMEZIA_SPECIAL_PO Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
  
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO Script"
fi


impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select distinct JNJ_MONTH from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">PO_INPUT_log

INPUT_MONTH=$(cat PO_INPUT_log|grep -Ewo "[0-9]?[0-9]")
echo "$INPUT_MONTH"

count=`expr $JNJMONTH - 2`
echo "$count"

if  [ $count == $INPUT_MONTH ]

then echo "JNJ Month is :$JNJMONTH and Current Month is :$INPUT_MONTH So,need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
	impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi
