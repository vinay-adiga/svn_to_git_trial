-- Created  a new table for exclusion of consignment stock  POs  which are belonging to consignment stock transaction type,JIRA:-6925 by arath3 on 30-06-2017
--Changed coding for LeadTime Mapping for BPCS plants,JIRA:-7032 by arath3 on 03-07-2017
INSERT OVERWRITE TABLE  lpfg_wrk.OTIF_BPCS_POGR 
WITH ITEH AS 
    (   SELECT trim(ith.TREF) as tref,
             trim(ith.THLIN) as thlin,
             trim(ith.TPROD) as tprod,
             trim(ith.SOURCE) as source,
             trim(ith.REGION) as region ,
             trim(ite.tapo) as tapo,
             trim(ith.ttdte) as ttdte,
    ith.TQTY as tqty
        FROM LPFG_STG.ITH ITH ,LPFG_STG.ITE ITE 
        WHERE TRIM(ITH.TTYPE)=TRIM(ITE.TTYPE) 
        AND TRIM(ITE.TAPO)='Y'  -- changed for bcps dec 2016 data
    )
SELECT TRIM(hpo.SOURCE) AS SOURCE,
    TRIM(hpo.REGION) AS REGION,
 CASE 
  WHEN (
    TRIM(hpo.REGION) = 'ZAC'
    AND (
     lower(trim(pwhse)) LIKE 'cp'
     OR lower(trim(pwhse)) LIKE 'cq'
     )
    )
   THEN 'ZA02'
  WHEN (TRIM(hpo.REGION) = 'ZAC')
   THEN 'ZA01'
  ELSE TRIM(hpo.REGION)
  END AS env,
 TRIM(hpo.REGION) AS WERKS,
    TRIM(hpo.PPROD) AS MATNR,
 trim(iim.IDESC) AS MATNR_DESC,
 CAST(CAST(trim(iteh.TTDTE) AS BIGINT) + 19280000 AS STRING) AS RECEIPT_DATE,
    TRIM(hpo.PORD) AS EBELN,
    TRIM(hpo.PLINE) AS EBELP,
 CASE 
     WHEN trim(hpo.PUM) <> trim(iim.IUMS) AND TRIM(hpo.PUM) = 'G' 
       THEN (cast(iteh.TQTY as double)/cast(trim(hpo.pumcn) as double) / 1000 )
     WHEN TRIM(hpo.PUM) = 'G' 
       THEN cast(iteh.TQTY AS Double) / 1000
  WHEN trim(hpo.PUM) <> trim(iim.IUMS) AND TRIM(hpo.PUM) IN ('TO','MI','TH','KM') 
       THEN (cast(iteh.TQTY as double)/cast(trim(hpo.pumcn) as double) * 1000 )
     WHEN TRIM(hpo.PUM) IN ('TO','MI','TH','KM')  
       THEN cast(iteh.TQTY AS Double) * 1000  
  WHEN trim(hpo.PUM) <> trim(iim.IUMS) 
       THEN (cast(iteh.TQTY as double)/cast(trim(hpo.pumcn) as double))
 ELSE cast(iteh.TQTY AS Double)
 END AS MENGE,
 CASE 
     WHEN trim(hpo.PUM) = 'G' 
       THEN (cast(hpox.PXOQTY AS Double) / 1000)
  WHEN trim(hpo.PUM) IN ('TO' ,'MI','TH','KM') 
       THEN (cast(hpox.PXOQTY AS Double) * 1000)
     ELSE cast(hpox.PXOQTY AS Double) 
 END AS ORDQ,
 CASE 
     WHEN trim(hpo.PUM) = 'G' OR trim(hpo.PUM) = 'TO'
         THEN 'KG'
  WHEN trim(hpo.PUM) = 'MI' OR TRIM(hpo.PUM) = 'TH'
         THEN 'EA'
  WHEN trim(hpo.PUM) = 'KM'
         THEN 'M'
     ELSE trim(hpo.PUM) 
  END AS GR_UOM,
 TRIM(hpo.PVEND) AS LIFNR,
    CONCAT (
        TRIM(hpo.PORD),
        TRIM(hpo.PLINE)
        ) AS EBELC,
    CAST(CAST(trim(hpo.PEDTE) AS BIGINT) + 19280000 AS STRING) AS PO_CREATION_DATE,
 CAST(CAST(trim(hpox.PXRDTE) AS BIGINT) + 19280000 AS STRING) AS REQUESTED_DATE,
        TRIM(hpo.PECST) AS NETPR,
    '1' AS PEINH,
    TRIM(hpo.PQORD),  
    CASE 
  WHEN TRIM(hpo.PUM) = 'G' OR TRIM(hpo.PUM) = 'TO'
        THEN 'KG'
  WHEN trim(hpo.PUM) = 'MI' OR TRIM(hpo.PUM) = 'TH'
         THEN 'EA'
  WHEN trim(hpo.PUM) = 'KM'
         THEN 'M'
  ELSE TRIM(hpo.PUM)
    END AS PUM,
 TRIM(PECST),
 TRIM(hpo.PUM) AS MEINS,
   TRIM(PACST) AS DMBTR,
 TRIM(POCUR) AS WARERS,
 TRIM(POCUR) AS WAERSB,
 TRIM(PWHSE),
 TRIM(PTMKY),
 TRIM(PIEXRT),
 TRIM(PACST),
 TRIM(hpo.PTMKY) AS inco,
 '' AS inco_desc,
 '' AS PO_TYPE,
 'Regular' AS PO_CLASSIFICATION,
 CAST(CAST(trim(hpox.PXRDTE) AS BIGINT) + 19280000 AS STRING) AS ORDD,
    CAST(CAST(trim(HPO.PDDTE) AS BIGINT) + 19280000 AS STRING) AS RDD,
cast(trim(HPO.PQORD)as DOUBLE) AS RDQ,
'' AS RDQ_UOM,
'' AS RECEIPT_QTY_UOM,
'' AS ORDQ_UOM,
CASE 
WHEN trim(hpo.REGION)='DEM' THEN 'Wuppertal,Germany'
WHEN trim(hpo.REGION)='GRC' THEN 'Mandra,Greece' 
WHEN trim(hpo.REGION)='FRM' THEN 'Sezanne,France'
WHEN TRIM(hpo.REGION)='ITM' THEN 'Pomezia,Italy'
WHEN TRIM(hpo.REGION)='ZAC' THEN 'South Africa'
END AS PLANT_DESC,
CASE 
 WHEN TRIM(cic.ICLEAD) IS NULL
    THEN ''
 ELSE TRIM(cic.ICLEAD) 
END AS STD_LEAD_TIME,
CASE 
 WHEN TRIM(hpo.REGION) = 'ZAC' AND trim(hpo.pocur)='ZAR' THEN 'DOM'
 WHEN TRIM(hpo.REGION) = 'ZAC' THEN 'IMP'
ELSE ''
END AS PROCUREMENT_TYPE
FROM  lpfg_stg.HPO hpo
LEFT OUTER JOIN lpfg_stg.BPCS_HPOX hpox ON 
                                    (
                     trim(hpo.PORD)=trim(hpox.PXORD)
                     AND trim(hpo.PLINE)=trim(hpox.PXPLIN)
                     AND trim(hpo.SOURCE)=trim(hpox.SOURCE)
                     AND trim(hpo.REGION)=trim(hpox.REGION)
                     AND CAST(CAST(trim(hpox.PXRDTE) AS BIGINT) + 19280000 AS STRING) >='20150101'
                     AND CAST(CAST(trim(hpox.PXRDTE) AS BIGINT) + 19280000 AS STRING) <=from_unixtime(unix_timestamp(now()),'yyyyMMdd')
                     ) 
LEFT OUTER JOIN ITEH ON             (
         trim(hpox.PXORD)=trim(iteh.TREF)--PO_NO
         AND trim(hpox.PXPLIN)=trim(iteh.THLIN)--PO_LINE
         AND trim(hpox.PXITEM)=trim(iteh.TPROD)--MATERIAL_NO
         AND trim(hpox.SOURCE)=trim(iteh.SOURCE)
         AND trim(hpox.REGION)=trim(iteh.REGION)
         )
LEFT OUTER JOIN (select max(ICLEAD) as ICLEAD,trim(ICPROD) as ICPROD,TRIM(ICFAC) AS ICFAC,source,region from lpfg_stg.BPCS_CIC group by ICPROD,ICFAC,source,region) CIC ON (
         trim(hpo.PPROD)=trim(cic.ICPROD)
   AND trim(hpo.POFAC)=trim(cic.ICFAC)
         AND trim(hpo.SOURCE)=trim(cic.SOURCE)
         AND trim(hpo.REGION)=trim(cic.REGION)
   )
 JOIN (select * from lpfg_stg.IIM where(
                                        CASE 
             WHEN trim(iim.REGION) = 'DEM' THEN trim(iim.iityp) IN ('A' , 'B')
             WHEN trim(iim.REGION) = 'FRM' THEN trim(iim.iityp) IN ('P' , 'R')
             WHEN trim(iim.REGION) = 'GRC' THEN trim(iim.iityp) IN ('A' , 'B')
             WHEN trim(iim.REGION) = 'ITM' THEN trim(iim.iityp) IN ('A' , 'B' ,'V')
             WHEN trim(iim.REGION) = 'ZAC' THEN (trim(iim.iityp) IN ('P' , 'R') AND trim(iim.IVEND) <> '0')
                                        END
          )AND TRIM(iim.ifci) NOT LIKE 'P%'  AND TRIM(iim.ifci) NOT LIKE 'CL%'
 )iim ON (
         trim(hpo.PPROD)=trim(iim.IPROD)
         AND trim(hpo.SOURCE)=trim(iim.SOURCE)
         AND trim(hpo.REGION)=trim(iim.REGION)
        )   --Changed for jira-7393 by arath3 on 15-07-2017      
Where CAST(CAST(trim(hpox.PXRDTE) AS BIGINT) + 19280000 AS STRING)>='20150101' AND CAST(CAST(trim(hpox.PXRDTE) AS BIGINT) + 19280000 AS STRING)<=from_unixtime(unix_timestamp(now()),'yyyyMMdd') and (trim(hpo.PID) not in('PZ') or cast(trim(hpo.PQREC)as double) > 0);