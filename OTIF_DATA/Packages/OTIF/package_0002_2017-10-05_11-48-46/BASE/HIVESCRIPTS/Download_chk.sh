#!/bin/sh
KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
QUERY_FILE=$4
LOG=shell-impala-$USER-$(date +%s%N).log
export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

echo "$KEYTAB"
#redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1
/usr/bin/klist -e >>${LOG} 2>&1

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select tenpj from lpfg_stg.DTEC where  teddt='Y' and teny8=regexp_replace(substr(cast(now() AS string), 1, 10),'-','')">log

JNJMONTH=$(cat log|grep -Ewo "[0-9]?[0-9]")
echo "$JNJMONTH"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select distinct JNJ_MONTH from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">PO_INPUT_log

INPUT_MONTH=$(cat PO_INPUT_log|grep -Ewo "[0-9]?[0-9]")
echo "$INPUT_MONTH"

count=`expr $JNJMONTH - 2`
echo "$count"

if  [ $count == $INPUT_MONTH ]

then echo "JNJ Month is :$JNJMONTH and Current Month is :$INPUT_MONTH Need to run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
 else
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select count(*) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP">PO_HIST_TMP_COUNT_Log

PO_HIST_TMP=$(cat PO_HIST_TMP_COUNT_Log|grep -Ewo "[0-9]?[0-9]")
echo "$PO_HIST_TMP"

if [ $PO_HIST_TMP == 0 ]
then echo "Count of OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP is 0 So,need to run OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP Script"
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE
  
else
 echo "Record count is not 0..So No need to Run OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select sum(MISSED_QUANTITY) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT">SUM_MISS_QTY_log

SUM_MISSED_QTY=$(cat SUM_MISS_QTY_log|grep -Ewo "[0-9]*")

echo "$SUM_MISSED_QTY"

if [ $SUM_MISSED_QTY -gt 0 ]
then 
impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -f $QUERY_FILE

echo " Missed Quantity got Updated So,need to run OTIF_POMEZIA_SPECIAL_PO_HISTORY Script to get Updated Quantity so History_Temp Table is running"

#echo "Connecting to HBASE Shell"
#hbase shell
#echo "truncate 'lpfg_wrk:OTIF_POMEZIA_SPECIAL_PO_INPUT'" | hbase shell

else
	echo "Missed Quantity is 0 so no need to move to Next level table"
    echo "Running Current month in JNJ Calendar..So No need to Run OTIF_POMEZIA_SPECIAL_PO_INPUT Script"
 echo "Running Current month in JNJ Calendar..So No need to Move OTIF_POMEZIA_SPECIAL_PO_INPUT data to OTIF_POMEZIA_SPECIAL_PO_HISTORY_TEMP Script"
fi

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(MONTH_YEAR) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY ">PO_HIST_Month_Log

#PO_HIST_Month=$(cat PO_HIST_Month_Log|grep -Ewo "[A-Z][a-z]*?.[0-9]*")
PO_HIST_Month=$(cat PO_HIST_Month_Log|grep -Ewo "[0-9]*")
echo "$PO_HIST_Month"

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "select MAX(MONTH_YEAR) from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT ">PO_INPUT_Month_Log

#PO_INPUT_Month=$(cat PO_INPUT_Month_Log|grep -Ewo "[A-Z][a-z]*?.[0-9]*")
PO_INPUT_Month=$(cat PO_INPUT_Month_Log|grep -Ewo "[0-9]*")
echo "$PO_INPUT_Month"

if  [ $PO_HIST_Month -eq $PO_INPUT_Month ]

then echo "Client has updated Missed quantity for Current Month..So need to Overwrite current month Missed Quantity Values"
echo "$PO_INPUT_Month"
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "INSERT OVERWRITE TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY select KEY_ID,PLANT,MATERIAL_NO,MATERIAL_DESC,SUPPLIER_CODE,SUPPLIER_NAME,PO_NO,PO_LINE,JNJ_MONTH,MONTH,YEAR,MONTH_YEAR,RECEIPT_QUANTITY,MISSED_QUANTITY  from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_INPUT where month_year = $PO_INPUT_Month">HIST_MONTH_QTY
 echo "Chk"
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -V -q "INSERT INTO TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY select * from lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_HISTORY ">HIST_MONTH_QTY
 echo "Check"

 else
    echo "History table has correct data no need to run"
fi