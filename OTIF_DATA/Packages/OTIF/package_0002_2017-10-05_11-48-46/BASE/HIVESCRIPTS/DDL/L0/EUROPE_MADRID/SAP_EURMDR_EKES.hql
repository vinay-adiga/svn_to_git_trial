DROP TABLE IF EXISTS lpfg_stg.SAP_EURMDR_EKES;

CREATE EXTERNAL TABLE lpfg_stg.SAP_EURMDR_EKES (
SAP_ID string,
MANDT string,
EBELN string,
EBELP string,
ETENS string,
EBTYP string,
EINDT string,
LPEIN string,
UZEIT string,
ERDAT string,
EZEIT string,
MENGE string,
DABMG string,
ESTKZ string,
LOEKZ string,
KZDIS string,
XBLNR string,
VBELN string,
VBELP string,
MPROF string,
EMATN string
) PARTITIONED BY (SOURCE STRING,REGION STRING) ROW format delimited fields terminated BY "|" ;

ALTER TABLE lpfg_stg.SAP_EURMDR_EKES ADD PARTITION (SOURCE='SAP',region = 'EURMDR') 
LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/EUROPE_MADRID/TRANSACTION/EKES';



