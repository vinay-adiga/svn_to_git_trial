#!/bin/bash

# paranoia with shell scripts, always to be encouraged

KEYTAB=$1
PRINCIPAL=$2
CONN_PARAM=$3
USERNAME=$4
PASSWORD=$5
SQOOPQUERY=$6
WORKFLOWNAME=$7
SERVER=$8
TABLENAME=$9
SYSTEM=${10}
PLANT=${11}
COORDINATORNAME=${12}
TARGETDIR=${13}
SQOOPDIR=${14}
DRIVER=${15}

LOG=sqoop_log-$USER-$(date +%s%N)-$TABLENAME.log

ADATE=
ATIME=


# echo-ing params to find logs on local fs in case of error
echo "HOST: ${HOSTNAME}"
echo "LOG: ${LOG}"
echo "SQOOPQUERY:${SQOOPQUERY}"

if [ $# -ne 15 ];
then
  ERROR="Usage: $0 <keytab-file> <kerberos-principal> <query-file> <impalad_host:impalad_port>"
  echo "$ERROR"
  echo "$ERROR" >> ${LOG}
  exit 1
fi

export PYTHON_EGG_CACHE=./myeggs

if [ ! -f $KEYTAB ];
then
  ERROR="Unable to access [ keytab-file: $KEYTAB ], check existence and permissions"
  echo "$ERROR"
  echo "$ERROR" >> ${LOG}
  exit 2
fi

# redirecting both stderror and stdout to append to the log-file
/usr/bin/kinit -kt $KEYTAB -V "$PRINCIPAL" >>${LOG} 2>&1

/usr/bin/klist -e >>${LOG} 2>&1

#
#SQOOP log function
#
function sqooplog()  {
  STATUS=$1
  ALOG=$(tail -75 ${LOG} | sed -e "s/'/ /g")
  #echo "$ALOG"
 echo "Running Impala action"
TEST=$(echo "${SQOOPQUERY}"  | sed "s/'/\\\'/g")
echo "$TEST"

 hadoop fs -get /dev/edl/sc/consumer/lpfg/str/lpfg_stg/TESTPPA/FILES/TEST/lpfg_ex_date.txt

RUNDATE=`cat lpfg_ex_date.txt`
echo "$RUNDATE"
IFS='\ ' read -r -a array <<< "$RUNDATE"
echo "After ifs"

ADATE=$(date +%F)
ATIME=$(date +%R)

 INQUERY="insert into lpfg_wrk.PPA_SQOOP_LOG_TABLE values 
('"${WORKFLOWNAME}"','"${COORDINATORNAME}"','"${SYSTEM}"','"${TABLENAME}"','"${PLANT}"','"${ADATE}"','"${ATIME}"','"${SERVER}"','"${STATUS}"','"${TEST}"','"${ALOG}"');"
  impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $CONN_PARAM -q "$INQUERY" >>${LOG} 2>&1
  hadoop fs -put ${LOG} /dev/edl/sc/consumer/lpfg/appcode/logs
   rm ${LOG}
 echo "Running Impala action Successful"

 }

echo "Checking overwriteflag  :"
TABLENAME_LOWER=$(echo "$TABLENAME" | tr '[:upper:]' '[:lower:]')
 impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i itsusraedld02.jnj.com -q "select overwrite_flow10,flow8location from lpfg_stg.api_control where lower(l0_tablename)='$TABLENAME_LOWER'" --quiet>/tmp/overwriteflag$SYSTEM$TABLENAME.log
 overwrite_flow10=$(tail -n +4 /tmp/overwriteflag$SYSTEM$TABLENAME.log| head -n +1| awk '{ print $2; }')
 FLOW8DIR=$(tail -n +4 /tmp/overwriteflag$SYSTEM$TABLENAME.log| head -n +1| awk '{ print $4; }')
 echo "Overwrite Flag :"$overwrite_flow10
 echo "Flow8Dir :"$FLOW8DIR
rm /tmp/overwriteflag$SYSTEM$TABLENAME.log
 #if [ "$overwrite_flow10" == "true" ]
 #then
 #	echo $FLOW8DIR
 #	echo "hadoop fs -cp ${FLOW8DIR} ${SQOOPDIR}"
 #	hadoop fs -rmr ${SQOOPDIR}
 #	hadoop fs -cp ${FLOW8DIR} ${SQOOPDIR}
#else
#echo "sqoop import --driver $DRIVER --connect $SERVER --username $USERNAME --password $PASSWORD --query "$SQOOPQUERY" --target-dir $SQOOPDIR --hive-drop-import-delims --fields-terminated-by '|' -m 1 --delete-target-dir"
 
sqoop import --driver $DRIVER --connect $SERVER --username $USERNAME --password $PASSWORD --query "$SQOOPQUERY" --target-dir $SQOOPDIR --hive-drop-import-delims --fields-terminated-by '|' -m 1 --delete-target-dir  >>${LOG} 2>&1
	RC=$?
	if [ $RC -ne 0 ]
	then
	    hadoop fs -put -f  ${LOG} /dev/edl/sc/consumer/lpfg/appcode/logs/
	    echo "sqooping not finish successfully, [ rc = $RC ]"
		echo "sqoop failed for the table:"$TABLENAME | mailx -s "SQOOP FAILED"anuradha.meldoddi@relevancelab.com,rsamshee@ITS.JNJ.COM,anindita.rath@relevancelab.com,amit.sahu@relevancelab.com,ashis.dey@relevancelab.com,sudhir.satapathy@relevancelab.com,ashok.kumar@relevancelab.com
	    sqooplog FAILURE
		exit 0
	fi
	
	partfilesize=$(hadoop fs -ls  "$SQOOPDIR"/part-m-00000|awk '{print $5}'|sed 's/[^0-9]*//g')
	
	if [ $partfilesize -gt 0 ];
	then 
	   echo "Sqoop Action Successfull"
	     sqooplog SUCCESS
		 hadoop fs -rm -f -r $TARGETDIR
		 hadoop fs -mkdir $TARGETDIR
		 hadoop fs -mv $SQOOPDIR/* $TARGETDIR
		 #echo "After audit insert: $(date +%T)"
		 exit $RC
	else
	   hadoop fs -put -f ${LOG} /dev/edl/sc/consumer/lpfg/appcode/logs/
	   echo "Sqoop did not finish successfully, [ rc = $RC ]" >>${LOG}
	   sqooplog FAILURE
	   exit 0
	fi
rm $LOG 
#fi