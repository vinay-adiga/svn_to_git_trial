DROP TABLE IF EXISTS lpfg_stg.OTIF_TARGET;

	CREATE EXTERNAL TABLE lpfg_stg.OTIF_TARGET (
		 PLANT string
		 ,TARGET string
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_stg/otif/OTIF_TARGET";