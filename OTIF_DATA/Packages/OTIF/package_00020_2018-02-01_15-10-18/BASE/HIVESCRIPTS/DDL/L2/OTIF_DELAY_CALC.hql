DROP TABLE IF EXISTS lpfg_wrk.OTIF_DELAY_CALC;

CREATE EXTERNAL TABLE lpfg_wrk.OTIF_DELAY_CALC (
	   PLANT String,
	   ITEMCODE String,
	   PO_NO String,
	   PO_LINE String,
	   ORDD String,
	   RECEIPT_DATE String,
	   RECEIPT_QUANTITY Double,
	   DELAY BIGINT,
	   OLD_DELAY Int
		) STORED AS PARQUET LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_DELAY_CALC";