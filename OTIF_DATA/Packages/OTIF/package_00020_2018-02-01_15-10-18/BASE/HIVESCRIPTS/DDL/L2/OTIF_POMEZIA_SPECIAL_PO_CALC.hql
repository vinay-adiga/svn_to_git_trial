DROP TABLE IF EXISTS lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_CALC;
	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_POMEZIA_SPECIAL_PO_CALC (
		KEY_ID String,
		PLANT String,
		MATERIAL_NO String,
		MATERIAL_DESC String,
		SUPPLIER_CODE String,
		SUPPLIER_NAME String,
		PO_NO String,
		PO_LINE String,
		TYPE String,
		JNJ_MONTH INT,
		MONTH String,
		YEAR String,
		MONTH_YEAR String,
		RECEIPT_QUANTITY DOUBLE,
		MISSED_QUANTITY DOUBLE
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/otif/OTIF_POMEZIA_SPECIAL_PO_CALC";