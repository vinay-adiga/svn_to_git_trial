DROP TABLE IF EXISTS lpfg_wrk.OTIF_TOLERANCE;

	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_TOLERANCE (
		 PLANT string,
		 PO_NO string,
		 YEAR string,
		 SUPPLIER_CODES string,
		 DAYS_EARLY_TOLERANCE string,
		 DAYS_END_TOLERANCE string
		) STORED AS PARQUET LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_TOLERANCE";

