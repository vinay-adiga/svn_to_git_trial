#!/bin/bash

#echo "server name"
#read servername
servername=itsusraedld02.jnj.com

#echo "dev/qa/prod?"
#read name

#echo "path name for final directory"
#read pathname
pathname=FINAL/HIVESCRIPTS/DDL/
cd $pathname

pwd

find -type f | grep .hql | xargs ls -1 > filename.txt

cat filename.txt

sed -e 's/^..//g' filename.txt > filename1.txt

cat filename1.txt

echo "executing the file now"

echo "--------------------------------"

while read line
do
echo $line;

impala-shell --ssl --ca_cert=/opt/cloudera/security/CAcerts/combinedtrust.pem -k -i $servername -f $line >> DDL.log 2>&1

done < filename1.txt

rm filename*