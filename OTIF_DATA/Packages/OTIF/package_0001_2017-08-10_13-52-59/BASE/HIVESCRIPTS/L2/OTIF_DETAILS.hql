INSERT overwrite TABLE lpfg_wrk.OTIF_DETAILS
SELECT trim(po.ENV) AS PLANT,
	CASE 
		WHEN trim(po.ENV) = 'ZA01'
			THEN 'East London,South Africa'
		WHEN TRIM(po.ENV) = 'ZA02'
			THEN 'Cape Town,South Africa'
		ELSE trim(po.PLANT_DESC)
		END AS PLANTDESC,
	po.EBELN AS PO_NO,
	po.EBELP AS PO_LINE,
	po.MATNR AS ITEMCODE,
	po.LIFNR AS SUPPLIERCODE,
	vendor.NAME1 AS SUPPLIERNAME,
	vendor.VENDOR_PARENT_HQ AS VENDOR_PARENTNAME,
	po.INCO AS INCOTERMS,
	po.matnr_desc AS ITEMDESC,
	po.PO_TYPE AS PO_TYPE,
	po.PO_CLASSIFICATION AS PO_CLASSIFICATION,
	po.PUM AS PO_UOM,
	po.GR_UOM AS GR_UOM,
	CASE 
		WHEN length(po.PO_CREATION_DATE) = 8
			THEN CONCAT (
					substr((po.PO_CREATION_DATE), 5, 2),
					'/',
					substr((po.PO_CREATION_DATE), 7, 2),
					'/',
					substr((po.PO_CREATION_DATE), 1, 4)
					)
		END AS ORDER_CREATION_DATE,
	CASE 
		WHEN length(po.ORDD) = 8
			THEN CONCAT (
					substr((po.ORDD), 5, 2),
					'/',
					substr((po.ORDD), 7, 2),
					'/',
					substr((po.ORDD), 1, 4)
					)
		END AS ORDD,
	substr((po.ORDD), 1, 4) AS YEAR,
	po.ORDQ AS ORDQ,
	CASE 
		WHEN length((po.RDD)) = 8
			THEN CONCAT (
					substr((po.RDD), 5, 2),
					'/',
					substr((po.RDD), 7, 2),
					'/',
					substr((po.RDD), 1, 4)
					)
		END AS RDD,
	po.RDQ AS RDQ,
	CASE 
		WHEN length(CAST(po.RECEIPT_DATE AS STRING)) = 8
			THEN CONCAT (
					substr(CAST(po.RECEIPT_DATE AS STRING), 5, 2),
					'/',
					substr(CAST(po.RECEIPT_DATE AS STRING), 7, 2),
					'/',
					substr(CAST(po.RECEIPT_DATE AS STRING), 1, 4)
					)
		END AS RECEIPT_DATE,
	po.MENGE AS RECEIPT_QUANTITY,
	cast(po.STD_LEAD_TIME AS BIGINT) AS STANDARDLT,
	TAR.TARGET AS TARGET,
	material.CATEGORY AS MATERIAL_TYPE,
	PO.PROCUREMENT_TYPE AS PROCUREMENT_TYPE
FROM lpfg_wrk.OTIF_POGR po
LEFT OUTER JOIN lpfg_wrk.OTIF_VENDORS vendor ON (
		po.SOURCE = vendor.SOURCE
		AND po.REGION = vendor.REGION
		AND substring(po.ENV, 1, 2) = substring(vendor.ENV, 1, 2) --Updated code for South Africa
		AND po.LIFNR = vendor.LIFNR
		)
LEFT OUTER JOIN lpfg_wrk.OTIF_MATERIAL material ON (
		substring(material.ENV, 1, 2) = substring(po.ENV, 1, 2)
		AND regexp_replace(po.MATNR, "^0*", "") = regexp_replace(material.MATNR, "^0*", "")
		)
LEFT OUTER JOIN lpfg_wrk.OTIF_PLANT_TARGET TAR ON (po.ENV = TAR.PLANT);
