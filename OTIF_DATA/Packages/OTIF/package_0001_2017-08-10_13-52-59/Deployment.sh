#!/bin/bash

echo '---------- Changing Mode of package 01 -----------'
chmod -R 771 /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0001_2017-08-10_13-52-59

echo '---------- Changing user of Service Account-----------'
chown -R salpfged /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0001_2017-08-10_13-52-59

echo '---------- Getting into the directory of package 01-----------'
cd /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0001_2017-08-10_13-52-59

echo '---------- Changing mode of deploy.sh-----------'
chmod 755 deploy.sh

echo '---------- Running deploy.sh-----------'
sh deploy.sh



echo '---------- Running the DDL Script of OTIF -----------'

echo '---------- DDL Script of OTIF -----------' > LOG_Package_01

sh FINAL/HIVESCRIPTS/DDL/DDL_OTIF.sh >> LOG_Package_01 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of OTIF Executed Successfully'
else
    echo "DDL Script of OTIF executed with error code=$EXIT_STATUS"
fi

echo '---------- Uploading sqoop_log_otif.sh file from Edge node to HDFS -----------' >> LOG_Package_01

hadoop fs -put -f /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0001_2017-08-10_13-52-59/sqoop_log_otif.sh /dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_HIVE/HIVESCRIPTS
EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'File uploaded Successfully'
else
    echo "File uploaded with error code=$EXIT_STATUS"
fi

echo '---------- Running OTIF workflow -----------' >> LOG_Package_01

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0001_2017-08-10_13-52-59/FINAL/oozie_wf/OTIF_WEEKEND_L0_TO_L4_WF/job.properties -run >> LOG_Package_01 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Workflow Executed Successfully'
else
    echo "OTIF Workflow executed with error code=$EXIT_STATUS"
fi

echo 'Going to KILL OTIF Coordinator'

echo '----------OTIF_L0_TO_L4_COORDINATOR-----------' >> LOG_Package_01

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -kill 0029678-170730013045523-oozie-oozi-C

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Workflow KILLED Successfully'
else
    echo "OTIF Workflow KILLED with error code=$EXIT_STATUS"
fi


echo 'Going to schedule OTIF Coordinator'

echo '----------OTIF_L0_TO_L4_COORDINATOR-----------' >> LOG_Package_01


cd  /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0001_2017-08-10_13-52-59/FINAL/oozie_coord/OTIF_WEEKEND_L0_TO_L4_COORDINATOR >> LOG_Package_01 2>&1 


pwd 

echo 'OTIF Oozie Coordinator Run'

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config job.properties -submit -timezone UTC -Dstart_date=`date -u "+%Y-%m-%dT%H:00Z"` -Dotif_frequency="30 9 * * 2-6" 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'Coordinator Scheduled Successfully'
else
    echo "The Coordinator Scheduled interrupted and Executed with error code=$EXIT_STATUS"
fi