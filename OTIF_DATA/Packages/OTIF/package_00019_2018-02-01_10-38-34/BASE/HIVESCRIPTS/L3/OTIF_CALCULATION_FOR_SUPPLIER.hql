INSERT overwrite TABLE lpfg_core.OTIF_CALCULATION_FOR_SUPPLIER
SELECT ot_suplier_base.plant,
	ot_suplier_base.SupplierCode,
	ot_suplier_base.SupplierName,
	CASE 
		WHEN ot_suplier_base.month = 1
			THEN CONCAT ('Jan')
		WHEN ot_suplier_base.month = 2
			THEN CONCAT ('Feb')
		WHEN ot_suplier_base.month = 3
			THEN CONCAT ('Mar')
		WHEN ot_suplier_base.month = 4
			THEN CONCAT ('Apr')
		WHEN ot_suplier_base.month = 5
			THEN CONCAT ('May')
		WHEN ot_suplier_base.month = 6
			THEN CONCAT ('Jun')
		WHEN ot_suplier_base.month = 7
			THEN CONCAT ('Jul')
		WHEN ot_suplier_base.month = 8
			THEN CONCAT ('Aug')
		WHEN ot_suplier_base.month = 9
			THEN CONCAT ('Sep')
		WHEN ot_suplier_base.month = 10
			THEN CONCAT ('Oct')
		WHEN ot_suplier_base.month = 11
			THEN CONCAT ('Nov')
		WHEN ot_suplier_base.month = 12
			THEN CONCAT ('Dec')
		ELSE ''
		END AS Month,
	ot_suplier_base.year AS Year,
	CASE 
		WHEN ot_suplier_base.month = 1
			THEN CONCAT ('Jan','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 2
			THEN CONCAT ('Feb','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 3
			THEN CONCAT ('Mar','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 4
			THEN CONCAT ('Apr','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 5
			THEN CONCAT ('May','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 6
			THEN CONCAT ('Jun','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 7
			THEN CONCAT ('Jul','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 8
			THEN CONCAT ('Aug','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 9
			THEN CONCAT ('Sep','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 10
			THEN CONCAT ('Oct','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 11
			THEN CONCAT ('Nov','-',cast(ot_suplier_base.year AS string))
		WHEN ot_suplier_base.month = 12
			THEN CONCAT ('Dec','-',cast(ot_suplier_base.year AS string))
		ELSE ''
		END AS Month_year,
	ot_suplier_base.ordq_sum,
	ot_suplier_base.Cumulative_Sum,
	ot_suplier_base.Cumulative_Sum_RC,
	ot_suplier_base.On_Time,
	ot_suplier_base.InFull_Month_Supplier AS InFull_Mon_Supplier,
	ot_suplier_base.InFull_Month_Supplier_RC AS InFull_Mon_Supplier_RC,
	round(ot_suplier_base.OTIF_Month_Supplier, 2) AS OTIF_Month_Supplier,
	round(ot_suplier_base.OTIF_Month_Supplier_RC, 2) AS OTIF_Month_Supplier_RC,
	BASE_FINAL.ordq_sum_all AS ordq_sum_all,
	BASE_FINAL.Cumulative_Sum_all,
	BASE_FINAL.Cumulative_Sum_all_RC,
	ot_suplier_base.YTD_InFull_Supplier,
	ot_suplier_base.YTD_InFull_Supplier_RC,
	round((cast(YTD_InFull_Supplier AS DOUBLE) * 100), 2) AS YTD_OTIF_Supplier,
	round((cast(YTD_InFull_Supplier_RC AS DOUBLE) * 100), 2) AS YTD_OTIF_Supplier_RC,
	TRIM(ot_target.TARGET) AS TARGET,
	ot_suplier_base.days_early_tolerance,
	ot_suplier_base.days_end_tolerance,
	ot_suplier_base.RECEIPT_QUANTITY,
	ot_suplier_base.RECEIVEDONTIME,
	ot_suplier_base.REASON_CODE,
	ot_suplier_base.CUM_SUM_RECEIPT_QTY,
	ot_suplier_base.INFULL_ALL,
	ot_suplier_base.ReceivedOnTime_RC,
	ot_suplier_base.Cumulative_QNT_RC,
	ot_suplier_base.InFull_RC,
	ot_suplier_base.Otif_RC,
	ot_suplier_base.COUNT_SLOTIF ,
	ot_suplier_base.SUM_SLOTIF ,
	ot_suplier_base.SUPPLIER_SLOTIF ,
	ot_suplier_base.COUNT_YEAR_SLOTIF ,
	ot_suplier_base.SUM_YEAR_SLOTIF ,
	ot_suplier_base.YTD_SUPPLIER_SLOTIF ,
	ot_suplier_base.COUNT_SLOTIF_RC ,
	ot_suplier_base.SUM_SLOTIF_RC ,
	ot_suplier_base.SUPPLIER_SLOTIF_RC ,
	ot_suplier_base.COUNT_YEAR_SLOTIF_RC ,
	ot_suplier_base.SUM_YEAR_SLOTIF_RC ,
	ot_suplier_base.YTD_SUPPLIER_SLOTIF_RC 
	
FROM (
	SELECT BASE.*,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFull_Supplier,
		CASE 
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) <= cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) / cast(BASE.ordq_sum_all AS DOUBLE))
			WHEN cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) > cast(BASE.ordq_sum_all AS DOUBLE)
				THEN (cast(BASE.ordq_sum_all AS DOUBLE) - (cast(BASE.Cumulative_Sum_all_RC AS DOUBLE) - cast(BASE.ordq_sum_all AS DOUBLE))) / cast(BASE.ordq_sum_all AS DOUBLE)
			ELSE 0
			END AS YTD_InFull_Supplier_RC
	FROM lpfg_core.OTIF_SUPPLIERWISE_BASE BASE
	) ot_suplier_base
JOIN (
	SELECT plant,
		suppliercode,
		year,
		sum(MAX_ORDQ_SUM) AS ordq_sum_all,
		sum(MAX_CUM_SUM) AS Cumulative_Sum_all,
		sum(MAX_CUM_SUM_RC) AS Cumulative_Sum_all_RC
	FROM (
		SELECT plant,
			suppliercode,
			month,
			year,
			max(ordq_sum) AS MAX_ORDQ_SUM,
			max(Cumulative_Sum) AS MAX_CUM_SUM,
			max(Cumulative_Sum_RC) AS MAX_CUM_SUM_RC
		FROM lpfg_core.OTIF_SUPPLIERWISE_BASE
		GROUP BY plant,
			suppliercode,
			month,
			year
		) suplier_base
	GROUP BY plant,
		suppliercode,
		year
	) BASE_FINAL ON (
		ot_suplier_base.plant = BASE_FINAL.plant
		AND ot_suplier_base.suppliercode = BASE_FINAL.suppliercode
		AND ot_suplier_base.year = BASE_FINAL.year
		)
LEFT OUTER JOIN lpfg_wrk.OTIF_PLANT_TARGET ot_target ON (ot_suplier_base.PLANT = ot_target.PLANT);
		