DROP TABLE IF EXISTS lpfg_stg.OTIF_TOLERANCE_BY_PLANT;

CREATE EXTERNAL TABLE lpfg_stg.OTIF_TOLERANCE_BY_PLANT (
		SOURCE_ID string,
		YEAR string,
		PLANT string,
		LOWER_TOLERANCE string,
		UPPER_TOLERANCE string
		) ROW format delimited fields terminated BY "|" LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_stg/otif/OTIF_TOLERANCE_BY_PLANT";
