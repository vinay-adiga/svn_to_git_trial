DROP TABLE IF EXISTS lpfg_stg.BPCS_HPOX;
	CREATE EXTERNAL TABLE lpfg_stg.BPCS_HPOX (
		PXORD string,
		PXPLIN string,
		PXITEM string,
		PXOQTY DOUBLE,
		PXUOM string,
		PXEDTE string,
		PXRDTE string,
		PXREQ string,
		PXECST string,
		PXVTXC string,
		PXITXC string,
		PXFOBC string,
		PXFOBP string,
		PXPSRC string,
		PXLIST string
		) PARTITIONED BY (
		SOURCE STRING,
		REGION STRING
		) ROW format delimited fields terminated BY "|";

ALTER TABLE lpfg_stg.BPCS_HPOX ADD PARTITION (
	SOURCE = 'BPCS',
	region = 'FRM'
	) LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/BPCS/TRANSACTION/FRM/HPOX';

ALTER TABLE lpfg_stg.BPCS_HPOX ADD PARTITION (
	SOURCE = 'BPCS',
	region = 'ZAC'
	) LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/BPCS/TRANSACTION/ZAC/HPOX';

ALTER TABLE lpfg_stg.BPCS_HPOX ADD PARTITION (
	SOURCE = 'BPCS',
	region = 'DEM'
	) LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/BPCS/TRANSACTION/DEM/HPOX';

ALTER TABLE lpfg_stg.BPCS_HPOX ADD PARTITION (
	SOURCE = 'BPCS',
	region = 'GRC'
	) LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/BPCS/TRANSACTION/GRC/HPOX';

ALTER TABLE lpfg_stg.BPCS_HPOX ADD PARTITION (
	SOURCE = 'BPCS',
	region = 'ITM'
	) LOCATION '/dev/edl/sc/consumer/lpfg/str/lpfg_stg/BPCS/TRANSACTION/ITM/HPOX';
