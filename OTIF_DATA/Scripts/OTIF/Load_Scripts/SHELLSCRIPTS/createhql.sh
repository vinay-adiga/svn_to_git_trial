#!/bin/sh
#original scripts path
INPUTDIR=$1
OUTPUTDIR=$2

echo "Remove existing output directory"
rm -rf $OUTPUTDIR
echo "Input Dir: $INPUTDIR"
echo "Output Dir to be used by impala to run scripts: $OUTPUTDIR"
cp -r $INPUTDIR $OUTPUTDIR
echo "INPUT DIR IS $INPUTDIR
echo "OUTPUT DIR iS $OUTPUTDIR
while IFS=':' read -r col1 col2
do 
REPL=$col1
ENVOR=$col2

echo "Replacement Char: $REPL"
echo "Enviroinment : $ENVOR"
#find $OUTPUTDIR -type f -exec sed -i -e 's/'$REPL'/'$ENVOR'/g' {} \;
find $OUTPUTDIR -type f -exec sed -i -e "s/${REPL}/${ENVOR}/g" {} \;
done <$3
