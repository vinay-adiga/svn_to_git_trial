#!/bin/bash

echo '---------- Changing Mode of package 20 -----------'
chmod -R 771 /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0000_2017-07-27_18-54-06

echo '---------- Changing user of Service Account-----------'
chown -R salpfged /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0000_2017-07-27_18-54-06

echo '---------- Getting into the directory of package 20-----------'
cd /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0000_2017-07-27_18-54-06

echo '---------- Changing mode of deploy.sh-----------'
chmod 755 deploy.sh

echo '---------- Running deploy.sh-----------'
sh deploy.sh



echo '---------- Running the DDL Script of OTIF -----------'

echo '---------- DDL Script of OTIF -----------' > LOG_Package_00

sh FINAL/HIVESCRIPTS/DDL/DDL_OTIF.sh >> LOG_Package_00 2>&1

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo ' DDL Script of OTIF Executed Successfully'
else
    echo "DDL Script of OTIF executed with error code=$EXIT_STATUS"
fi


echo '---------- Running OTIF workflow -----------' >> LOG_Package_00

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0000_2017-07-27_18-54-06/FINAL/oozie_wf/OTIF_L0_TO_L4_WF/job.properties -run >> LOG_Package_00 2>&1 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'OTIF Workflow Executed Successfully'
else
    echo "OTIF Workflow executed with error code=$EXIT_STATUS"
fi

echo 'Going to schedule OTIF Coordinator'

echo '----------OTIF_L0_TO_L4_COORDINATOR-----------' >> LOG_Package_00


cd  /data/data01/dev/edl/sc/consumer/lpfg/appcode/scripts/OTIF_Deployment/OTIF/package_0000_2017-07-27_18-54-06/FINAL/oozie_coord/OTIF_L0_TO_L4_COORDINATOR >> LOG_Package_00 2>&1 


pwd 

echo 'OTIF Oozie Coordinator Run'

oozie job -oozie https://itsusraedld01.jnj.com:11443/oozie -config job.properties -submit -timezone UTC -Dstart_date=`date -u "+%Y-%m-%dT%H:00Z"` -Dotif_frequency="30 9 * * 2-6" 

EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
     echo 'Coordinator Scheduled Successfully'
else
    echo "The Coordinator Scheduled interrupted and Executed with error code=$EXIT_STATUS"
fi