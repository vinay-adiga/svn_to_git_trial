INSERT OVERWRITE TABLE lpfg_core.OTIF_PLANT_ORDQ_CALC
SELECT itemcode,
	plant,
	ordd,
	month,
	year,
	receipt_quantity,
	po_no,
	po_line,
	days_early_tolerance,
	days_end_tolerance,
	Number_of_deliveries,
	po_sku,
	OTIF_CALC.po_sku_ordd,
	po_sku_ReqDate_deliverables,
	po_classification,
	plantdesc,
	DELAY,
	on_time,
	ReceivedOnTime,
	ordq,
	Cumulative_QNT,
	InFull,
	otif,
	Material_Description,
	PO_UOM,
	Receipt_UOM,
	INCOTERMS,
	Receipt_Date,
	STANDARDLT,
	MATERIAL_TYPE,
	PO_TYPE,
	min_no_del,
	max_no_del,
	sum(cast(ordq AS DOUBLE)) OVER (  
		PARTITION BY plant,
		month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))),
		year(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) ORDER BY month(from_unixtime(unix_timestamp(ordd, 'MM/dd/yyyy'))) DESC 
		ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
		) AS ordq_sum,
	ReceivedOnTime_RC,
	Cumulative_QNT_RC,
	InFull_RC,
	Otif_RC 
FROM lpfg_core.OTIF_CALCULATION OTIF_CALC,
	(
		SELECT min(number_of_deliveries) AS min_no_del,
			max(number_of_deliveries) AS max_no_del,
			po_sku_ordd
		FROM lpfg_core.OTIF_CALCULATION ot_calc
		GROUP BY po_sku_ordd
		) calc
WHERE OTIF_CALC.po_sku_ordd = calc.po_sku_ordd
	AND OTIF_CALC.number_of_deliveries = calc.max_no_del;
