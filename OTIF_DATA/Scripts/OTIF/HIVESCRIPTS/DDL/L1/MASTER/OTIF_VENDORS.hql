DROP TABLE IF EXISTS lpfg_wrk.OTIF_VENDORS;

	CREATE EXTERNAL TABLE lpfg_wrk.OTIF_VENDORS (
		SOURCE STRING
		,REGION STRING
		,ENV string
		,LIFNR string
		,KTOKK string
		,STRAS string
		,ORT01 string
		,REGIO string
		,LAND1 string
		,ZIPC string
		,CITY string
		--,NAME2 string
		,NAME3 string
		,TELF1 string
		,NAME1 string
		,affiliate string
		,SAP_VENDORCODE string
		,VENDOR_PARENT_HQ string
		) STORED AS PARQUET LOCATION "/dev/edl/sc/consumer/lpfg/str/lpfg_wrk/OTIF_VENDORS";