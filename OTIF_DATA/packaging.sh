#!/bin/sh
#
#PACKAGING SCRIPT
#
PROJECT=$1
if [ -d tempdir ]
then
	rm -rf tempdir
fi
mkdir tempdir
OPFILE="xxx"
counter=0
while read name
do
	if [[ ${name} == *"[HEADER]"* ]]
	then
		OPFILE="tempdir/header"
	fi
	if [[ ${name} == *"[CODE]"* ]]
	then
		OPFILE="tempdir/readfile"
	fi
	if [[ ${name} == *"[ADDITIONAL_STEPS]"* ]]
	then
		OPFILE="tempdir/addsteps"
	fi
	
	if [[ ${name} == *"[HEADER]"* ]] || [[ ${name} == *"[CODE]"* ]] || [[ ${name} == *"[ADDITIONAL_STEPS]"* ]]
	then
		counter=1
	else
		counter=0
	fi
	
	size=${#name}

	if [ $counter -eq 0 ] && [ $size -gt 3 ]
	then
		
		echo $name >> $OPFILE
	fi
done < Packages/$PROJECT/package.txt





if [ -d ../../deployment/TEMP1 ]
then
	rm -rf ../../deployment/TEMP1
fi

mkdir ../../deployment/TEMP1



DATE=$(date +%F"_"%H"-"%M"-"%S)

echo $PROJECT
TYPE=$2
PROJECTDIR=$3

CNTR=0

FILEEX=$(ls Packages/$PROJECT/* | grep package_000)
echo "OLDER PACKAGING EXISTS:" "${#FILEEX}"

FILEEXCNT=${#FILEEX}

echo "FILECNT:.."$FILEEXCNT

if [ $FILEEXCNT -gt 0 ]
then
		echo "FILE CNT IS $FILEEXCNT"
		#PDIR=$(ls -td -- Packages/$PROJECT/package_000* | head -n 1 | cut -d'/' -f3)
		#PDIR=$(ls -ld Packages/$PROJECT/package_000* | cut -d' ' -f10 | cut -d'_' -f2 | sort -nr | head -1)
		 if [ "$PROJECT" = "Data_Int" ]
                then
                         PDIR=$(ls -d Packages/$PROJECT/package_000* | cut -d'_' -f3 | sort -nr | head -1)
                else
                         PDIR=$(ls -d Packages/$PROJECT/package_000* | cut -d'_' -f2 | sort -nr | head -1)
                fi
		
		echo "!!!!!!!!!!!!!!!!....$PDIR"
        #IFS="_"
        #set -- $PDIR
        #CURRCNTR=$2
		CURRCNTR=$PDIR
		#re='^[0-9]+$'
		
		echo "CURRENT PACKAGE COUNER:"$CURRCNTR
        
		#if ! [[ $CURRCNTR =~ $re ]] ; then
		#	echo "error: existing package dosed not contain package number" 
		#	echo " PACKAGE CREATION WILL EXIT NOW...."
		#	exit 4
		#fi
		CNTR=`expr $CURRCNTR + 1` 
		echo "!!!!!!!!UPDATED COUNTER!!!!!"
		
fi
IFS=""
echo "PAKCGE COUNTER IS : "$	

#CNTR=$(cat ~/deployement/counter)
#echo "Package Countter:"$CNTR


PACKAGINGDIR="package_000"$CNTR"_"$DATE


echo "PROJECT DIR IS: "$PROJECTDIR

if [ -d Packages/$PROJECT/$PACKAGINGDIR ]
then
	rm -rf Packages/$PROJECT/$PACKAGINGDIR
fi

mkdir Packages/$PROJECT/$PACKAGINGDIR


if [[ $TYPE == "HIVE" ]]
then
	echo "Paackaging Started............. ................ ."
	while read scripts
		do
		xxx="${scripts}"
		size="${#xxx}"
		echo ${size}
		if [ $size -gt 4 ]
			then
			set -- "${xxx}"
			IFS="\/";declare -a a=($*)
			filename="${a[${#a[@]}-1]}"
			IFS=""
			
			CREATEDIR="BASE/"$(echo $scripts | sed -e "s|${PROJECTDIR}||Ig" | sed -e "s|${filename}||Ig")
			
			echo "DIR IS  "$CREATEDIR
			if [ ! -d ../../deployment/TEMP1/$CREATEDIR ]
			then 
				mkdir -p ../../deployment/TEMP1/$CREATEDIR
			fi
				
			echo "Copying File:=============>" $scripts
			#cp -r $scripts ~/deployement/TEMP1/$CREATEDIR
			#mkdir -p ~/deployement/TEMP1/$CREATEDIR/$name
			#mkdir -p Packages/$PROJECT/$PACKAGINGDIR/BASE/${filename}
			rsync -r $scripts ../../deployment/TEMP1/$CREATEDIR/${filename}
			RC=$?	
			if [ $RC -ne 0 ]
				then
					echo "ERROR WHILE COPYIGING FILE: " $name 
					echo "DELETING CURRENt PACKAGE AS ITS A ERROR"
					rm -rf Packages/$PROJECT/$PACKAGINGDIR
					echo "EXITING BUILD............................."
					
					exit 4
			fi			
	fi	
	done < tempdir/readfile
	
	cp $PROJECTDIR/Load_Scripts/SHELLSCRIPTS/config_qa.properties ../../deployement/TEMP1/
	cp $PROJECTDIR/Load_Scripts/SHELLSCRIPTS/config_prod.properties ../../deployement/TEMP1/
	cp $PROJECTDIR/Load_Scripts/SHELLSCRIPTS/config_dev.properties ../../deployement/TEMP1/
	cp $PROJECTDIR/Load_Scripts/SHELLSCRIPTS/createhql.sh ../../deployement/TEMP1/
	echo "SVN REVESION:"$(svn info |grep Revision: |cut -c11-) >> tempdir/header 
	echo "SVN LAST CHANGE DATE :"$(svn info |grep "Last Changed Date": |cut -c20-) >> tempdir/header
	echo "----------------------------------------------------------------------------------" >> tempdir/header
	echo "----------------------------------------------------------------------------------" >> tempdir/header
	echo "ADDITIONAL INFORMATION" >> tempdir/header
	
	cat tempdir/header tempdir/addsteps > Packages/$PROJECT/$PACKAGINGDIR/readme.txt
	
	mv ../../deployment/TEMP1/* Packages/$PROJECT/$PACKAGINGDIR/ 
	cp Packages/$PROJECT/config.properties Packages/$PROJECT/$PACKAGINGDIR/
	RC=$?	
			if [ $RC -ne 0 ]
				then
					echo "ERROR WHILE COPYIGING FILE: config.properties for package" 
					echo "DELETING CURRENt PACKAGE AS ITS A ERROR"
					rm -rf Packages/$PROJECT/$PACKAGINGDIR
					echo "EXITING BUILD............................."
					exit 4
			fi		
	
	cp Packages/$PROJECT/deploy.sh Packages/$PROJECT/$PACKAGINGDIR/
	RC=$?	
			if [ $RC -ne 0 ]
				then
					echo "ERROR WHILE COPYIGING FILE: deploy.sh  for package" 
					echo "DELETING CURRENt PACKAGE AS ITS A ERROR"
					rm -rf Packages/$PROJECT/$PACKAGINGDIR
					echo "EXITING BUILD............................."
					exit 4
			fi		
	echo $PACKAGINGDIR > Packages/$PROJECT/latestpackage.txt
	echo "Sleeping for 10s "
	sleep 10s
	ls -l Packages/$PROJECT/$PACKAGINGDIR/BASE/oozie_wf
	#git add . Packages/$PROJECT/$PACKAGINGDIR --auto-props --parents --depth infinity
	git add .  Packages/$PROJECT/$PACKAGINGDIR
	git commit -m "$PACKAGINGDIR"
	git commit Packages/$PROJECT/latestpackage.txt -m "latest package"
	git push
	RC=$?	
	if [ $RC -ne 0 ]
		then
			echo "ERROR WHILE COMITTING THE  package to GIT" 
			echo "DELETING CURRENt PACKAGE AS ITS A ERROR"
			rm -rf Packages/$PROJECT/$PACKAGINGDIR
			echo "EXITING BUILD............................."
			exit 4
		fi	
	
	echo $CNTR > ../../deployment/counter
fi

rm -rf tempdir


echo "PACKAGING COMPLETED"

